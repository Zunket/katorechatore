@extends('layout.master')

@section('title')

{{APP_NAME}}
@endsection

@section('content')

    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
          <div class="row">
            <div class="col-xl-3 col-md-6 col-12">
              <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!-- <div class="p-2 text-center bg-danger rounded-left">
                             <i class="fa fa-user-circle-o font-large-2 text-white"></i> 
                        </div> -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">Total Earning</h5>
                            <h5 class="text-bold-400">${{$total_earnings}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12">
              <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!-- <div class="p-2 text-center bg-danger rounded-left">
                             <i class="fa fa-user-circle-o font-large-2 text-white"></i> 
                        </div> -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">Commision from Vendor</h5>
                            <h5 class="text-bold-400">${{$total_restaurant_comission}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="media align-items-stretch">
                          <!-- <div class="p-2 text-center bg-success rounded-left">
                               <i class="fa fa-motorcycle font-large-2 text-white"></i> 
                          </div> -->
                          <div class="py-1 px-2 media-body">
                              <h5 class="success"> Commision from Delivery Charge</h5>
                              <h5 class="text-bold-400">${{$total_delivery_boy_comission}}</h5>
                              <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                              </div> -->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!--  -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">Total Commision</h5>
                            <h5 class="text-bold-400">${{$total_admin_comission+$total_delivery_boy_comission+$total_restaurant_comission}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
          </div>
        
        
        </div>
        </div>
      </div>
      <div class="content-body">
        <!-- Basic form layout section start -->


        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header">
                  <!-- <h4 class="card-title">DELIVERIES</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                  <div class="heading-elements">
                     <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      </ul>
                      </div>
                      
                    </div> -->
                </div>


               <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <form action="#" method="get" class="icons-tab-steps wizard-notification">
                    <fieldset>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <!-- <label for="firstName2">Delivery People :</label> -->
                              <select class="c-select form-control" id="Delivery_People" name="user_id"> 
                                <option value="">Select</option>
                                @foreach($data as $d)
                                <option value="{{$d->user_id}}">{{$d->user_name}}</option>
                                
                                @endforeach
                              </select>  
                            </div>
                          </div>
                         
                            <div class="col-md-6"> 
                             <div class="form-group">
                             <label>Start Date : </label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                 <span class="input-group-text">
                                   <span class="la la-calendar-o"></span>
                                </span>
                                </div>
                              <input type="text" name="from_date" value="{{old('from_date')}}" class="form-control pickadate-arrow" placeholder="Change Formats"/>
                            </div>
                           </div>
                           </div>
                           <div class="col-md-6"> 
                            <div class="form-group">
                             <label>End Date : </label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                 <span class="input-group-text">
                                   <span class="la la-calendar-o"></span>
                                </span>
                                </div>
                              <input type="text" name="to_date" value="{{old('from_to')}}" class="form-control pickadate-arrow" placeholder="Change Formats"/>
                            </div>
                           </div>

                          </div>
                          <div class="col-md-12">
                            <div class="form-actions">
                              <button class="btn btn-success mr-1" style="padding: 10px 15px;" onclick="javascript:form_validation();">
                                <i class="ft-check-square"></i> Submit
                                </button>
                              <a href="{{url('/admin/deliveries')}}" class="btn btn-danger mr-1" style="padding: 10px 15px;" role="button"><i class="ft-x"></i> Reset</a>
                              
                            </div>
                          </div>
                        
                          
                        </div>

                          
                      </fieldset>
                    </form>
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead> 
                          <tr>
                            
                            <th>Customer Name</th>
                           <!--  <th>Delivery People</th> -->
                            <!-- <th>Restaurant</th> -->
                            <th>Address</th>
                            <th>Cost</th>
                            <!-- <th>Status</th>  -->
                            <!-- <th>Order List</th> -->
                            <th>Order List</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1; ?>
                          @foreach($data as $d)
                          <tr>
                           
                            <td>{{$d->user_name?$d->user_name:'N/A'}}</td>
                           <!--  <td>{{$d->delivery_boy_name}}</td> -->
                            <!-- <td>{{$d->restaurant_name}}</td> -->
                            <td>{{$d->delivery_address}}</td>
                            <td> ${{$d->bill_amount}}</td>
                            <!-- <td> <span class="btn btn-info">COMPLETED</span> </td> -->
                            <!-- <td><button class="btn btn-primary" data-id="1">Order List</button></td> -->
                             <td>   
                              <div class="form-group">       
                                 <button type="button" class="btn btn-success shadow-box" data-id="1" data-toggle="modal"  data-target="#{{$d->order_id}}">  View Order  </button>

                                  <div class="modal animated slideInRight text-left" id="{{$d->order_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                                   <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                     <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel76">View Order</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                     </div>
                                  <div class="modal-body">
                                      <div class="row m-0">
                                          <dl class="order-modal-top">
                                              <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Order ID</dt>
                                                  <dd class="col-sm-9 order-txt orderid"><span>: </span>{{$d->order_id}}</dd>
                                              </div>
                                             <!--  <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Restaurant Name</dt>
                                                  <dd class="col-sm-9 order-txt rest_name"><span>: </span>Lido azul</dd>
                                              </div> -->
                                              
                                              @if($d->user_name !='')
                                              <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Customer Name</dt>
                                                  <dd class="col-sm-9 order-txt cust_name"><span>: </span>{{$d->user_name}}</dd>
                                              </div>
                                              @endif
                                              @if($d->delivery_boy_name)
                                              <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Delivery People</dt>
                                                  <dd class="col-sm-9 order-txt cust_name"><span>: </span>{{$d->delivery_boy_name}}</dd>
                                              </div>
                                              @endif
                                              @if($d->restaurant_name)
                                              <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Restaurant</dt>
                                                  <dd class="col-sm-9 order-txt cust_name"><span>: </span>{{$d->restaurant_name}}</dd>
                                              </div>
                                              @endif
                                              <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Address</dt>
                                                  <dd class="col-sm-9 order-txt address"><span>: </span>{{$d->delivery_address}}</dd>
                                              </div>
                                              <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Phone Number</dt>
                                                  <dd class="col-sm-9 order-txt cust_phone"><span>: </span>{{$d->phone}}</dd>
                                              </div>
                                              <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Delivery Date</dt>
                                                  <dd class="col-sm-9 order-txt cust_delivery_date"><span>: </span>{{$d->ordered_time}}</dd>
                                              </div>
                                               <!-- <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Note</dt>
                                                  <dd class="col-sm-9 order-txt cust_order_note"><span>: -- </span></dd>
                                              </div> -->
                                              <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Shop Rating</dt>
                                                  <dd class="col-sm-9 order-txt rate_shop"><span>: {{$d->rating}} </span></dd>
                                              </div>
                                              <!-- <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Delivery boy rating</dt>
                                                  <dd class="col-sm-9 order-txt rate_deliveryboy"><span>: -- </span></dd>
                                              </div> -->
                                              <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Total Amount</dt>
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>${{$d->bill_amount}}</dd>
                                                  <br>
                                                  <br>
                                              </div>
                                              
                                              <div class="row m-0">
                                                  <dt class="col-sm-3 order-txt p-0">Status</dt>
                                                  @if($d->order_status==0)
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Accept</dd>
                                                  @endif
                                                  @if($d->order_status==1)
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Assign</dd>
                                                  @endif
                                                  @if($d->order_status==2)
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Delivery boy assigned</dd>
                                                  @endif
                                                  @if($d->order_status==3)
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Food delivered to Delivery boy</dd>
                                                  @endif
                                                  @if($d->order_status==4)
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Towards Customer</dd>
                                                  @endif
                                                  @if($d->order_status==5)
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Reached Customer</dd>
                                                  @endif
                                                  @if($d->order_status==6)
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Delivered to Customer</dd>
                                                  @endif
                                                  @if($d->order_status==7)
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Completed</dd>
                                                  @endif
                                                  @if($d->order_status == 10)
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Cancelled</dd>
                                                  @endif
                                                  @if($d->order_status == 9)
                                                  <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Cancelled By Admin</dd>
                                                  @endif
                                                  <br>
                                                  <br>
                                              </div>
                                              
                                             <!--   <div class="row m-0" id="order_status_list">
                                                <dd class="col-sm-3 order-txt p-0">ORDERED</dd>                
                                                <dd class="col-sm-9 order-txt "> 2018-03-17 14:24:15</dd>
                                                <dd class="col-sm-3 order-txt p-0">RECEIVED</dd>                
                                                <dd class="col-sm-9 order-txt "> 2018-03-17 14:24:25</dd>
                                                <dd class="col-sm-3 order-txt p-0">ASSIGNED</dd>                
                                                <dd class="col-sm-9 order-txt "> 2018-03-17 14:24:47</dd>
                                                <dd class="col-sm-3 order-txt p-0">PROCESSING</dd>                
                                                <dd class="col-sm-9 order-txt "> 2018-03-17 14:24:56</dd>
                                                <dd class="col-sm-3 order-txt p-0">REACHED</dd>                
                                                <dd class="col-sm-9 order-txt "> 2018-03-17 14:25:00</dd>
                                                <dd class="col-sm-3 order-txt p-0">PICKEDUP</dd>                
                                                <dd class="col-sm-9 order-txt "> 2018-03-17 14:25:04</dd>
                                                <dd class="col-sm-3 order-txt p-0">ARRIVED</dd>                
                                                <dd class="col-sm-9 order-txt "> 2018-03-17 14:25:07</dd>
                                                <dd class="col-sm-3 order-txt p-0">COMPLETED</dd>                
                                                <dd class="col-sm-9 order-txt "> 2018-03-17 14:25:08</dd>
                                              </div> -->
                                              <hr>
                                          </dl>
                                          <div class="table-responsive">
                                              <table class="table">
                                                  <thead>
                                                      <tr>
                                                          <!-- <th>Product Image</th> -->
                                                          <th>Product Name</th>
                                                          <th>Note</th>
                                                          <th>Price</th>
                                                          <th>Quantity</th>
                                                          <th>Cost</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody class="cartstbl">
                                                    @foreach($data1 as $d1)
                                                    @if($d1->request_id == $d->request_id)
                                                    <tr>
                                                      <!-- <td>
                                                        <img src="http://ecx.images-amazon.com/images/I/51bRhyVTVGL._SL50_.jpg" width="100px" alt=""></td> -->
                                                        <td>{{$d1->food_name}}</td>
                                                        <td>null</td>
                                                        <td>${{$d1->price}}</td>
                                                        <td>{{$d1->quantity}}</td>
                                                        <td>${{$d1->price * $d1->quantity}}</td>
                                                      </tr>
                                                      @endif
                                                      @endforeach
                                                    </tbody>
                                                  <tfoot>
                                                    <tr>
                                                          <th></th>
                                                          <th></th>
                                                          <th></th>
                                                          <th>Packaging Charge</th>
                                                          <th ><span>: </span> {{$d->restaurant_packaging_charge}}</th>
                                                          <th> </th>
                                                      </tr>
                                                       <tr>
                                                          <th></th>
                                                          <th></th>
                                                          <th></th>
                                                          <th>Discount</th>
                                                          <th class="discount"><span>: </span> {{$d->offer_discount}}</th>
                                                          <th> </th>
                                                      </tr>
                                                      <tr>
                                                          <th></th>
                                                          <th></th>
                                                          <th></th>
                                                          <th>Delivery Charge</th>
                                                          <th class="delivery_charge"><span>: </span> {{$d->delivery_charge}}</th>
                                                          <th> </th>
                                                      </tr>
                                                      <tr>
                                                          <th></th>
                                                          <th></th>
                                                          <th></th>
                                                          <th>Tax</th>
                                                          <th class="tax"><span>: </span> {{$d->tax}}</th>
                                                          <th> </th>
                                                      </tr>
                                                      <tr>
                                                          <th></th>
                                                          <th></th>
                                                          <th></th>
                                                          <th>Total</th>
                                                          <th class="tot_amt"><span>: </span> {{$d->bill_amount}}</th>
                                                          <th> </th>
                                                      </tr>
                                                  </tfoot>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </td>
                      <td>
                        @if($d->order_status==0)
                        <a href="{{url('/')}}/admin/accept_request/{{$d->request_id}}" class="btn btn-success">Accept</a>
                        <a href="{{url('/')}}/admin/cancel_request/{{$d->request_id}}" class="btn btn-danger">Cancel</a>
                        @endif
                         @if($d->order_status==1)
                        <a href="{{url('/')}}/admin/assign_request/{{$d->request_id}}" class="btn btn-success">Assign</a>
                        <a href="{{url('/')}}/admin/cancel_request/{{$d->request_id}}" class="btn btn-danger">Cancel</a>
                        @endif
                        @if($d->order_status==2)
                        <a href="#">Delivery boy assigned</a>
                        <a href="{{url('/')}}/admin/cancel_request/{{$d->request_id}}" class="btn btn-danger">Cancel</a>
                        @endif
                        @if($d->order_status==3)
                        <a href="#">Food delivered to Delivery boy</a>
                        @endif
                        @if($d->order_status==4)
                        <a href="#">Towards Customer</a>
                        @endif
                        @if($d->order_status==5)
                        <a href="#">Reached Customer</a>
                        @endif
                        @if($d->order_status==6)
                        <a href="#">Delivered to Customer</a>
                        @endif
                        @if($d->order_status==7)
                        <a href="#">Completed</a>
                        @endif
                        @if($d->order_status == 10)
                        <a href="#">Cancelled</a>
                        @endif
                        @if($d->order_status == 9)
                        <a href="#">Cancelled by Admin</a>
                        @endif
                        
                      </td>
                 </tr>
                 <?php $i++; ?>
                 @endforeach

                  </tbody>
                 </table>
                    </div>
             
                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- // Basic form layout section end -->
      </div>
    </div>
 


    @endsection     
 