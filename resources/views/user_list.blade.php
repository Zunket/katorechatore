@extends('layout.master')

@section('title')
{{APP_NAME}}
@endsection

@section('content')
<style type="text/css">
  #top-list{
  list-style-type: none;
  list-style: none;
}
</style>
  <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
          <div class="row">
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!-- <div class="p-2 text-center bg-danger rounded-left">
                             <i class="fa fa-user-circle-o font-large-2 text-white"></i> 
                        </div> -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All User</h5>
                            <h5 class="text-bold-400">{{$user_detail->count()}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="media align-items-stretch">
                          <!-- <div class="p-2 text-center bg-success rounded-left">
                               <i class="fa fa-motorcycle font-large-2 text-white"></i> 
                          </div> -->
                          <div class="py-1 px-2 media-body">
                              <h5 class="success">Active User</h5>
                              <h5 class="text-bold-400">0</h5>
                              <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                              </div> -->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-xl-4 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!--  -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">Inactive User</h5>
                            <h5 class="text-bold-400">0</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
          </div>
        
          </div>
        </div>
          <!-- <h3 class="content-header-title mb-0 d-inline-block">{{ strtoUpper(trans('constants.user'))}} {{ strtoUpper(trans('constants.list'))}}</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}/admin/dashboard">{{ strtoUpper(trans('constants.dashboard'))}}</a>
                </li>
                <li class="breadcrumb-item"><a href="#">{{ strtoUpper(trans('constants.user'))}} {{ strtoUpper(trans('constants.list'))}}</a>
                </li>
              </ol>
            </div>
          </div> -->
        </div>
      
      <div class="content-body">
        <!-- Basic form layout section start -->


        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header">
                    <h4 class="card-title" style="height:20px;">
                  </h4>
                  <h4 class="card-title"></h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                  <div class="heading-elements">
                     <ul class="list-inline mb-0">
                      <!-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li> -->
                      </ul>
                      </div>
                      
                    </div>
                </div>


               <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead> 


                          <tr>
                           
                            <th>Name</th>
                            <th>Email</th>
                            <th>Image</th>
                            <th>Customer Details</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          @foreach($user_detail as $key)
                          <tr>
                            
                            <td>{{$key->name}}</td>
                            <td>{{$key->email}}</td>
                            <td><img src="{{$key->profile_image}}" width="100px" alt=""></td>
                            <td>{{$key->phone}}</td>
                            <td> 
                              <ul id="top-list">
                                <li>
                                  <a href="{{url('/')}}/admin/edit_user/{{$key->id}}" class="button btn btn-icon btn-success shadow-box mr-1 link_clr"><i class="ft-edit"></i></a>
                                  <a href="{{url('/')}}/admin/delete_user/{{$key->id}}" class="btn btn-icon btn-danger shadow-box mr-1 link_clr" data-id="1" data-toggle="modal"  data-target="#{{$key->id}}"><i class="ft-delete"></i></a></li>
                              </ul>
                            </td>
                            
                          </tr>
                          <!--- Deleted Restaurant / Model -->
                                  <div class="modal animated slideInRight text-left" id="{{$key->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                                       <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                         <div class="modal-header">
                                          <h4 class="modal-title" id="myModalLabel76">Delete User</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                         </div>
                                         <form method="post" action="{{url('/')}}/admin/delete_user/{{$key->id}}">
                                            <div class="modal-body">
                                              
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                  <input type="hidden" name="id" value="{{$key->id}}">
                                                <div class="form-group">
                                                    <label for="eventName2">Are you sure to delete user : {{$key->name}}</label>
                                                </div>
                                            
                                            </div>
                                            <div class="modal-footer">
                                              <button type="submit" class="btn btn-black mr-1" style="padding: 10px 15px;">
                                                    <i class="ft-check-square"></i> Delete
                                                </button>
                                                <button type="button" class="btn btn-danger mr-1" data-dismiss="modal" style="padding: 10px 15px;">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                
                                            </div>
                                          </form>
                                      </div>
                                    </div>
                                  </div>
                            <!--- End Deleted Restaurant / Modulel -->
                          
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- // Basic form layout section end -->
      </div>
    </div>

    @endsection     
 