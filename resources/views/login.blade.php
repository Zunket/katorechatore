<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Ansonika">
    <title>{{ config('app.name') }}</title>
    <!-- BASE CSS -->
    <link href="{{URL::asset('public/login-assets/LOGIN/css/base.css')}}" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_FAVICON)}}">
     
</head>
<style type="text/css">
    .btn.btn-submit {
    width: 100%;
    margin-top: 30px;
    color: #191b19;
    padding: 10px;
    background: #fff;
    font-weight: 600;
    outline: 0;
    -webkit-transition: all .2s ease;
    transition: all .2s ease;
    font-size: 16px;

}


</style>

<body style="background: white;">
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="box b-a-0" style="top: 132px; box-shadow: none;height: 448px;position: relative;
    display: block;
    margin-bottom: 0.75rem;">
                        <div class="p-2 text-xs-center" style="text-align: center!important;padding: 1.5rem 1.5rem!important;">
                            <h2>{{ config('app.name') }} Login</h2>
                        </div>
                        <div style="font-size:17px;" >
                           <!--  @if(session()->has('success'))
                                <div class="card" style="color:green;">
                                    <div class="card-content">
                                        <strong>{{ Session::get('success')}}</strong>
                                    </div>
                                </div>
                            @endif -->
                            
                            @if(session()->has('error'))
                                <div class="card" style="color:red;"><!-- #f97c7c -->
                                    <div class="card-content">
                                        <strong>{{ Session::get('error')}}</strong>
                                    </div>
                                </div>
                            @endif
                            </div>    
                            <form action="{{url('/')}}/admin/login" method="post" class="form-material mb-1" id="myLogin">  
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <!--<img src="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_LOGO)}}" width="250">-->
                                    <input type="text" class="form-control" placeholder="Username" name="email" required="" >
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input type="Password" class="form-control" placeholder="Password" name="password" required="" >
                                </div>
                                <div class="form-group mb-0">
                                      <button type="submit" class="btn btn-success shadow-box btn-block waves-effect waves-light btn-lg" ><i class="ti-arrow-right float-xs-right"></i>Login</button>
                                    <!-- <a href="{{route('register')}}" class="btn btn-success">New User? Sign Up </a> -->
                                </div>
                            </form>
                    </div><!-- End sub_content -->
                </div>
                <div class="col-md-8">
                    <img src="{{URL::asset('public/app-assets/images/portrait/small/login.jpg')}}" alt="Admin Panel" style="width: 100%;">
                </div>
            </div>
        </div>

    
   
   
    </section><!-- End Header video -->
    <!-- End SubHeader ============================================ -->
<!-- COMMON SCRIPTS -->
<script src="{{URL::asset('public/login-assets/LOGIN/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{URL::asset('public/login-assets/LOGIN/js/common_scripts_min.js')}}"></script>
<script src="{{URL::asset('public/login-assets/LOGIN/js/functions.js')}}"></script>
<script src="{{URL::asset('public/login-assets/LOGIN/assets/validate.js')}}"></script>

<!-- SPECIFIC SCRIPTS -->
<script src="{{URL::asset('public/login-assets/LOGIN/js/video_header.js')}}"></script>
<script>
$(document).ready(function() {
    'use strict';
      HeaderVideo.init({
      container: $('.header-video'),
      header: $('.header-video--media'),
      videoTrigger: $("#video-trigger"),
      autoPlayVideo: true
    });    

});
</script>

</body>
</html>