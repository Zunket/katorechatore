@extends('layout.master')

@section('title')

{{ config('app.name') }}

@endsection

@section('content')
  <style type="text/css">
    #map {
        width: 100%;
        height: 300px;
    }

    .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

  </style>
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
      <!-- <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new"> -->
         <!--  <h3 class="content-header-title mb-0 d-inline-block">{{$title}}</h3> -->
          <!-- <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}/admin/restaurant_list">{{ strtoUpper(trans('constants.restaurant'))}} {{ strtoUpper(trans('constants.list'))}}</a>
                </li>
                <li class="breadcrumb-item"><a href="#">{{$title}}</a>
                </li>
              </ol>
            </div>
          </div> -->
       <!--  </div>
        
      </div> -->
      <div class="content-body">
        <section id="icon-tabs">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title"></h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                  <!-- <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div> -->
                </div>

                <div class="card-content collapse show">
                  <div class="card-body">
                    <form action="{{url('/')}}/admin/setting/setting_add_to_restaurants" enctype="multipart/form-data" method="post" class="icons-tab-steps wizard-notification" id="add_restaurant">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        @if(isset($data))
                        <input type="hidden" class="form-control" value="{{$data->id}}" name="id" >
                         @endif
                    <fieldset>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="eventName2">{{trans('constants.restaurant')}} {{trans('constants.name')}} <span style="color: red;">*</span></label>
                              <input type="text" name="name"  value="@if(isset($data->restaurant_name)){{$data->restaurant_name}}@endif" class="form-control" id="eventName2">
                            </div>
                             <div class="form-group">
                              <label for="email">Email <span style="color: red;">*</span></label>
                             <input type="text" name="email"  value="@if(isset($data->email)){{$data->email}}@endif" class="form-control" id="email">
                             
                            </div>

                            <div class="form-group">
                              <label for="email">Password <span style="color: red;">*</span></label>
                             <input type="text" name="password" value="@if(isset($data->org_password)){{$data->org_password}}@endif" class="form-control" id="password">
                             
                            </div>
                     
                             <div class="form-group">
                              <label for="content">Mobile number <span style="color: red;">*</span></label>
                             <input type="text" name="phone" id="phone"  value="@if(isset($data->phone)){{$data->phone}}@endif" class="form-control" id="details">
                              <span class="error_message" id="phone_error"></span>
                            </div>

                           <!--  <div class="form-group">
                              <label for="pass">Password :</label>
                             <input type="password" class="form-control" id="password">
                             
                            </div> -->

                            <!--  <div class="form-group">
                              <label for="pass">Conform Password :</label>
                             <input type="password" class="form-control" id="con_password">
                             
                            </div> -->

                           <!--  <div class="form-group">
                              <label for="status">{{trans('constants.status')}} <span style="color: red;">*</span></label>
                              <select class="c-select form-control" id="status" name="status">
                                <option value="1" @isset($data->status) @if($data->status == 1)'selected' @endif @endisset>{{trans('constants.active')}}</option>
                                <option value="0" @isset($data->status) @if($data->status == 0)'selected' @endif @endisset>{{trans('constants.inactive')}}</option>
                                <option value="Active">Active</option> -->
                              <!-- </select>
                            </div> --> 

                             <!--<div class="form-group">
                              <label for="eventName2">{{trans('constants.status')}}<span style="color: red;">*</span></label>
                             <label class="switch">
                              <input type="checkbox" name="status" value="1" 
                              @if(isset($data->status)){{($data->status==1)?"checked":"" }}@endif>                        
                              <span class="slider round"></span></label>
                           </div>-->

                            <div class="form-group">
                              <label for="status">{{trans('constants.cuisines')}} <span style="color: red;">*</span></label>
                              <select class="c-select form-control select2"  multiple="multiple" id="cuisines" name="cuisines[]" style="width: 652.406px;">
                                @if(isset($cuisines))
                                  @foreach($cuisines as $val)
                                    <option value="{{$val->id}}" @if(isset($cuisine_ids)) {{ (in_array($val->id,$cuisine_ids)) ? 'selected' : '' }} @endif>{{$val->name}}</option>
                                  @endforeach
                                @endif
                                <!-- <option value="Active">Active</option> -->
                              </select>
                            </div>

                            <div class="">
                              <label for="status">{{trans('constants.delivery_type')}} <span style="color: red;">*</span></label>
                              @php
                                if(isset($data->delivery_type)){
                                $delivery_type = json_decode($data->delivery_type);
                                if($delivery_type=='' || $delivery_type==0) $delivery_type=array();
                                }else{
                                  $delivery_type = array();
                                }
                              @endphp
                              <div class="form-check">
                                <label class="form-check-label" for="check1">
                                  <input type="checkbox" class="form-check-input" name="delivery_type[]" id="home_delivery" value="1" required class="chk form-control" @if(in_array(1, $delivery_type)) checked @endif>
                                  Home Delivery
                                  
                                </label>
                              </div>
                              <div class="form-check">
                                <label class="form-check-label" for="check2">
                                <input type="checkbox" class="form-check-input" name="delivery_type[]" id="pickup" value="2" class="chk form-control" @if(in_array(2, $delivery_type)) checked @endif>Pickup
                                 </label>
                              </div>
                              <div class="form-check">
                                <label class="form-check-label" for="check3">
                                <input type="checkbox" class="form-check-input" name="delivery_type[]" id="dining" value="3" class="chk form-control" @if(in_array(3, $delivery_type)) checked @endif>Dining
                              </label>
                            </div>
                              <br>
                              <span class="error_message" id="delivery_type_error"></span>
                            </div>
                            

                             <div class="form-group">
                            <label for="password-confirm">{{trans('constants.res_timing_weekday')}}</label>

                            <!-- <input id="everyday" type="checkbox" checked="" class="form-control" name="everyday" value="1"> -->
                        </div>
                         <div class="row  everyday ">
                           <!--  <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="hours_opening">Everyday</label>
                                    <input type="checkbox" class="chk form-control" checked="" name="day[ALL]" value="ALL">
                                </div>
                            </div> -->
                            <div class="col-xs-4" style="padding: 0 0 0 15px;">
                                <div class="form-group">
                                    <label for="hours_opening">{{trans('constants.res_opens')}}<span style="color: red;">*</span></label>

                                    <div class="input-group clockpicker">
                                      <input type="text" id="timepicker" name="opening_time" value="@if(isset($data->opening_time)){{$data->opening_time}}@else 00:00 @endif" width="276" />
                                       <!--  <input type="text" class="form-control" name="opening_time" value="@if(isset($data->opening_time)){{$data->opening_time}}@else 00:00 @endif" required=""> -->
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4" style="padding: 0 0 0 15px;">
                                <div class="form-group">
                                    <label for="hours_closing">{{trans('constants.res_close')}}<span style="color: red;">*</span></label>
                                    <div class="input-group clockpicker">
                                      <input type="text" id="timepicker1" name="closing_time" value="@if(isset($data->closing_time)){{$data->closing_time}}@else 00:00 @endif"  width="276" />
                                       <!--  <input type="text" class="form-control" name="closing_time" value="@if(isset($data->closing_time)){{$data->closing_time}}@else 00:00 @endif" required=""> -->
                                      
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">{{trans('constants.res_timing_weekend')}}<span style="color: red;">*</span></label>
                        </div>
                         <div class="row  everyday ">
                            <div class="col-xs-4" style="padding: 0 0 0 15px;">
                                <div class="form-group">
                                    <label for="hours_opening">{{trans('constants.res_opens')}}<span style="color: red;">*</span></label>

                                    <div class="input-group clockpicker">
                                       <!--  <input type="text" class="form-control" name="weekend_opening_time" value="@if(isset($data->weekend_opening_time)){{$data->weekend_opening_time}}@else 00:00 @endif" required=""> -->
                                        <input type="text" id="timepicker2"name="weekend_opening_time"value="@if(isset($data->weekend_opening_time)){{$data->weekend_opening_time}}@else 00:00 @endif" required="" width="276" />
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4" style="padding: 0 0 0 15px;">
                                <div class="form-group">
                                    <label for="hours_closing">{{trans('constants.res_close')}}<span style="color: red;">*</span></label>

                                    <div class="input-group clockpicker">
                                       <!--  <input type="text" class="form-control" name="weekend_closing_time" value="@if(isset($data->weekend_closing_time)){{$data->weekend_closing_time}}@else 00:00 @endif" required=""> -->
                                         <input type="text" id="timepicker3"name="weekend_closing_time"value="@if(isset($data->weekend_closing_time)){{$data->weekend_closing_time}}@else 00:00 @endif" width="276" />
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
   

                           <div class="form-group row">
                             <label class="col-md-3 label-control" for="projectinput4">{{trans('constants.restaurant')}} {{trans('constants.image')}}<span style="color: red;">*</span></label>

                            <div class="col-md-9">
                              @if(isset($data->image))
                              
                                @if(file_exists(RESTAURANT_UPLOADS_PATH.$data->image))                                
                                <img id="blah" src="{{BASE_URL.RESTAURANT_UPLOADS_PATH.$data->image}}" alt="your image"  style="max-width:180px;"><br>
                                @else
                                <img id="blah" src="{{BASE_URL.UPLOADS_PATH.PROFILE_ICON}}" alt="your image"  style="max-width:180px;"><br>
                               @endif 
                              @endif
                               <input type='file' name="image" onchange="readURL(this);"  style="padding:10px;background:000;" @if(!isset($data->image))  @endif>
                          </div>
                         </div>
                        
                          <h4>Offer Settings</h4>
                           
                          <div class="form-group">
                            <label for="percentage">Discount Type </label>
                              <select class="c-select form-control" id="status" name="discount_type">
                                <option value="1" @isset($data->discount_type) @if($data->discount_type==1) 'selected' @endif @endisset>Flat Offer</option>
                                <option value="2" @isset($data->discount_type) @if($data->discount_type==2) 'selected' @endif @endisset>Percentage Offer</option>
                              </select>
                             </div>
                             <div class="form-group">
                               <label for="percentage">Target Amount </label>
                                 <input type="text" class="form-control floating-label" name="target_amount" value="@if(isset($data->target_amount)){{$data->target_amount}}@else 0 @endif" >
                             </div>

                              <div class="form-group">
                               <label for="percentage">Offer Amount </label>
                                 <input type="text" class="form-control floating-label" name="offer_amount" value="@if(isset($data->offer_amount)){{$data->offer_amount}}@else 0 @endif">
                             </div>
                             
                          </div>


                          <div class="col-md-6">
                         
                            <div class="form-group">
                               <label for="amount">Packaging charge <span style="color: red;">*</span></label>
                                 <input type="number" class="form-control" id="amount" name="packaging_charge" value="@if(isset($data->packaging_charge)){{$data->packaging_charge}}@else 0 @endif">
                             </div>

                             <div class="form-group">
                              <label for="status">Select City <span style="color: red;">*</span></label>
                              <select class="c-select form-control" onchange="getcity_area()" id="city" name="city">
                              @foreach($city as $c)  
                                <option value="{{$c->id}}" @isset($data->city) @if($data->city==$c->id) selected @endif @endisset>{{$c->city}}</option>
                              @endforeach
                              </select>
                            </div>

                            <div class="form-group">
                              <label for="status">Select Area <span style="color: red;">*</span></label>
                              <select class="c-select form-control" id="area" name="area">
                               @foreach($area as $a)
                                <option value="{{$a->id}}" @isset($data->area) @if($data->area==$a->id) selected @endif @endisset>{{$a->area}}</option>
                              @endforeach
                              </select>
                            </div>
                          
                             <div class="form-group">
                              <label for="description">Description </label>
                              <textarea name="shop_description" id="description" rows="6" class="form-control" placeholder="Enter Description">@if(isset($data->shop_description)){{$data->shop_description}}@endif</textarea>
                             
                            </div>

                             <div class="form-group">
                               <label for="percentage">{{trans('constants.estimate_delivery_time')}} ({{trans('constants.mins')}}) <span style="color: red;">*</span></label>
                                 <input type="text" class="form-control" id="percentage" value="@if(isset($data->estimated_delivery_time)){{$data->estimated_delivery_time}}@endif" name="estimated_delivery_time" placeholder="15-25">
                             </div>
                            
                              <div class="form-group">
                              <label for="description">Address <span style="color: red;">*</span></label>
                                <input id="searchMapInput" name="address" class="form-control" type="text" placeholder="Enter a address" value="@if(isset($data->address)){{$data->address}}@endif">
                                <input type="hidden" id="latitude" name="latitude" value="@if(isset($data->lat)) {{ $data->lat }} @else {{ env('DEFAULT_LAT')}} @endif">
                                <input type="hidden" id="longitude" name="longitude" value="@if(isset($data->lng)) {{ $data->lng }} @else {{ env('DEFAULT_LNG')}} @endif">
                                <span id="address_err" class="text-danger"></span>
                              </div>
                              <div class="form-group">
                                <div id="map"></div>
                              </div>

                       <!-- <h4>{{trans('constants.restaurant')}} {{trans('constants.comission_setting')}}</h4>
                        <div class="form-group">
                          <label for="percentage">{{trans('constants.admin_commision')}} % </label>
                            <input type="text" class="form-control floating-label" name="admin_commision" value="@if(isset($data->admin_commision)){{$data->admin_commision}}@else 0 @endif">
                        </div>

                        <div class="form-group">
                          <label for="percentage">{{trans('constants.restaurant')}} {{trans('constants.delivery_charge')}} </label>
                            <input type="text" class="form-control floating-label" name="restaurant_delivery_charge" value="@if(isset($data->restaurant_delivery_charge)){{$data->restaurant_delivery_charge}}@else 0 @endif">
                        </div>

                        <div class="form-group">
                          <label for="percentage">{{trans('constants.driver_commission')}} % </label>
                            <input type="text" class="form-control floating-label" name="driver_commision" value="@if(isset($data->driver_commision)){{$data->driver_commision}}@else 0 @endif">
                        </div>
-->
                      </div>
                        </div>
 
                      <!--   <h3>{{trans('constants.bank_details')}}</h3> -->
                        <!-- <div class="row">
                          <div class="col-md-4">
                             <div class="form-group">
                              <label for="name">Account Name<span style="color: red;">*</span></label>
                              <input id="name" type="text" class="form-control" name="account_name" value="@if(isset($data->RestaurantBankDetails->account_name)){{$data->RestaurantBankDetails->account_name}}@endif">
                            </div>
                             </div>
                             <div class="col-md-4">
                             <div class="form-group">
                              <label for="name">Account Address<span style="color: red;">*</span></label>
                              <input id="name" type="text" class="form-control" name="account_address" value="@if(isset($data->RestaurantBankDetails->account_address)){{$data->RestaurantBankDetails->account_address}}@endif">
                            </div>
                             </div>
                             <div class="col-md-4">
                             <div class="form-group">
                              <label for="name">Account Number<span style="color: red;">*</span></label>
                              <input id="name" type="text" class="form-control" name="account_no" value="@if(isset($data->RestaurantBankDetails->account_no)){{$data->RestaurantBankDetails->account_no}}@endif">
                            </div>
                             </div>
                             
                        </div> -->
                        <!-- <div class="row">
                          <div class="col-md-4">
                             <div class="form-group">
                              <label for="name">Bank Name<span style="color: red;">*</span></label>
                              <input id="name" type="text" class="form-control" name="bank_name"  value="@if(isset($data->RestaurantBankDetails->bank_name)){{$data->RestaurantBankDetails->bank_name}}@endif">
                            </div>
                             </div>
                             <div class="col-md-4">
                             <div class="form-group">
                              <label for="name">Branch Name<span style="color: red;">*</span></label>
                              <input id="name" type="text" class="form-control" name="branch_name" value="@if(isset($data->RestaurantBankDetails->branch_name)){{$data->RestaurantBankDetails->branch_name}}@endif" >
                            </div>
                             </div>
                             <div class="col-md-4">
                             <div class="form-group">
                              <label for="name">Branch Address <span style="color: red;">*</span></label>
                              <input id="name" type="text" class="form-control" name="branch_address"  value="@if(isset($data->RestaurantBankDetails->branch_address)){{$data->RestaurantBankDetails->branch_address}}@endif">
                            </div>
                             </div>
                             
                        </div> -->
                        <!-- <div class="row">
                          <div class="col-md-6">
                             <div class="form-group">
                              <label for="name">Swift Code</label>
                              <input id="name" type="text" class="form-control" name="swift_code" value="@if(isset($data->RestaurantBankDetails->swift_code)){{$data->RestaurantBankDetails->swift_code}}@endif"  >
                            </div>
                             </div>
                             <div class="col-md-6">
                             <div class="form-group">
                              <label for="name">Routing Number</label>
                              <input id="name" type="text" class="form-control" name="routing_no" value="@if(isset($data->RestaurantBankDetails->routing_no)){{$data->RestaurantBankDetails->routing_no}}@endif"  >
                            </div>
                             </div>
                            
                             
                        </div> --> 

                        
                        @php //dd($data->Document); @endphp
                        @if(count($document)>0)
                          <h4>{{trans('constants.doc_upload')}}</h4>
                          @foreach($document->chunk(2) as $chunk)
                            <div class="row">
                              @foreach($chunk as $item)
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="description">{{$item->document_name}}</label>
                                    <input type="file" name="document[{{$item->id}}][document]" class="form-control">
                                    @if($item->expiry_date_needed==1)
                                      <label for="description">{{trans('constants.expiry_date')}}</label>
                                      <input type="text" name="document[{{$item->id}}][date]" class="form-control pickadate-selectors picker__input picker__input--active" @isset($data->Document) @foreach($data->Document as $val) @if($val->id==$item->id) value="@if($val->pivot->expiry_date!='0000-00-00') {{ date('d F, Y',strtotime($val->pivot->expiry_date))}} @endif" @endif @endforeach @endisset>
                                    @endif
                                  </div>
                                </div>
                              @endforeach
                            </div>
                          @endforeach
                        @endif
                        <div class="row">
                          <div class="col-md-6">
                            <!-- <div class="form-actions"> -->
                              <!--<button type="button" class="btn btn-warning mr-1" style="padding: 10px 15px;">
                                <i class="ft-x"></i> Cancel
                                </button>-->
                              <button class="btn btn-success mr-1" style="padding: 10px 15px;" onclick="javascript:form_validation();">
                                <i class="ft-check-square"></i> Save
                                </button>
                            <!-- </div> -->
                          </div>
                        </div>
                      </fieldset>
                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              </section>
            </div>
          </div>

          <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
          <script>
        $('#timepicker').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
    <script>
        $('#timepicker1').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
    <script>
        $('#timepicker2').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
    <script>
        $('#timepicker3').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
<script type="text/javascript">
      $(function() {
    $('#datetimepicker3').datetimepicker({
      pickDate: false
    });
  });

$(document).ready(function() {
  var city_id = $('#city').val();
  $.ajax({
    url : "{{url('/')}}/admin/getcity_area/"+city_id,
    method : "get",
    success : function (data)
    {
    console.log(data.area);
      if(data.area != '') 
      {
        var area='';
        $.each( data.area, function( key, value ) {
          area += '<option value="'+value.id+'">'+value.area+'</option>';
        });
        $('#area').html(area);
      }
      else
      {
          $('#area').html("");
      }
    }

  });
});
  
  function getcity_area()
        {
          var city_id = $('#city').val();
          $.ajax({
            url : "{{url('/')}}/admin/getcity_area/"+city_id,
            method : "get",
            success : function (data)
            {
            console.log(data.area);
              if(data.area != '') 
              {
                var area='';
                $.each( data.area, function( key, value ) {
                  area += '<option value="'+value.id+'">'+value.area+'</option>';
                });
                $('#area').html(area);
              }
              else
              {
                  $('#area').html("");
              }
            }

          });
        }
</script>
          
    <script type="text/javascript"
     src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
    </script>

<script src="{{URL::asset('public/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('public/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('public/app-assets/js/scripts/forms/select/form-select2.js')}}" type="text/javascript"></script>
<script>
	function form_validation(){

		var home_delivery = document.getElementById('home_delivery').checked;
	    var pickup = document.getElementById('pickup').checked;
	    var dining = document.getElementById('dining').checked;
	   	var phone = document.getElementById('phone').value;

	    if(!home_delivery && !pickup && !dining)
	    {
	    	$('#delivery_type_error').fadeIn().html('Please select any one of the delivery type').delay(3000).fadeOut('slow');
	    }else if(isNaN(phone))
	    {
	    	$('#phone_error').fadeIn().html('Please enter only numbers').delay(3000).fadeOut('slow');
	    }else
	    {
	    	document.getElementById("add_restaurant").submit();
	    }

	    // if(iserror == 0)
	    // {
	    //     document.getElementById("wizard").submit();
	    // }
	}
</script>
<script>
function initMap() {
    var lati = document.getElementById('latitude').value;
    var long = document.getElementById('longitude').value;
    var myLatlng = new google.maps.LatLng(Number(lati),Number(long));
    var geocoder = new google.maps.Geocoder();
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: Number(lati), lng: Number(long)},
      zoom: 13
    });
    //{{ ( (isset($data->mobile_number)) ? $data->mobile_number : '') }}
    var input = document.getElementById('searchMapInput');
   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
   
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
  
    var infowindow = new google.maps.InfoWindow();
     var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          draggable:true
         
        });

    autocomplete.addListener('place_changed', function() {
        //infowindow.close();
        marker.setVisible(true);
        var place = autocomplete.getPlace();
    
        /* If the place has a geometry, then present it on a map. */
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
      
        var address = '';
        if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
      
        document.getElementById('latitude').value = place.geometry.location.lat();
        document.getElementById('longitude').value = place.geometry.location.lng();
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
        
        //check rerstaurant address comes within area selected
        fun_check_restaurant(place.geometry.location.lat(), place.geometry.location.lng());

        /* Location details */
    });
        // draggabled address /* Start

        google.maps.event.addListener(marker, 'dragend', 
        function(marker){
        var latLng = marker.latLng; 
        currentLatitude = latLng.lat();
        currentLongitude = latLng.lng();

            geocoder.geocode({'latLng': latLng }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                    document.getElementById('searchMapInput').value = results[0].formatted_address;
                    document.getElementById('latitude').value = currentLatitude;
                    document.getElementById('longitude').value = currentLongitude;
                    infowindow.setContent('<div>' + results[0].formatted_address + '<br>');
                    infowindow.open(map, marker);
                    }
                }
            });
        }); 

        // draggabled address /* End
  
}

function fun_check_restaurant(lat=0, lng=0)
{
  var area_id = $('#area').val();
  if(lat==0 && lng==0){
    lat = $('#latitude').val();
    lng = $('#longitude').val();
  }
  if(area_id!='' && area_id!=null)
  {
    $.ajax({
      url : "{{url('/')}}/admin/check_restaurant_address",
      method : "post",
      data : {"_token": "{{ csrf_token() }}","area_id":area_id,"lat":lat,"lng":lng},
      success : function (data)
      {
        console.log(data);
        if(!data)
        {
          $("#address_err").html("Given address not within specified range!");
          $("#searchMapInput").val("");
        }else{
          $("#address_err").html("");
        }
      }

    });
  }else{
    $("#address_err").html("Please select Area!");
    $("#searchMapInput").val("");
  }
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{GOOGLE_API_KEY}}&libraries=places&callback=initMap" async defer></script>
      @endsection     
 