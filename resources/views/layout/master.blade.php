<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>{{ config('app.name') }}</title>
      <!--<link rel="icon" href="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_FAVICON)}}">-->
      <!-- <title>@yield('title')</title> -->
      <link rel="apple-touch-icon" href="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_FAVICON)}}">
      <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_FAVICON)}}">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
         rel="stylesheet">
      <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
         rel="stylesheet">
      <!-- BEGIN VENDOR CSS-->
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/vendors.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/forms/selects/select2.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/pickers/daterange/daterangepicker.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
      <!-- END VENDOR CSS-->
      <!-- BEGIN MODERN CSS-->
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/app.css')}}">
      <!-- END MODERN CSS-->
      <!-- BEGIN Page Level CSS-->
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/core/menu/menu-types/vertical-menu-modern.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/core/colors/palette-gradient.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/charts/morris.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/fonts/simple-line-icons/style.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/plugins/pickers/daterange/daterange.css')}}">
      <!-- END Page Level CSS-->
      <!-- BEGIN Custom CSS-->
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/assets/css/style.css')}}">
      <!-- END Custom CSS-->
      <!-- BEGIN Page Level CSS-->
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/view-orders.css')}}">
      <!-- END Page Level CSS-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
      <script type="text/javascript"
         src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
      <!--  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
   </head>
   <style type="text/css">
      .main-menu.menu-dark .navigation {
      background: {{MENU_COLOR}} !important;
      }
      .main-menu.menu-dark .navigation > li.open > a {
      background: {{HIGHLIGHT_COLOR}} !important;
      }
      .main-menu.menu-dark .navigation > li > ul {
      background: {{MENU_COLOR}}  !important;
      }
      .navbar-semi-dark .navbar-header {
      background: {{MENU_COLOR}}  !important;
      }
      .main-menu.menu-dark {
      background: {{MENU_COLOR}}  !important;
      }
      .error_message {
      color: red;
      }
   </style>
   <script> 
      function WebSocketTest() { 
               
         if ("WebSocket" in window) {
             console.log("WebSocket is supported by your Browser!");
             
             // Let us open a web socket
             var ws = new WebSocket("ws://{{$_SERVER['HTTP_HOST']}}:8083");
      
             ws.onopen = function() {
               console.log('Socket connected successfully!..');
             };
      
             ws.onmessage = function (evt) { 
               try {
                 var received_msg = JSON.parse(evt.data);
                 console.log("Message is received..."+evt.data);
                 let id = received_msg.msg;
                 console.log("Message is received..."+evt.data);
                 if (id == {{Session::get('userid')}}) {
                   // call the alarm function
                   // document.getElementById('mySound').play();
                   var audio = new Audio('{{url('/')}}/public/sound/to-the-point.mp3');
                   var playPromise = audio.play();
                   document.getElementById('notification').hidden = false;
                 }
               } catch (error) {
                   
               }
               
             };
      
             ws.onclose = function() { 
               console.log("Connection is closed..."); 
             };
      
         } else {
             console.log("WebSocket NOT supported by your Browser!");
         }
       }
      
       WebSocketTest();
   </script>
   <body class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar"
      data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
      <!-- fixed-top-->
      <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow navbar-semi-dark fixed-top navbar-shadow">
         <div class="navbar-wrapper">
            <div class="navbar-header">
               <ul class="nav navbar-nav flex-row">
                  <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                  <li class="nav-item mr-auto">
                     <a class="navbar-brand" href="{{url('/')}}/admin/dashboard" style="padding:0px;">
                     <img class="brand-logo" alt="{{ config('app.name') }}" src="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_LOGO)}}"  style="width: 190px;margin-left: 10px;margin-top: 7px;"> 
                     </a>
                  </li>
               </ul>
            </div>
            <div class="navbar-container content">
               <div class="collapse navbar-collapse" id="navbar-mobile">
                  <ul class="nav navbar-nav mr-auto float-left">
                  </ul>
                  <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-notification nav-item">
                      <form action="{{ url('admin/is_read') }}" id="isRead" method="POST">
                       {{ csrf_field() }}
                       </form>     
                       <a class="nav-link nav-link-label" href="{{URL('/')}}/admin/neworder_list" data-toggle="dropdown" onclick="event.preventDefault();
               document.getElementById('isRead').submit();"><i class="ficon ft-bell"></i>
                         <span class="badge badge-pill badge-default badge-danger badge-default badge-up badge-glow">{{TOTORDERCOUNT}}</span>
                       </a>
                     </li>
                     <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                        <span class="mr-1">{{trans('constants.welcome_title')}}
                        <span class="user-name text-bold-700">{{ucfirst(session()->get('user_name'))}}</span>
                        </span>
                        <span class="avatar avatar-online">
                        <img src="{{URL::asset('public/app-assets/images/portrait/small/avatar-s-19.png')}}" alt="avatar"><i></i></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{URL('/admin/setting/edit_store/'.session()->get('userid'))}}"><i class="ft-user"></i>Edit</a>
                <a class="dropdown-item" href="{{URL('/admin/setting/edit_account/'.session()->get('userid'))}}"><i class="ft-mail"></i>Account</a>
                <a class="dropdown-item" href="{{URL('/')}}/admin/neworder_list"><i class="ft-check-square"></i>Order</a>
                <!--<a class="dropdown-item" href="#"><i class="ft-message-square"></i> Chats</a>-->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{url('/')}}/admin/logout"><i class="ft-power"></i> {{trans('constants.logout_txt')}}</a>
              </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </nav>
      <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
         @include('includes.navBar')
      </div>
      <div class="app-content content">
         <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-12" style="padding:0 48px 0 48px;">
               @if(Session::has('error'))
               <p class="alert alert-danger" style="top:100%;">{{ Session::get('error') }}</p>
               @endif
               @if(Session::has('success'))
               <p class="alert alert-success" style="top: 100%;">{{ Session::get('success') }}</p>
               @endif
            </div>
            <div class="col-md-5">
               <br>
               <!--FOR NEW ORDER NOTIFICATION ALERT  -->
               <div class="card" id="notification">
                  <div class="card-content">
                     <div class="media align-items-stretch">
                        <div class="p-2 text-center bg-danger rounded-left">
                           <i class="fa fa-user-circle-o font-small-3 text-white"></i>
                        </div>
                        <div class="py-1 px-2 media-body">
                           <h5 class="danger">New Order Received. To view click <a href="{{url('/')}}/admin/neworder_list">Here</a> <a onclick="myFunction()" style="float: right"> x </a></h5>
                        </div>
                     </div>
                  </div>
               </div>
               <!--FOR NEW ORDER NOTIFICATION ALERT ENDS  -->
            </div>
         </div>
         <!--FOR NEW ORDER NOTIFICATION ALERT  -->
         <script type="text/javascript">
            document.getElementById('notification').hidden = true;
         </script>
         <!--FOR NEW ORDER NOTIFICATION ALERT ENDS -->
         @yield('content')
      </div>
      @include('includes.footer')
      @yield('script')
      <!--FOR NEW ORDER NOTIFICATION ALERT  -->
      <script>
         function myFunction() {
           document.getElementById('notification').hidden = true;
         }
      </script>
      <!--FOR NEW ORDER NOTIFICATION ALERT ENDS -->
