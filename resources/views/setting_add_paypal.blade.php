@extends('layout.master')

@section('title')

{{ config('app.name') }}

@endsection

@section('content')
  <style type="text/css">
    #map {
        width: 100%;
        height: 300px;
    }

    .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

  </style>
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
      <!-- <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new"> -->
         <!--  <h3 class="content-header-title mb-0 d-inline-block">{{$title}}</h3> -->
          <!-- <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}/admin/restaurant_list">{{ strtoUpper(trans('constants.restaurant'))}} {{ strtoUpper(trans('constants.list'))}}</a>
                </li>
                <li class="breadcrumb-item"><a href="#">{{$title}}</a>
                </li>
              </ol>
            </div>
          </div> -->
       <!--  </div>
        
      </div> -->
      <div class="content-body">
        <section id="icon-tabs">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title"></h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                  <!-- <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div> -->
                </div>

                <div class="card-content collapse show">
                  <div class="card-body">
                    <form action="{{url('/')}}/admin/setting/setting_add_to_paypal" enctype="multipart/form-data" method="post" class="icons-tab-steps wizard-notification" id="add_restaurant">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        @if(isset($data))
                        <input type="hidden" class="form-control" value="{{$data->id}}" name="id" >
                         @endif
                    <fieldset> 
                        <h3>Paypal Details</h3>
                        <div class="row">
                          <div class="col-md-6">
                             <div class="form-group">
                              <label for="name">Paypal Id<span style="color: red;">*</span></label>
                              <input id="name" type="text" class="form-control" name="paypal_id" value="@if(isset($data->RestaurantBankDetails->paypal_id)){{$data->RestaurantBankDetails->paypal_id}}@endif">
                            </div>
                             </div>
                           </div>  
                        <div class="row">
                          <div class="col-md-6">
                            <!-- <div class="form-actions"> -->
                              <!--<button type="button" class="btn btn-warning mr-1" style="padding: 10px 15px;">
                                <i class="ft-x"></i> Cancel
                                </button>-->
                              <button class="btn btn-success mr-1" style="padding: 10px 15px;" onclick="javascript:form_validation();">
                                <i class="ft-check-square"></i> Save
                                </button>
                            <!-- </div> -->
                          </div>
                        </div>
                      </fieldset>
                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              </section>
            </div>
          </div>

          <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
          <script>
        $('#timepicker').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
    <script>
        $('#timepicker1').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
    <script>
        $('#timepicker2').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
    <script>
        $('#timepicker3').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
<script type="text/javascript">
      $(function() {
    $('#datetimepicker3').datetimepicker({
      pickDate: false
    });
  });

$(document).ready(function() {
  var city_id = $('#city').val();
  $.ajax({
    url : "{{url('/')}}/admin/getcity_area/"+city_id,
    method : "get",
    success : function (data)
    {
    console.log(data.area);
      if(data.area != '') 
      {
        var area='';
        $.each( data.area, function( key, value ) {
          area += '<option value="'+value.id+'">'+value.area+'</option>';
        });
        $('#area').html(area);
      }
      else
      {
          $('#area').html("");
      }
    }

  });
});
  
  function getcity_area()
        {
          var city_id = $('#city').val();
          $.ajax({
            url : "{{url('/')}}/admin/getcity_area/"+city_id,
            method : "get",
            success : function (data)
            {
            console.log(data.area);
              if(data.area != '') 
              {
                var area='';
                $.each( data.area, function( key, value ) {
                  area += '<option value="'+value.id+'">'+value.area+'</option>';
                });
                $('#area').html(area);
              }
              else
              {
                  $('#area').html("");
              }
            }

          });
        }
</script>
          
    <script type="text/javascript"
     src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
    </script>

<script src="{{URL::asset('public/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('public/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('public/app-assets/js/scripts/forms/select/form-select2.js')}}" type="text/javascript"></script>
<script>
	function form_validation(){

		var home_delivery = document.getElementById('home_delivery').checked;
	    var pickup = document.getElementById('pickup').checked;
	    var dining = document.getElementById('dining').checked;
	   	var phone = document.getElementById('phone').value;

	    if(!home_delivery && !pickup && !dining)
	    {
	    	$('#delivery_type_error').fadeIn().html('Please select any one of the delivery type').delay(3000).fadeOut('slow');
	    }else if(isNaN(phone))
	    {
	    	$('#phone_error').fadeIn().html('Please enter only numbers').delay(3000).fadeOut('slow');
	    }else
	    {
	    	document.getElementById("add_restaurant").submit();
	    }

	    // if(iserror == 0)
	    // {
	    //     document.getElementById("wizard").submit();
	    // }
	}
</script>
<script>
function initMap() {
    var lati = document.getElementById('latitude').value;
    var long = document.getElementById('longitude').value;
    var myLatlng = new google.maps.LatLng(Number(lati),Number(long));
    var geocoder = new google.maps.Geocoder();
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: Number(lati), lng: Number(long)},
      zoom: 13
    });
    //{{ ( (isset($data->mobile_number)) ? $data->mobile_number : '') }}
    var input = document.getElementById('searchMapInput');
   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
   
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
  
    var infowindow = new google.maps.InfoWindow();
     var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          draggable:true
         
        });

    autocomplete.addListener('place_changed', function() {
        //infowindow.close();
        marker.setVisible(true);
        var place = autocomplete.getPlace();
    
        /* If the place has a geometry, then present it on a map. */
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
      
        var address = '';
        if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
      
        document.getElementById('latitude').value = place.geometry.location.lat();
        document.getElementById('longitude').value = place.geometry.location.lng();
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
        
        //check rerstaurant address comes within area selected
        fun_check_restaurant(place.geometry.location.lat(), place.geometry.location.lng());

        /* Location details */
    });
        // draggabled address /* Start

        google.maps.event.addListener(marker, 'dragend', 
        function(marker){
        var latLng = marker.latLng; 
        currentLatitude = latLng.lat();
        currentLongitude = latLng.lng();

            geocoder.geocode({'latLng': latLng }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                    document.getElementById('searchMapInput').value = results[0].formatted_address;
                    document.getElementById('latitude').value = currentLatitude;
                    document.getElementById('longitude').value = currentLongitude;
                    infowindow.setContent('<div>' + results[0].formatted_address + '<br>');
                    infowindow.open(map, marker);
                    }
                }
            });
        }); 

        // draggabled address /* End
  
}

function fun_check_restaurant(lat=0, lng=0)
{
  var area_id = $('#area').val();
  if(lat==0 && lng==0){
    lat = $('#latitude').val();
    lng = $('#longitude').val();
  }
  if(area_id!='' && area_id!=null)
  {
    $.ajax({
      url : "{{url('/')}}/admin/check_restaurant_address",
      method : "post",
      data : {"_token": "{{ csrf_token() }}","area_id":area_id,"lat":lat,"lng":lng},
      success : function (data)
      {
        console.log(data);
        if(!data)
        {
          $("#address_err").html("Given address not within specified range!");
          $("#searchMapInput").val("");
        }else{
          $("#address_err").html("");
        }
      }

    });
  }else{
    $("#address_err").html("Please select Area!");
    $("#searchMapInput").val("");
  }
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{GOOGLE_API_KEY}}&libraries=places&callback=initMap" async defer></script>
      @endsection     
 