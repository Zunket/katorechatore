@extends('layout.master')

@section('title')
{{APP_NAME}}
@endsection

@section('content')
<div class="content-wrapper">
  <div class="content-body">
    <section id="icon-tabs">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <!-- <h4 class="card-title">CREATE DRIVER</h4> -->
              <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
              <!-- <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                  <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                  <li><a data-action="close"><i class="ft-x"></i></a></li>
                </ul>
              </div> -->
            </div>
           

            <div class="card-content collapse show">
              <div class="card-body">
                <form role="form" action="{{url('/')}}/admin/add_driver" class="icons-tab-steps wizard-notification" method="post" enctype="multipart/form-data" id="add_driver">
                  <input type="hidden" name="_token" value="{{csrf_token()}}">
                  @if(isset($insert1))
                  <input type="hidden" name="id" value="{{($insert1->id ?$insert1->id:'')}}"  >
                  @endif

                  <fieldset>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Driver Name<span style="color: red;">*</span></label>
                          <input id="name" type="text" class="form-control" name="driver_name" value="@if(isset($insert1->name)){{$insert1->name}}@endif" required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Email</label>
                          <input id="name" type="email" class="form-control" name="email" value="@if(isset($insert1->email)){{$insert1->email}}@endif" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label  for="projectinput4">Status<span style="color: red;">*</span></label>
                          <select name="status" id="" class="form-control" required="">
                            <option value="1">Active</option>
                            <option value="2">In Active</option>
                          </select> 
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Phone No<span style="color: red;">*</span></label>
                          <input id="name" type="text" class="form-control" name="phone_no" value="@if(isset($insert1->phone)){{$insert1->phone}}@endif" required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Address Line 1<span style="color: red;">*</span></label>
                          <input id="name" type="text" class="form-control" name="address_line_1" value="@if(isset($insert1->Deliverypartner_detail->address_line_1)){{$insert1->Deliverypartner_detail->address_line_1}}@endif" required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Address Line 2</label>
                          <input id="name" type="text" class="form-control" name="address_line_2" value="@if(isset($insert1->Deliverypartner_detail->address_line_2)){{$insert1->Deliverypartner_detail->address_line_2}}@endif"  autofocus="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Country<span style="color: red;">*</span></label>
                          <select class="c-select form-control" id="country" name="country" required onchange="getprovience(1)">
                            <option value="">--select country--</option>
                            @foreach($country as $c)
                            <option value="{{$c->id}}" @isset($insert1->Deliverypartner_detail->country) @if($insert1->Deliverypartner_detail->country==$c->id) selected @endif @endisset>{{$c->country}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">State/Province<span style="color: red;">*</span></label>
                          <select class="c-select form-control" id="state_province" name="state_province" required onchange="getprovience(2)">
                            @if(!empty($state))
                              <option value="{{$state->id}}" @isset($insert1->Deliverypartner_detail->state_province) @if($insert1->Deliverypartner_detail->state_province==$state->id) selected @endif @endisset>{{$state->state}}</option>
                            @endif
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                        <label for="name">City<span style="color: red;">*</span></label>
                          <select class="c-select form-control" id="city" name="city" required>
                            @if(!empty($city))
                              <option value="{{$city->id}}" @isset($insert1->Deliverypartner_detail->city) @if($insert1->Deliverypartner_detail->city==$city->id) selected @endif @endisset>{{$city->city}}</option>
                            @endif
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Zip Code<span style="color: red;">*</span></label>
                          <input id="name" type="text" class="form-control" name="zip_code" value="@if(isset($insert1->Deliverypartner_detail->zip_code)){{$insert1->Deliverypartner_detail->zip_code}}@endif" required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Avatar <span style="color: red;">{{@$insert1?'':'*'}}</span></label>
                            <!--@if(@$insert1->profile_pic)
                              
                                <img id="blah" src="{{BASE_URL.UPLOADS_PROFILE.$insert1->profile_pic}}" alt="your image"  style="max-width:180px;"><br>
                            @else
                                <img id="blah" src="{{BASE_URL.UPLOADS_PATH.PROFILE_ICON}}" alt="your image"  style="max-width:180px;"><br>
                               
                            @endif-->
                          <input id="name" type="file" class="form-control" name="profile_pic" value="@if(isset($insert1->profile_pic)){{$insert1->profile_pic}}@endif" @if(!isset($insert1)) required="" @endif autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">About</label>
                          <textarea name="about"  rows="4" class="form-control" placeholder="About"></textarea>
                        </div>
                      </div>
                    </div>

                    <h3>Document Settings</h3>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="name">License @if(!isset($insert1))<span style="color: red;">*</span>@endif</label>
                          <input id="name" type="file" class="form-control" name="license" value="@if(isset($insert1->profile_pic)){{$insert1->profile_pic}}@endif" @if(!isset($insert1)) required="" @endif autofocus="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="name">Expiry Date<span style="color: red;">*</span></label>
                          <input id="license_expiry" type="text" class="form-control picker__input  picker__input--active" name="license_expiry" value="@if(isset($insert1->expiry_date)){{$insert1->expiry_date}}@endif" required=""  autofocus="">
                      </div>
                    </div>
                    </div>
                    <h3>Account Settings</h3>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Account Name<span style="color: red;">*</span></label>
                          <input id="name" type="text" class="form-control" name="account_name" value="@if(isset($insert1->Deliverypartner_detail->account_name)){{$insert1->Deliverypartner_detail->account_name}}@endif" required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Account Address<span style="color: red;">*</span></label>
                          <input id="name" type="text" class="form-control" name="account_address" value="@if(isset($insert1->Deliverypartner_detail->account_address)){{$insert1->Deliverypartner_detail->account_address}}@endif"  required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Account Number<span style="color: red;">*</span></label>
                          <input id="name" type="text" class="form-control" name="account_no" value="@if(isset($insert1->Deliverypartner_detail->account_no)){{$insert1->Deliverypartner_detail->account_no}}@endif" required="" autofocus="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Bank Name<span style="color: red;">*</span></label>
                          <input id="name" type="text" class="form-control" name="bank_name"  value="@if(isset($insert1->Deliverypartner_detail->bank_name)){{$insert1->Deliverypartner_detail->bank_name}}@endif" required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Branch Name<span style="color: red;">*</span></label>
                          <input id="name" type="text" class="form-control" name="branch_name" value="@if(isset($insert1->Deliverypartner_detail->branch_name)){{$insert1->Deliverypartner_detail->branch_name}}@endif" required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="name">Branch Address <span style="color: red;">*</span></label>
                          <input id="name" type="text" class="form-control" name="branch_address"  value="@if(isset($insert1->Deliverypartner_detail->branch_address)){{$insert1->Deliverypartner_detail->branch_address}}@endif" required="" autofocus="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="name">Swift Code</label>
                          <input id="name" type="text" class="form-control" name="swift_code" value="@if(isset($insert1->Deliverypartner_detail->swift_code)){{$insert1->Deliverypartner_detail->swift_code}}@endif"  autofocus="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="name">Routing Number</label>
                          <input id="name" type="text" class="form-control" name="routing_no" value="@if(isset($insert1->Deliverypartner_detail ->routing_no)){{$insert1->Deliverypartner_detail ->routing_no}}@endif"  autofocus="">
                        </div>
                      </div>
                    </div>
                    @if(!isset($insert1))
                    <h3>Security Settings</h3>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="password">Password<span style="color: red;">*</span></label>
                          <input id="password" type="password" class="form-control" name="password"  value="@if(isset($insert1->password)){{$insert1->password}}@endif" required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="confirm_password">Confirm Password<span style="color: red;">*</span></label>
                          <input id="confirm_password" type="password" class="form-control" value="" required="" autofocus="">
                        </div>
                      </div>
                    </div>
                    @endif
                    <span class="error_message" id="password_error"></span>
                    <h3>{{trans('constants.vehicle')}} Settings</h3>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="vehname">{{trans('constants.vehicle')}} {{trans('constants.name')}}<span style="color: red;">*</span></label>
                          <input id="vehname" type="text" class="form-control" name="vehicle_name" value="@if(isset($insert1->Vehicle->vehicle_name)){{$insert1->Vehicle->vehicle_name}}@endif" required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="vehicle_no">{{trans('constants.vehicle_no')}}<span style="color: red;">*</span></label>
                          <input id="vehicle_no" type="text" class="form-control" name="vehicle_no" value="@if(isset($insert1->Vehicle->vehicle_no)){{$insert1->Vehicle->vehicle_no}}@endif" required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="insurance_no">{{trans('constants.insurance_no')}}<span style="color: red;">*</span></label>
                          <input id="insurance_no" type="text" class="form-control" name="insurance_no" value="@if(isset($insert1->Vehicle->insurance_no)){{$insert1->Vehicle->insurance_no}}@endif" required="" autofocus="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="rc_no">{{trans('constants.rc_no')}}<span style="color: red;">*</span></label>
                          <input id="rc_no" type="text" class="form-control" name="rc_no" value="@if(isset($insert1->Vehicle->rc_no)){{$insert1->Vehicle->rc_no}}@endif"  required="" autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="insurance_expiry_date">{{trans('constants.insurance')}} {{trans('constants.expiry_date')}}<span style="color: red;">*</span></label>
                          <input id="insurance_expiry_date" type="text" class="form-control pickadate-selectors picker__input" name="insurance_expiry_date" value="@if(isset($insert1->Vehicle->insurance_expiry_date)){{$insert1->Vehicle->insurance_expiry_date}}@endif" required="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="rc_expiry_date">{{trans('constants.rc')}} {{trans('constants.expiry_date')}}<span style="color: red;">*</span></label>
                          <input id="rc_expiry_date" type="text" class="form-control pickadate-selectors picker__input" name="rc_expiry_date" value="@if(isset($insert1->Vehicle->rc_expiry_date)){{$insert1->Vehicle->rc_expiry_date}}@endif" required="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          @if(isset($insert1->Vehicle->insurance_image))
                              <!-- @if(file_exists(BASE_URL.DRIVER_IMAGE_PATH.$insert1->Vehicle->insurance_image)) -->
                              <img id="blah" src="{{BASE_URL.DRIVER_IMAGE_PATH.$insert1->Vehicle->insurance_image}}" alt="your image"  style="max-width:180px;"><br>
                              @else
                              <img id="blah" src="{{BASE_URL.UPLOADS_PATH.PROFILE_ICON}}" alt="your image"  style="max-width:180px;"><br>
                              <!-- @endif -->
                            @endif
                          <label for="insurance_image">{{trans('constants.insurance_image')}} @if(!isset($insert1)) <span style="color: red;">*</span>@endif</label>
                          <input id="insurance_image" type="file" class="form-control" name="insurance_image" value="" @if(!isset($insert1)) required="" @endif autofocus="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          @if(isset($insert1->Vehicle->rc_image))
                           <!--  @if(file_exists(BASE_URL.DRIVER_IMAGE_PATH.$insert1->Vehicle->rc_image)) -->
                            <img id="blah" src="{{BASE_URL.DRIVER_IMAGE_PATH.$insert1->Vehicle->rc_image}}" alt="your image"  style="max-width:180px;"><br>
                            @else
                            <img id="blah" src="{{BASE_URL.UPLOADS_PATH.PROFILE_ICON}}" alt="your image"  style="max-width:180px;"><br>
                            <!-- @endif -->
                          @endif
                          <label for="rc_image">{{trans('constants.rc_image')}} @if(!isset($insert1)) <span style="color: red;">*</span>@endif</label>
                          <input id="rc_image" type="file" class="form-control" name="rc_image" value="" @if(!isset($insert1)) required="" @endif autofocus="">
                        </div>
                      </div>
                    </div>
                    <div class="form-actions">
                      <button onclick="javascript:return form_validation();" class="btn btn-success mr-1" style="padding: 10px 15px;">
                        <i class="ft-check-square"></i> Save
                      </button>
                      <a href="{{url('/')}}/admin/deliverypeople_list" class="btn btn-danger mr-1" style="padding: 10px 15px;" role="button"><i class="ft-x"></i> Cancel</a>
                      
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>


@endsection     

@section('script')

<script>
  $('#license_expiry').pickadate({
    min: new Date(),
    selectYears: true,
    selectMonths: true
  });

  function form_validation()
  {
    var password = document.getElementById('password').value;
    var confirm_password = document.getElementById('confirm_password').value;
    if(password != confirm_password)
    {
      $('#password_error').fadeIn().html('Password and Confirm Password does not match').delay(3000).fadeOut('slow');
      return false;
    }
    // else
    // {
    //   document.getElementById("add_driver").submit();
    // }
  }

  function getprovience(id)
  {
    if(id==1)
      var provienceid = $('#country').val();
    else
     var provienceid = $("#state_province").val();

    $.ajax({
      url : "{{url('/')}}/admin/getprovience/"+provienceid+"/"+id,
      method : "get",
      success : function (data)
      {
        if(id==1){
          var state='<option value="">--select state--</option>';
          if(data.state != '') 
          {
            $.each( data.state, function( key, value ) {
              state += '<option value="'+value.id+'">'+value.state+'</option>';
            });
          }
          $('#state_province').html(state);

        }else{
          var city='<option value="">--select city--</option>';
          if(data.city != '') 
          {
            $.each( data.city, function( key, value ) {
              city += '<option value="'+value.id+'">'+value.city+'</option>';
            });
          }
          $('#city').html(city);
        }
      }

    });
  }
</script>

@endsection