@extends('layout.master')

@section('title')

{{ config('app.name') }}

@endsection

@section('content')

     
   <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
          <div class="row">
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!-- <div class="p-2 text-center bg-danger rounded-left">
                             <i class="fa fa-user-circle-o font-large-2 text-white"></i> 
                        </div> -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All Report</h5>
                            <h5 class="text-bold-400">{{$total_restaurant_report }}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="media align-items-stretch">
                          <!-- <div class="p-2 text-center bg-success rounded-left">
                               <i class="fa fa-motorcycle font-large-2 text-white"></i> 
                          </div> -->
                          <div class="py-1 px-2 media-body">
                              <h5 class="success">Total Admin Commision</h5>
                              <h5 class="text-bold-400">${{$total_admin_commision }}</h5>
                              <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                              </div> -->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-xl-4 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!--  -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">Total Rastaurant Commision</h5>
                            <h5 class="text-bold-400">${{$total_restaurant_commision}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
          <!-- <h3 class="content-header-title mb-0 d-inline-block">{{strtoUpper(trans('constants.order'))}} {{strtoUpper(trans('constants.report'))}}</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}/admin/dashboard">{{strtoUpper(trans('constants.dashboard'))}}</a></li>
                <li class="breadcrumb-item"><a href="#">{{strtoUpper(trans('constants.order'))}} {{strtoUpper(trans('constants.report'))}}</a>
                </li>
               
              </ol>
            </div>
          </div> -->
        </div>
        
      </div>
      <!-- <div class="card-content collapse show">
        <div class="card-body">
        <form method="post" action="{{url('/')}}/admin/restaurant_report_filter">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          
                             
                            <div class="row">
                            <div class="col-md-4">
                             <div class="form-group">
                              <label for="email">From <span style="color: red;">*</span></label>
                             <input type="date" name="start" id="start" class="form-control">
                             
                            </div>
                          </div>
                              <div class="col-md-4">
                             <div class="form-group">
                              <label for="email">To <span style="color: red;">*</span></label>
                             <input type="date" name="end" id="end"   class="form-control">
                             </div>
                            </div>
                          

        

        <div class="col-xs-1">
           <div class="col-md-6 col-md-offset-3"> 
            <button type='submit' class='btn btn-info'> <i class="fa fa-search" aria-hidden="true"></i> Search </button>
               </div>
        </div>
      </form>
    </div>
  </div>
    </div><br> -->
      <div class="content-body">
        <!-- Basic form layout section start -->


         
       
      <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  
                  <h4 class="card-title"></h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                  <!-- <div class="heading-elements">
                     <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      </ul>
                      </div>
                      
                    </div> -->
                </div>


               <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <div class="table-responsive">
                   <!--  <table id="orderwise_report" class="table table-striped table-bordered zero-configuration"> -->
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead> 
                          <tr>
                           
                            <th>Order Id</th>
                            <th>Customer Name</th>
                            <th>Customer Phone</th>
                            <th>Delivery Boy Name</th>
                            <th>Delivery Boy Phone</th>
                            <th>Store Name</th>
                            <th>Order Amount</th>
                            <th>Tax</th>
                            <th>Offer Discount</th>
                            <th>Admin Comission</th>
                            <th>Delivery Boy Comission</th>
                            <th>Store Commission</th>
                            <!-- <th>Status</th> -->
                          </tr>
                        </thead>
                        <tbody>
                           @foreach($restaurant_details as $restaurant)
                            <tr>
                            <td>{{$restaurant->order_id}}</td>
                            <td>{{$restaurant->customer_name}}</td>
                            <td>{{$restaurant->phone}}</td>
                            <td>{{$restaurant->delivery_boy_name}}</td>
                            <td>{{$restaurant->delivery_boy_phone}}</td>
                            <td>{{$restaurant->restaurant_name}}</td>
                            <td>${{$restaurant->item_total}}</td>
                            <td>${{$restaurant->tax}}</td>
                            <td>${{$restaurant->offer_discount}}</td>
                            <td>${{$restaurant->admin_commision}}</td>
                            <td>${{$restaurant->delivery_boy_commision}}</td>
                            <td>${{$restaurant->restaurant_commision}}</td>
                           
                          </tr>
                         
                         @endforeach 
                         
                          
                      </tbody>

                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      
        <!-- // Basic form layout section end -->
      </div>
    </div>
  </div>
  <script>
           
  $(document).ready(function(){
      
      var table = $('#orderwise_report').DataTable({
          dom: "lBfrtip",
           buttons: [
        {
             extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'A3',
                 
                customize: function (doc) { doc.defaultStyle.fontSize =7.2;  doc.styles.tableHeader.fontSize = 10; }

            },
             'excel','csv','print','copy',
            ],
          paging: true,
          //pageLength: 10,
          "searching": true,
          "ordering": true,
          "info": true,
          "lengthChange": true,
          "bProcessing": true,
          "bServerSide": true,
          "sAjaxSource": "orderwise_report_pagination",
          
          // "sAjaxSource": function ( data, callback, settings ) {

          //     $.ajax({
          //         url: 'restaurant_report_pagination',
          //         dataType:"json",
          //         type: 'post',
          //         contentType: 'application/x-www-form-urlencoded',
          //         data: {
          //             "_token": "{{ csrf_token() }}",
          //             //RecordsStart: data.start,
          //             //PageSize: data.length
          //         },
          //         success: function( data, textStatus, jQxhr ){
          //             console.log(data);
          //             callback({
          //                 data: data.Data,
          //                 recordsTotal:  data.TotalRecords,
          //                 recordsFiltered:  data.RecordsFiltered
          //             });
          //         },
          //         error: function( jqXhr, textStatus, errorThrown ){
          //         }
          //     });
          // },
          columns: [
              { data: "id" },
              { data: "order_id" },
              { data: "customer_name" },
              { data: "customer_phone" },
              { data: "delivery_boy_name" },
              { data: "delivery_boy_phone" },
              { data: "restaurant_name" },
              { data: "item_total" },
              { data: "tax" },
              { data: "offer_discount" },
              { data: "admin_commision" },
              { data: "delivery_boy_commision" }, 
              { data: "restaurant_commision" },
              { data: "status" },
          ]
          

      });



    });

</script>
  @endsection
 

 


