<!DOCTYPE html>
<head>
	
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Maven+Pro" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<html>
<body style="/* padding-left: 30%;padding-right: 10%;      width: 60%;margin: 0px;*/">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="35%" align="center" valign="top">
      <section style="background-image: url('{{BASE_URL}}{{UPLOADS_EMAIL_PATH}}header_bg.png');padding: 20px 0px 100px 30px;background-repeat: no-repeat;background-position: right;background-size: cover;">
		<div style="width: 50%;max-width: 50%;display: inline-block;">
			<img src="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_LOGO)}}" alt="{{APP_NAME}} Logo" style="width: 170px;">
		</div>
		
		
		<p style="font-family: 'Source Sans Pro', sans-serif;font-size: 20px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.2;letter-spacing: 0.8px;text-align: left;color: #686868;">Here's your My Food OTP : <br>
			<h2 style="text-align: left">{{$data}}</h2>
		</p>
		
	</section>
	</td>
  </tr>
</table>

</body>
</html>