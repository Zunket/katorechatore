@extends('layout.master')

@section('title')

{{APP_NAME}}

@endsection

@section('content')
<style type="text/css">
   .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

  #top-list{
  list-style-type: none;
  list-style: none;
}
</style>
  <div class="content-wrapper">
      <!-- <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new"> -->
          
          <!-- <h3 class="content-header-title mb-0 d-inline-block">{{strtoUpper(trans('constants.addon'))}}</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}/admin/dashboard">{{strtoUpper(trans('constants.dashboard'))}}</a>
                </li><li class="breadcrumb-item"><a href="#">{{strtoUpper(trans('constants.addon'))}} {{strtoUpper(trans('constants.list'))}}</a>
                </li>
              </ol>
            </div>
          </div> -->
       <!--  </div>
      </div> -->
      <div class="row">
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!-- <div class="p-2 text-center bg-danger rounded-left">
                             <i class="fa fa-user-circle-o font-large-2 text-white"></i> 
                        </div> -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All Addons</h5>
                            <h5 class="text-bold-400">{{$total_addon}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="media align-items-stretch">
                          <!-- <div class="p-2 text-center bg-success rounded-left">
                               <i class="fa fa-motorcycle font-large-2 text-white"></i> 
                          </div> -->
                          <div class="py-1 px-2 media-body">
                              <h5 class="success">Active Addons</h5>
                              <h5 class="text-bold-400">{{$active_addon}}</h5>
                              <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                              </div> -->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-xl-4 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!--  -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">Inactive Addons</h5>
                            <h5 class="text-bold-400">{{$inactive_addon}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      <div class="content-body">
        <!-- Basic form layout section start -->


        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header">
                    <h4 class="card-title">
                      </h4>
                  <h4 class="card-title"></h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                  <div class="heading-elements">
                     <ul class="list-inline mb-0">
                      <!-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li> -->
                      @if(session()->get('role')!=1)
                        <!-- <li> <button class="btn btn-primary btn-sm"><i class="ft-plus white"></i><a style=" color: white !important;" href="{{url('/')}}/admin/add_addons"> {{trans('constants.add')}} {{trans('constants.addon')}}</a></button></li> -->
                      @endif
                      </ul>
                      </div>
                      
                    </div>
                </div>


               <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead> 
                          <tr>
                           <th>{{trans('constants.sno')}}</th>
                            @if(session()->get('role')==1)
                              <th>{{trans('constants.restaurant')}} {{trans('constants.name')}}</th>
                            @endif
                            <th>{{trans('constants.name')}}</th>
                            <th>{{trans('constants.price')}}</th>
                            <th>{{trans('constants.status')}}</th>
                            @if(session()->get('role')!=1)
                              <th>{{trans('constants.action')}}</th>
                            @endif
                          </tr>
                      </thead>
                        <tbody>
                          @php $i=1; @endphp
                          @foreach($addons_list as $value)
                            <tr>
                              <td>{{$i}}</td>
                              @if(session()->get('role')==1)
                                <td>@if(!empty($value->Restaurant)) {{$value->Restaurant->restaurant_name}} @endif</td>
                              @endif
                              <td>{{$value->name}}</td>
                              <td>${{$value->price}}</td>
                               <td> 
                             @if($value->status == 1)
                              <label class="switch">
                                   <input type="checkbox" name="status" onclick="javascript:window.location.href='{{url('/')}}/admin/addon_status_disable/{{$value->id}}'" value="1" checked>                       
                              <span class="slider round"></span></label>
                              @else
                              <label class="switch">
                                   <input type="checkbox" name="status" onclick="javascript:window.location.href='{{url('/')}}/admin/addon_status_enable/{{$value->id}}'" value="1">                       
                              <span class="slider round"></span></label>
                              @endif
                            </td>
                              @if(session()->get('role')!=1)
                                <td>
                                  <a href="{{url('/')}}/admin/edit_addons/{{$value->id}}" class="button btn btn-icon btn-success mr-1 link_clr shadow-box"><i class="ft-edit"></i></a>
                                  <button type="button" class="btn btn-icon btn-danger mr-1 link_clr shadow-box" data-id="1" data-toggle="modal"  data-target="#delete{{$value->id}}"><i class="ft-delete"></i></button></li> 
                                </td>
                              @endif
                            </tr>
                            <!----------  Delete Cuisine Partner Model --------------------->

                                  <div class="modal animated slideInRight text-left" id="delete{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                                       <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                         <div class="modal-header">
                                          <h4 class="modal-title" id="myModalLabel76">Delete Addons</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                         </div>
                                         <form method="post" action="{{url('/')}}/admin/delete_addon">
                                            <div class="modal-body">
                                              
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                  <input type="hidden" name="id" value="{{$value->id}}">
                                                 <div class="form-group">
                                                    <label for="eventName2">Are you sure to delete  : {{$value->name}}</label>
                                                  </div>
                                            
                                            </div>
                                             <div class="modal-footer">
                                              <button type="submit" class="btn btn-black mr-1" style="padding: 10px 15px;">
                                                  <i class="ft-check-square"></i> Delete
                                                 </button>
                                                <button type="button" class="btn btn-danger mr-1" data-dismiss="modal" style="padding: 10px 15px;">
                                                   <i class="ft-x"></i> Cancel
                                                </button>
                                                 
                                            </div>
                                          </form>
                                      </div>
                                    </div>
                                  </div>
                                   <!----------  Delete Cuisine Model Ends--------------------->
                          @php $i=$i+1; @endphp
                          @endforeach
                        </tbody>
                   
                        <tfoot>
                          <tr>
                            <th>{{trans('constants.sno')}}</th>
                            <th>{{trans('constants.name')}}</th>
                            <th>{{trans('constants.price')}}</th>
                            <th>{{trans('constants.status')}}</th>
                            @if(session()->get('role')!=1)
                            <th>{{trans('constants.action')}}</th>
                            @endif
                          </tr>
                         </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- // Basic form layout section end -->
      </div>
  
    @endsection     
 