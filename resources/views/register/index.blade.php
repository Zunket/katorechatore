<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Ansonika">
    <title>{{ config('app.name') }}</title>
    <!-- BASE CSS -->
    <link href="{{URL::asset('public/login-assets/LOGIN/css/base.css')}}" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_FAVICON)}}">
     
</head>
<style type="text/css">
    .btn.btn-submit {
    width: 100%;
    margin-top: 30px;
    color: #191b19;
    padding: 10px;
    background: #fff;
    font-weight: 600;
    outline: 0;
    -webkit-transition: all .2s ease;
    transition: all .2s ease;
    font-size: 16px;
}
</style>

<body style="background-color: #4CAF50;">
    <section>
     
        <div class="col-md-12" >
<div class="modal-popup col-md-6 col-md-offset-3 box-shadow" style="margin-top:115px;border-radius: 0px;box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);">
            <h3><B> Restaurant Partner</B></h3>
            <hr/>
           
             <div style="font-size:14px;" >
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                  @if(session()->has('success'))
                        <div class="card" style="color:green;"><!-- gainsboro -->
                            <div class="card-content">
                                <strong>{{ Session::get('success')}}</strong>
                            </div>
                        </div>
                    @endif
                    
                    @if(session()->has('error'))
                        <div class="card" style="color:red;"><!-- #f97c7c -->
                            <div class="card-content">
                                <strong>{{ Session::get('error')}}</strong>
                            </div>
                        </div>
                    @endif
                   </div>    
                    <form action="{{route('register.post')}}" method="post" class="form-horizontal" id="myLogin" style="margin-top: 25px!important">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="restaurant_name">Restaurant Name:</label>
                          <div class="col-sm-8">
                            <input type="test" name="restaurant_name" required="required" value="{{old('restaurant_name')}}" placeholder="Restaurant Name" class="form-control" id="restaurant_name"/>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Email:</label>
                          <div class="col-sm-8">
                            <input type="email" name="email" required="required" value="{{old('email')}}" placeholder="Email" class="form-control" id="email"/>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="password">Password</label>
                          <div class="col-sm-8">
                            <input type="test" name="password" required="required" placeholder="Password" class="form-control" id="password"/>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="password_confirmation">Confirm Password</label>
                          <div class="col-sm-8">
                            <input type="test" name="password_confirmation" required="required" placeholder="Confirm Password" id="password_confirmation" class="form-control"/>
                          </div>
                        </div>

                          <div class="form-group">
                          <label class="control-label col-sm-3" for=""></label>
                          <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary btn-block" style="width: 100%;">Sign Up</button>
                          </div>
                          <div class="col-sm-3">
                            <a href="{{url('/admin')}}" class="btn btn-success" style="width: 100%;">Login Here! </a>
                          </div>
                        </div>
                    </form>
                </div>
        </div><!-- End sub_content -->
    </section><!-- End Header video -->
    <!-- End SubHeader ============================================ -->
<!-- COMMON SCRIPTS -->
<script src="{{URL::asset('public/login-assets/LOGIN/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{URL::asset('public/login-assets/LOGIN/js/common_scripts_min.js')}}"></script>
<script src="{{URL::asset('public/login-assets/LOGIN/js/functions.js')}}"></script>
<script src="{{URL::asset('public/login-assets/LOGIN/assets/validate.js')}}"></script>

<!-- SPECIFIC SCRIPTS -->
<script src="{{URL::asset('public/login-assets/LOGIN/js/video_header.js')}}"></script>
<script>
$(document).ready(function() {
    'use strict';
      HeaderVideo.init({
      container: $('.header-video'),
      header: $('.header-video--media'),
      videoTrigger: $("#video-trigger"),
      autoPlayVideo: true
    });    

});
</script>

</body>

<!-- Mirrored from www.ansonika.com/quickfood/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 22 Aug 2018 13:44:46 GMT -->
</html>