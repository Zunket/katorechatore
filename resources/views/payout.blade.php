@extends('layout.master')

@section('title')
{{APP_NAME}}

@endsection

@section('content')

<div class="content-wrapper">
  <div class="content-header row">
    <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
      <div class="row">
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!-- <div class="p-2 text-center bg-danger rounded-left">
                             <i class="fa fa-user-circle-o font-large-2 text-white"></i> 
                        </div> -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All Order</h5>
                            <h5 class="text-bold-400">33</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="media align-items-stretch">
                          <!-- <div class="p-2 text-center bg-success rounded-left">
                               <i class="fa fa-motorcycle font-large-2 text-white"></i> 
                          </div> -->
                          <div class="py-1 px-2 media-body">
                              <h5 class="success">Active Vendor</h5>
                              <h5 class="text-bold-400">22</h5>
                              <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                              </div> -->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-xl-4 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!--  -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">Inactive Vendor</h5>
                            <h5 class="text-bold-400">55</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  <div class="content-body">
    <!-- Basic form layout section start -->


    <section id="configuration">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-head">
              <div class="card-header">
                <h4 class="card-title">&nbsp;</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                <!-- <div class="heading-elements">
                 <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                  <li><a data-action="expand"><i class="ft-maximize"></i></a></li>

                </ul>
              </div> -->

            </div>
          </div>
          <div class="card-content collapse show">
            <div class="card-body card-dashboard">
              <div class="table-responsive">
                <table class="table table-striped table-bordered zero-configuration">
                  <thead> 
                    <tr>
                      <th>{{trans('constants.sno')}}</th>
                      <th>{{ucfirst($type)}}</th>
                      <th>{{trans('constants.phone')}}</th>   
                      <th>{{trans('constants.total_order')}}</th>   
                      <th>{{trans('constants.total')}} {{trans('constants.to_paid')}}</th>   
                      <th>{{trans('constants.action')}}</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $i=1; @endphp
                    @foreach($data as $value)
                    <tr>
                      <td>{{$i}}</td>
                      <td>@if($type=='vendor') {{$value->restaurant_name}} @else {{$value->name}} @endif</td>
                      <td>{{$value->phone}}</td>
                      <td>{{$value->foodrequest_count}}</td>
                      <td>${{$value->pending_payout}}</td>
                      <td><!--amount/restaurant_id or driver_id -->
                        <a href="{{url('/')}}/admin/add_payout/{{$type}}/{{$value->pending_payout}}/{{$value->id}}" class="button btn btn-icon btn-success mr-1 link_clr shadow-box">Make Payment</a>
                      </td>
                    </tr>
                    @php $i++ @endphp
                    @endforeach
                  </div>
                </div>

                @endsection     
