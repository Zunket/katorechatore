   <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

                 @if(Session::get('role') == 1)
                <li {{{ (Request::is('admin/dashboard') ? 'class=active' : '') }}} class="nav-item" >
                    <a href="{{URL('/')}}/admin/dashboard"><i class="fa fa-circle-thin" aria-hidden="true"></i><span data-i18n="" class="menu-title">Dashboard</span></a>
                </li>
                @endif
                 @if(Session::get('role') == 2)
                <li {{{ (Request::is('admin/dashboard') ? 'class=active' : '') }}} class="nav-item" >
                    <a href="{{URL('/')}}/admin/dashboard"><i class="fa fa-circle-thin" aria-hidden="true"></i><span data-i18n="" class="menu-title">Dashboard</span></a>
                </li>
                @endif
                
                <!--<li {{{ (Request::is('admin/order_dashboard') ? 'class=active' : '') }}}
                    {{{ (Request::is('admin/orders/new') ? 'class=active' : '') }}}
                    {{{ (Request::is('admin/orders/processing') ? 'class=active' : '') }}} {{{ (Request::is('admin/orders/pickup') ? 'class=active' : '') }}}{{{ (Request::is('admin/orders/delivered') ? 'class=active' : '') }}}{{{ (Request::is('admin/orders/cancelled') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="ft-menu"></i><span data-i18n="" class="menu-title">{{trans('constants.order_mng')}}</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/order_dashboard') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/order_dashboard" class="menu-item">{{trans('constants.order')}} {{trans('constants.dashboard')}}</a></li>
                        <li {{{ (Request::is('admin/orders/new') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/orders/new" class="menu-item">{{trans('constants.new')}} {{trans('constants.order')}}</a></li>
                        <li {{{ (Request::is('admin/orders/processing') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/orders/processing" class="menu-item">{{trans('constants.process')}} {{trans('constants.order')}}</a></li>
                        <li {{{ (Request::is('admin/orders/pickup') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/orders/pickup" class="menu-item"> {{trans('constants.order')}} {{trans('constants.pickup')}}</a></li>
                        <li {{{ (Request::is('admin/orders/delivered') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/orders/delivered" class="menu-item"> {{trans('constants.delivered')}} {{trans('constants.order')}}</a></li>
                        <li {{{ (Request::is('admin/orders/cancelled') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/orders/cancelled" class="menu-item"> {{trans('constants.cancelled')}} {{trans('constants.order')}}</a></li>
                    </ul>
                </li>-->

                 @if(Session::get('role') == 1)
                
              <!--  <li class="nav-item">
                    <a href="{{URL('/')}}/admin/dispatcher">
                        <i class="ft-user"></i>
                        <span data-i18n="" class="menu-title">Dispatcher</span>  
                    </a>
                </li> -->
                
                <li {{{ (Request::is('admin/store_list') ? 'class=active' : '') }}}
                    {{{ (Request::is('admin/add_store') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-cutlery"></i><span data-i18n="" class="menu-title">Restaurants</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/store_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/store_list" class="menu-item">All Restaurant</a></li>
                        <li {{{ (Request::is('admin/add_store') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_store" class="menu-item">Add Restaurant</a></li>
                    </ul>
                </li>
                <li {{{ (Request::is('admin/city_list') ? 'class=active' : '') }}}
                    {{{ (Request::is('admin/add_city') ? 'class=active' : '') }}} 
                    {{{ (Request::is('admin/country_list') ? 'class=active' : '') }}}{{{ (Request::is('admin/state_list') ? 'class=active' : '') }}}class="nav-item has-sub"><a href="#"><i class="fa fa-map-marker"></i><span data-i18n="" class="menu-title">Location</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/city_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/city_list" class="menu-item">All City</a></li>
                            
                        <li {{{ (Request::is('admin/add_city') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_city" class="menu-item">Add City</a></li>

                        <li {{{ (Request::is('admin/country_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/country_list" class="menu-item">All {{ trans('constants.country') }}</a></li>

                        <li {{{ (Request::is('admin/add_country') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/add_country" class="menu-item">Add Country</a></li>

                        <li {{{ (Request::is('admin/state_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/state_list" class="menu-item">All {{ trans('constants.state') }}</a></li>
                        <li {{{ (Request::is('admin/add_state') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/add_state" class="menu-item">Add State</a></li>
                    </ul>
                </li>
                <!--<li {{{ (Request::is('admin/product_list') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-cubes"></i><span data-i18n="" class="menu-title">Food Management</span></a>
                    <ul class="menu-content">
                       
                        <li {{{ (Request::is('admin/product_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/product_list" class="menu-item">Food List</a></li>
                    </ul>
                </li>-->
                <li {{{ (Request::is('admin/vehicle_list') ? 'class=active' : '') }}}
                    {{{ (Request::is('admin/add_vehicle') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-taxi"></i><span data-i18n="" class="menu-title">Vehicle</span></a>
                    <ul class="menu-content">
                       
                        <li {{{ (Request::is('admin/vehicle_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/vehicle_list" class="menu-item">All Vehicle</a></li>
                            
                        <li {{{ (Request::is('admin/add_vehicle') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_vehicle" class="menu-item">Add Vehicle</a></li>
                    </ul>
                </li>
                <li {{{ (Request::is('admin/driver_list') ? 'class=active' : '') }}}
                    {{{ (Request::is('admin/deliverypeople_list') ? 'class=active' : '') }}}{{{ (Request::is('admin/add_new_driver') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-hdd-o" aria-hidden="true"></i><span data-i18n="" class="menu-title">Driver</span></a>
                    <ul class="menu-content">

                        <li {{{ (Request::is('admin/driver_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/driver_list" class="menu-item"> All Driver</a></li>
                       
                        <li {{{ (Request::is('admin/deliverypeople_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/deliverypeople_list" class="menu-item">New Driver</a></li>
                            
                        <li {{{ (Request::is('admin/add_new_driver') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_new_driver" class="menu-item">Add Driver</a></li>
                    </ul>
                </li>
                <li {{{ (Request::is('admin/document_list') ? 'class=active' : '') }}}
                    {{{ (Request::is('admin/add_document') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-file-text"></i><span data-i18n="" class="menu-title">Document</span></a>
                    <ul class="menu-content">
                       
                        <li {{{ (Request::is('admin/document_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/document_list" class="menu-item">All Document</a></li>
                            
                        <li {{{ (Request::is('admin/add_document') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_document" class="menu-item">Add Document</a></li>
                    </ul>
                </li>
                 <li class="nav-item has-sub"><a href="#"><i class="fa fa-gift"></i><span data-i18n="" class="menu-title">Promo Code</span></a>
                    <ul class="menu-content">
                       
                        <li {{{ (Request::is('admin/coupon_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/coupon_list" class="menu-item">All PromoCode</a></li>
                            
                        <li {{{ (Request::is('admin/add_coupon') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_coupon" class="menu-item">Add New</a></li>
                    </ul>
                </li>
                 <li {{{ (Request::is('admin/reason_list') ? 'class=active' : '') }}}
                     {{{ (Request::is('admin/cancellation_reason_list') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-ban"></i><span data-i18n="" class="menu-title">Cancellation</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/reason_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/reason_list" class="menu-item">All Cancellation</a></li>
                        <li {{{ (Request::is('admin/cancellation_reason_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/cancellation_reason_list" class="menu-item">Add New</a></li>
                    </ul>
                </li>
                <!--<li class="nav-item has-sub"><a href="#"><i class="ft-user-check"></i><span data-i18n="" class="menu-title">Driver</span></a>
                    <ul class="menu-content">
                        <li><a href="{{URL('/')}}/admin/deliverypeople_list" class="menu-item"> All Driver</a></li>
                        <li><a href="{{URL('/')}}/admin/add_deliverypeople" class="menu-item">Add Driver</a></li>
                        <li><a href="{{URL('/')}}/admin/shift_delivery" class="menu-item">Shift Details</a></li>
                    </ul>
                </li>-->
                <!-- <li class="nav-item has-sub"><a href="#"><i class="ft-user"></i><span data-i18n="" class="menu-title">Dispute Manager</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/disp_managerlist') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/disp_managerlist" class="menu-item">All Manager</a></li>
                        <li {{{ (Request::is('admin/add_dispmanager') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_dispmanager" class="menu-item">Add New Manager</a></li>
                    </ul>
                </li> -->
                  
                <!--<li {{{ (Request::is('admin/promocodes_list') ? 'class=active' : '') }}}
                    {{{ (Request::is('admin/add_promocode') ? 'class=active' : '') }}}
                    {{{ (Request::is('admin/custumpush') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="ft-book"></i><span data-i18n="" class="menu-title">Promocodes</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/promocodes_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/promocodes_list" class="menu-item">Promocodes List</a></li>
                        <li {{{ (Request::is('admin/add_promocode') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_promocode" class="menu-item">Add Promocodes</a></li>
                        <li {{{ (Request::is('admin/custumpush') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/custumpush" class="menu-item">{{ trans('constants.custom_push') }}</a></li>
                    </ul>
                </li>-->
                 <li {{{ (Request::is('admin/banner_list') ? 'class=active' : '') }}}{{{ (Request::is('admin/add_banner') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-flag"></i><span data-i18n="" class="menu-title">Banner</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/banner_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/banner_list" class="menu-item">All Banner</a></li>
                        <li {{{ (Request::is('admin/add_banner') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_banner" class="menu-item">Add New Banner</a></li>
                    </ul>
                </li>
                <li class="nav-item has-sub"><a href="#"><i class="ft-book"></i><span data-i18n="" class="menu-title">Push Notification</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/notification_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/notification_list" class="menu-item">All Notification</a></li>
                        <!--  <li {{{ (Request::is('admin/add_noticeboard') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_noticeboard" class="menu-item">Add Notice Board</a></li> -->
                        <li {{{ (Request::is('admin/custumpush') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/custumpush" class="menu-item">Add New<!--Custom Push--></a></li>
                    </ul>
                </li>
                <li {{{ (Request::is('admin/user_list') ? 'class=active' : '') }}} class="nav-item">
                    <a href="{{URL('/')}}/admin/user_list"><i class="ft-users"></i>{{trans('constants.user')}}</a>
                    <!--<li><a href="{{URL('/')}}/admin/add_user" class="menu-item">Add User</a></li>-->
                </li>
                

                <!-- <li class="nav-item has-sub"><a href="#"><i class="ft-book"></i><span data-i18n="" class="menu-title">Page</span></a>
                    <ul class="menu-content">
                        <li><a href=" " class="menu-item">About us</a></li>
                        <li><a href=" " class="menu-item">FAQ</a></li>
                        <li><a href=" " class="menu-item">Privacy</a></li>
                        <li><a href=" " class="menu-item">Contact Us</a></li>
                        <li><a href=" " class="menu-item">Terms and Condition</a></li>
                        <li><a href=" " class="menu-item">Cancellation and Refunds</a></li>
                        <li><a href=" " class="menu-item">Other Terms</a></li>
                        <li><a href=" " class="menu-item">General Queries</a></li>
                        <li><a href=" " class="menu-item">Help</a></li>
                    </ul>
                </li> -->
               
                 
                 
                <li {{{ (Request::is('admin/cuisines_list') ? 'class=active' : '') }}} {{{ (Request::is('admin/add_cuisines') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-bars"></i><span data-i18n="" class="menu-title">Cuisines</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/cuisines_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/cuisines_list" class="menu-item">All Cuisines </a></li>
                        <li {{{ (Request::is('admin/add_cuisines') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_cuisines" class="menu-item">Add Cuisines</a></li>
                    </ul>
                </li>

                <li {{{ (Request::is('admin/addons_list') ? 'class=active' : '') }}}>
                    <a href="{{URL('/')}}/admin/addons_list"><i class="ft-user-check"></i><span data-i18n="" class="menu-title">{{trans('constants.addon')}}</span></a></li>
                </li>
                <li {{{ (Request::is('admin/payout/vendor') ? 'class=active' : '') }}} {{{ (Request::is('admin/payout/driver') ? 'class=active' : '') }}}
                {{{ (Request::is('admin/payout_history/vendor') ? 'class=active' : '') }}} {{{ (Request::is('admin/payout_history/driver') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-money"></i><span data-i18n="" class="menu-title">{{trans('constants.payout')}}</span></a>
                    <ul class="menu-content">
                      <li {{{ (Request::is('admin/payout/vendor') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/payout/vendor" class="menu-item">{{trans('constants.restaurant')}} {{trans('constants.payout')}} </a></li>    
                      <li {{{ (Request::is('admin/payout/driver') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/payout/driver" class="menu-item">{{trans('constants.driver')}} {{trans('constants.payout')}} </a></li>   
                      <li {{{ (Request::is('admin/payout_history/vendor') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/payout_history/vendor" class="menu-item">{{trans('constants.restaurant')}} {{trans('constants.transaction_history')}} </a></li>    
                      <li {{{ (Request::is('admin/payout_history/driver') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/payout_history/driver" class="menu-item">{{trans('constants.driver')}} {{trans('constants.transaction_history')}} </a></li>                    
                    </ul>
               </li>
                <li {{{ (Request::is('admin/food-quantity-list') ? 'class=active' : '') }}} {{{ (Request::is('admin/add-food-quantity') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-list-ol"></i><span data-i18n="" class="menu-title"> Item Quantity</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/food-quantity-list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/food-quantity-list" class="menu-item">All Item Quantity </a></li>
                        <li {{{ (Request::is('admin/add-food-quantity') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add-food-quantity" class="menu-item">Add Item Quantity </a></li>
                    </ul>
                </li>

                <li {{{ (Request::is('admin/category_list') ? 'class=active' : '') }}} {{{ (Request::is('admin/add_category') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-list-alt"></i><span data-i18n="" class="menu-title">Category</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/category_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/category_list" class="menu-item">All Category</a></li>
                        <li {{{ (Request::is('admin/add_category') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_category" class="menu-item">Add Category</a></li>
                    </ul>
                </li>
                @endif
                 @if(Session::get('role') == 2)
                 <!-- <li {{{ (Request::is('admin/restaurant_cuisines') ? 'class=active' : '') }}} class="nav-item">
                    <a href="{{URL('/')}}/admin/restaurant_cuisines"><i class="fa fa-bars" aria-hidden="true"></i><span data-i18n="" class="menu-title">Cuisines</span></a>
                </li> -->
                 <li {{{ (Request::is('admin/restaurant_cuisines') ? 'class=active' : '') }}} {{{ (Request::is('admin/add_restaurant_cuisines') ? 'class=active' : '') }}}><a href="#"><i class="fa fa-bars"></i><span data-i18n="" class="menu-title">Cuisines</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/restaurant_cuisines') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/restaurant_cuisines" class="menu-item">All Cuisines</a></li>
                        <li {{{ (Request::is('admin/add_restaurant_cuisines') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_restaurant_cuisines" class="menu-item">Add Cuisines</a></li>
                    </ul>
                </li>

                <!-- <li {{{ (Request::is('admin/restaurant_menu') ? 'class=active' : '') }}} class="nav-item">
                    <a href="{{URL('/')}}/admin/restaurant_menu"><i class="fa fa-list-alt"></i><span data-i18n="" class="menu-title">Category</span></a>
                </li> -->
                <li {{{ (Request::is('admin/restaurant_menu') ? 'class=active' : '') }}} {{{ (Request::is('admin/add_restaurant_menu') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="fa fa-list-alt"></i><span data-i18n="" class="menu-title">Menu</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/restaurant_menu') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/restaurant_menu" class="menu-item">All Menu</a></li>
                        <li {{{ (Request::is('admin/add_restaurant_menu') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_restaurant_menu" class="menu-item">Add Menu</a></li>
                    </ul>
                </li>

                <li {{{ (Request::is('admin/addons_list') ? 'class=active' : '') }}} {{{ (Request::is('admin/add_addons') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="ft-user-check"></i><span data-i18n="" class="menu-title">{{trans('constants.addon')}}</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/addons_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/addons_list" class="menu-item">All {{trans('constants.addon')}}</a></li>
                        <li {{{ (Request::is('admin/add_addons') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_addons" class="menu-item">{{trans('constants.add')}} {{trans('constants.addon')}}</a></li>
                    </ul>
                </li>
                <li {{{ (Request::is('admin/product_list') ? 'class=active' : '') }}} {{{ (Request::is('admin/add_product') ? 'class=active' : '') }}}class="nav-item has-sub"><a href="#"><i class="fa fa-product-hunt" aria-hidden="true"></i><span data-i18n="" class="menu-title">Food</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/product_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/product_list" class="menu-item">All Food</a></li>
                        <li {{{ (Request::is('admin/add_product') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/add_product" class="menu-item">Add Food</a></li>
                    </ul>
                </li>
                @endif
                <li class="nav-item has-sub"><a href="#"><i class="fa fa-shopping-cart"></i><span data-i18n="" class="menu-title">Order</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/deliveries') ? 'class=active' : '') }}}>
                           <a href="{{URL('/')}}/admin/deliveries" class="menu-item">All Order</a>
                        </li> 

                        <li {{{ (Request::is('admin/neworder_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/neworder_list" class="menu-item">New Order</a></li>

                        <li {{{ (Request::is('admin/neworder_list/7') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/neworder_list/7" class="menu-item">completed Order</a></li>
                        
                        <li {{{ (Request::is('admin/neworder_list/10') ? 'class=active' : '') }}}>
                        <a href="{{URL('/')}}/admin/neworder_list/10" class="menu-item">Cancel Order</a></li>

                        
                    </ul>
                </li>
                
                
                <!-- <li class="nav-item has-sub"><a href="#"><i class="ft-user-check"></i><span data-i18n="" class="menu-title">Leads</span></a>
                    <ul class="menu-content">
                        <li><a href="{{URL('/')}}/admin/restuarant_leads" class="menu-item">Restaurant Leads</a></li>
                        <li><a href="{{URL('/')}}/admin/delivery_people" class="menu-item">Delivery People</a></li>
                        <li><a href="{{URL('/')}}/admin/news_letter" class="menu-item">Newsletter</a></li>
                    </ul>
                </li> -->
               
                @if(Session::get('role') == 1)
                <li {{{ (Request::is('admin/admin_restaurant_report') ? 'class=active' : '') }}}{{{ (Request::is('admin/delivery_boy_reports') ? 'class=active' : '') }}}{{{ (Request::is('admin/restaurant_report') ? 'class=active' : '') }}} class="nav-item has-sub"><a href="#"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Order Reports</span></a>
                    <ul class="menu-content">
                        <li {{{ (Request::is('admin/admin_restaurant_report') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/admin_restaurant_report" class="menu-item">Vendor Report</a></li>
                       
                        <li {{{ (Request::is('admin/delivery_boy_reports') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/delivery_boy_reports" class="menu-item">Delivery Boy Report</a></li>
                        <!-- <li {{{ (Request::is('admin/restaurant_report') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/restaurant_report" class="menu-item"><span data-i18n="" class="menu-title">Order Reports</span></a></li> -->
                    </ul>
                </li>
                @endif
                @if(Session::get('role') == 1)
                <li {{{ (Request::is('admin/review') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/review"><i class="fa fa-star" aria-hidden="true"></i><span data-i18n="" class="menu-title">Review & rating</span></a>
                </li>
                <li {{{ (Request::is('admin/settings/site') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/settings/site"><i class="fa fa-building"></i><span data-i18n="" class="menu-title">Business setting</span></a>
                    <!-- <ul class="menu-content">
                      <li {{{ (Request::is('admin/settings/site') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/settings/site" class="menu-item">Business setting</a></li>
                      <li {{{ (Request::is('admin/settings/google') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/settings/google" class="menu-item">Payment Setting</a></li> -->                           
                      <!-- <li {{{ (Request::is('admin/settings/email') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/settings/email" class="menu-item">{{trans('constants.email_setting')}}</a></li>
                      <li {{{ (Request::is('admin/email_template_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/email_template_list" class="menu-item">{{trans('constants.email_template')}} {{trans('constants.list')}}</a></li> -->
                    <!-- </ul> -->
               </li>
               <li {{{ (Request::is('admin/settings/google') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/settings/google"><i class="fa fa-usd"></i><span data-i18n="" class="menu-title">Payment setting</span></a>
                    <!-- <ul class="menu-content">
                      <li {{{ (Request::is('admin/settings/site') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/settings/site" class="menu-item">Business setting</a></li>
                      <li {{{ (Request::is('admin/settings/google') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/settings/google" class="menu-item">Payment Setting</a></li> -->                           
                      <!-- <li {{{ (Request::is('admin/settings/email') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/settings/email" class="menu-item">{{trans('constants.email_setting')}}</a></li>
                      <li {{{ (Request::is('admin/email_template_list') ? 'class=active' : '') }}}><a href="{{URL('/')}}/admin/email_template_list" class="menu-item">{{trans('constants.email_template')}} {{trans('constants.list')}}</a></li> -->
                    <!-- </ul> -->
               </li>
               @endif
               @if(Session::get('role') == 1)
                 <li {{{ (Request::is('admin/logout') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/logout" class="menu-item"><i class="ft-power"></i><span data-i18n="" class="menu-title">Logout</span></a></li>
                 
               
                @endif
                @if(Session::get('role') == 2)
                <li {{{ (Request::is('admin/restaurant_report') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/restaurant_report" class="menu-item"><i class="ft-mail"></i><span data-i18n="" class="menu-title">Reports</span></a></li>
                <li class="nav-item has-sub"><a href="#"><i class="ft-settings"></i><span data-i18n="" class="menu-title">{{trans('constants.setting')}}</span></a>
                    <ul class="menu-content">
                      <li {{{ (Request::is('admin/setting/edit_store') ? 'class=active' : '') }}}><a href="{{URL('/admin/setting/edit_store/'.session()->get('userid'))}}" class="menu-item">Vendor</a></li>
                      <li {{{ (Request::is('admin/setting/edit_account') ? 'class=active' : '') }}}><a href="{{URL('/admin/setting/edit_account/'.session()->get('userid'))}}" class="menu-item">Account</a></li>
                      <li {{{ (Request::is('admin/setting/edit_paypal') ? 'class=active' : '') }}}><a href="{{URL('/admin/setting/edit_paypal/'.session()->get('userid'))}}" class="menu-item">Paypal</a></li>
                    </ul>
                </li>
                  <li {{{ (Request::is('admin/logout') ? 'class=active' : '') }}}><a href="{{url('/')}}/admin/logout" class="menu-item"><i class="ft-power"></i><span data-i18n="" class="menu-title">Logout</span></a></li>
                @endif
            </ul>
         </div>


  
  