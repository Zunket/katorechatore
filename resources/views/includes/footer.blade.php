
  
  <!-- BEGIN VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
  

  <script type="text/javascript" src="https://foodie.deliveryventure.com/assets/admin/plugins/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
  <script type="text/javascript" src="https://foodie.deliveryventure.com/assets/admin/plugins/dropify/dist/js/dropify.min.js"></script>

  <!-- BEGIN VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/pickadate/picker.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/pickadate/picker.date.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/pickadate/picker.time.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/pickadate/legacy.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js')}}"
  type="text/javascript"></script>

  
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
 
   <script src="{{URL::asset('public/app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
   <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>

      

  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/echarts/echarts.js')}}" type="text/javascript"></script>
   <script src="{{URL::asset('public/app-assets/vendors/js/charts/echarts/chart/line.js')}}" type="text/javascript"></script>
   <script src="{{URL::asset('public/app-assets/vendors/js/charts/echarts/chart/bar.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/morris.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/data/jvector/visitor-data.js')}}" type="text/javascript"></script>
   <script src="{{URL::asset('public/app-assets/vendors/js/pickers/daterange/daterangepicker.js')}}" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN MODERN JS-->
  <script src="{{URL::asset('public/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/core/app.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <!-- END MODERN JS-->

  <!-- BEGIN PAGE LEVEL JS--> 
  <script src="{{URL::asset('public/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/charts/echarts/line-area/basic-area.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/pages/dashboard-sales.js')}}" type="text/javascript"></script>
  
  <script src="{{URL::asset('public/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/forms/select/form-select2.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>

  <!-- END PAGE LEVEL JS-->