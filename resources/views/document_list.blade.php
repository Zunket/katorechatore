@extends('layout.master')

@section('title')

{{APP_NAME}}
@endsection

@section('content')

 <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
          <div class="row">
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!-- <div class="p-2 text-center bg-danger rounded-left">
                             <i class="fa fa-user-circle-o font-large-2 text-white"></i> 
                        </div> -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All Document</h5>
                            <h5 class="text-bold-400">{{$total_document}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="media align-items-stretch">
                          <!-- <div class="p-2 text-center bg-success rounded-left">
                               <i class="fa fa-motorcycle font-large-2 text-white"></i> 
                          </div> -->
                          <div class="py-1 px-2 media-body">
                              <h5 class="success">Active Document</h5>
                              <h5 class="text-bold-400">{{$active_total_document}}</h5>
                              <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                              </div> -->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-xl-4 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!--  -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">Inactive Document</h5>
                            <h5 class="text-bold-400">{{$inactive_total_document}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      <div class="content-body">
        <!-- Basic form layout section start -->


         <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header">
                    <h4 class="card-title" style="height:20px;"></h4>
                  <!-- <h4 class="card-title" style="height:50px;color:red;">
                ** Demo Mode : No Permission to Edit and Delete.</h4> -->
                  <h4 class="card-title"></h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                  <div class="heading-elements">
                     <ul class="list-inline mb-0">
                      <!-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li> -->
                     <!-- <li>  <button class="btn btn-primary btn-sm"><i class="ft-plus white"></i><a style=" color: white !important;" href="{{URL('/')}}/admin/add_document"> Add Document</a></button></li> -->
                      </ul>
                      </div>
                      
                    </div>
                </div>

                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead> 


                          <tr>
                          
                            <th>Document Name</th>
                            <th>Document For</th>
                            <th>Action</th>
                            
                            <th>Status</th>
                            
                            <!-- <th>Action</th> -->
                          </tr>
                        </thead>
                        <tbody>
                          <!--{{$s_no=1}}-->
                          @foreach($document_list as $document)
                          <tr>
                           
                           
                            <td>{{$document->document_name}}</td>
                            <td><?php

                            switch ((int) $document->document_for) {
                              case 1:
                                echo 'Driver';
                              break;
                              case 2:
                                echo 'Restaurant ';
                              break;
                              
                              
                              default:
                                echo 'NULL';
                                break;
                            }
                            ?></td>
                            <td>
                            
                            <a href="{{url('/')}}/admin/delete_document/{{$document->id}}" class="btn btn-icon btn-danger mr-1 link_clr shadow-box" data-id="1" data-toggle="modal"  data-target="#{{$document->id}}"><i class="ft-delete"></i></a>
                          </td>
                            <td><button class="btn btn-danger">
                             
                              <?php

                            switch ((int) $document->status) {
                              case 1:
                                echo 'Active';
                              break;
                              case 2:
                                echo 'In Active ';
                              break;
                              
                              
                              default:
                                echo 'NULL';
                                break;
                            }
                            ?></button></td>
                            
                           
                            
                           

                          </tr>
                          <!--- Deleted Restaurant / Model -->
                                  <div class="modal animated slideInRight text-left" id="{{$document->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                                       <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                         <div class="modal-header">
                                          <h4 class="modal-title" id="myModalLabel76">Delete Document</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                         </div>
                                         <form method="post" action="{{url('/')}}/admin/delete_document/{{$document->id}}">
                                            <div class="modal-body">
                                              
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                  <input type="hidden" name="id" value="{{$document->id}}">
                                                <div class="form-group">
                                                    <label for="eventName2">Are you sure to delete state : {{$document->document_name}}</label>
                                                </div>
                                            
                                            </div>
                                            <div class="modal-footer">
                                              <button type="submit" class="btn btn-black mr-1" style="padding: 10px 15px;">
                                                    <i class="ft-check-square"></i> Delete
                                                </button>
                                                <button type="button" class="btn btn-danger mr-1" data-dismiss="modal" style="padding: 10px 15px;">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                
                                            </div>
                                          </form>
                                      </div>
                                    </div>
                                  </div>
                            <!--- End Deleted Restaurant / Modulel -->
                          <!--{{$s_no++}}-->
                          @endforeach
                         

                          
                          
                        </tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- // Basic form layout section end -->
      </div>
    </div>
 

   @endsection     
