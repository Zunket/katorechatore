@extends('layout.master')

@section('title')
{{APP_NAME}}
@endsection

@section('content')
<div class="content-wrapper">
   <div class="content-header row">
    <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new"> 
      
      <div class="row">
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!-- <div class="p-2 text-center bg-danger rounded-left">
                             <i class="fa fa-user-circle-o font-large-2 text-white"></i> 
                        </div> -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All Country</h5>
                            <h5 class="text-bold-400">{{$total_country}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="media align-items-stretch">
                          <!-- <div class="p-2 text-center bg-success rounded-left">
                               <i class="fa fa-motorcycle font-large-2 text-white"></i> 
                          </div> -->
                          <div class="py-1 px-2 media-body">
                              <h5 class="success">All City</h5>
                              <h5 class="text-bold-400">{{$total_city}}</h5>
                              <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                              </div> -->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-xl-4 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!--  -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">All State</h5>
                            <h5 class="text-bold-400">{{$total_state}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
          </div>
     </div>
  <!--</div> -->
  <div class="content-body">
    <!-- Basic form layout section start -->


    <section id="configuration">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-head">
              <div class="card-header">
                <h4 class="card-title" style="height:20px"></h4>
                <!-- <h4 class="card-title"> {{trans('constants.state')}} {{strtoLower(trans('constants.list'))}}</h4> -->
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                <div class="heading-elements">
                 <ul class="list-inline mb-0">
                  <!-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                  <li><a data-action="expand"><i class="ft-maximize"></i></a></li> -->

                    <!--  <li> <button class="btn btn-primary btn-sm"><i class="ft-plus white"></i><a style=" color: white !important;" href="{{url('/')}}/admin/add_state"> {{trans('constants.add')}} {{trans('constants.state')}}</a></button></li> -->
               </ul>
              </div>

            </div>
          </div>
          <div class="card-content collapse show">
            <div class="card-body card-dashboard">
              <div class="table-responsive">
                <table class="table table-striped table-bordered zero-configuration">
                  <thead> 
                    <tr>
                      
                      <th>{{trans('constants.state')}}</th>   
                      <th>{{trans('constants.country')}} {{trans('constants.id')}}</th>    
                      <th>{{trans('constants.action')}}</th>   
                    </tr>
                  </thead>
                  <tbody>
                    
                    @forelse($data as $d)
                    <tr>
                     
                      <td>{{$d->state}}</td>   
                      <td>{{$d->Country->country}}</td>   
                      <td><a style=" color: white !important;" href="{{URL('/')}}/admin/edit_state/{{$d->id}}" class="button btn btn-icon btn-success mr-1 link_clr shadow-box"><i class="ft-edit"></i></a></button>
                      <a href="{{url('/')}}/admin/delete_state/{{$d->id}}" class="btn btn-icon btn-danger mr-1 link_clr shadow-box" data-id="1" data-toggle="modal"  data-target="#{{$d->id}}"><i class="ft-delete"></i></a></td>   
                    </tr>
                    <!--- Deleted Restaurant / Model -->
                                  <div class="modal animated slideInRight text-left" id="{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                                       <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                         <div class="modal-header">
                                          <h4 class="modal-title" id="myModalLabel76">Delete Country</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                         </div>
                                         <form method="post" action="{{url('/')}}/admin/delete_state/{{$d->id}}">
                                            <div class="modal-body">
                                              
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                  <input type="hidden" name="id" value="{{$d->id}}">
                                                <div class="form-group">
                                                    <label for="eventName2">Are you sure to delete state : {{$d->state}}</label>
                                                </div>
                                            
                                            </div>
                                            <div class="modal-footer">
                                               <button type="submit" class="btn btn-black mr-1" style="padding: 10px 15px;">
                                                    <i class="ft-check-square"></i> Delete
                                                </button>
                                                <button type="button" class="btn btn-danger mr-1" data-dismiss="modal" style="padding: 10px 15px;">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                               
                                            </div>
                                          </form>
                                      </div>
                                    </div>
                                  </div>
                            <!--- End Deleted Restaurant / Modulel -->
                    @empty
                    {{trans('constants.no_data')}}
                    @endforelse
                  </div>
                </div>

                @endsection     
