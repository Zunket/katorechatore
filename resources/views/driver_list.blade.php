@extends('layout.master')

@section('title')

{{APP_NAME}}
@endsection

@section('content')
<style type="text/css">
  #top-list{
  list-style-type: none;
  list-style: none;
}
</style>
 <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
          <div class="row">
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!-- <div class="p-2 text-center bg-danger rounded-left">
                             <i class="fa fa-user-circle-o font-large-2 text-white"></i> 
                        </div> -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All Driver</h5>
                            <h5 class="text-bold-400">{{$all_drivers}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-md-6 col-12">
              <div class="card">
                  <div class="card-content">
                      <div class="media align-items-stretch">
                          <!-- <div class="p-2 text-center bg-success rounded-left">
                               <i class="fa fa-motorcycle font-large-2 text-white"></i> 
                          </div> -->
                          <div class="py-1 px-2 media-body">
                              <h5 class="success">Active Driver</h5>
                              <h5 class="text-bold-400">{{$active_drivers}}</h5>
                              <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: 45%" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                              </div> -->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-xl-4 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!--  -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">Inactive Driver</h5>
                            <h5 class="text-bold-400">{{$in_active_drivers}}</h5>
                            <!-- <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
        
        </section>

         <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header">
                  <h4 class="card-title" style="height:20px;">
                 </h4>
                  <h4 class="card-title"></h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                  <div class="heading-elements">
                     <ul class="list-inline mb-0">
                      <!-- <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li> -->
                     <!-- <li>  <button class="btn btn-primary btn-sm"><i class="ft-plus white"></i><a style=" color: white !important;" href="{{URL('/')}}/admin/add_new_driver"> Add Delivery People</a></button></li> -->
                      </ul>
                      </div>
                      
                    </div>
                </div>

                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead> 


                          <tr>
                            
                            <th>Partner Id</th>
                            <th>Name</th>
                            <!-- <th>Email</th> -->
                            <th>Service Zone</th>
                            <th>Contact Details</th>
                            
                            <th>Image</th>
                            <th>Rating</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1; ?>
                          @foreach($data as $d)
                          <tr>
                            
                            <td>{{$d->partner_id}}
                            <td>{{$d->name}}</td>
                            <!-- <td>{{$d->email}}</td> -->
                            <td>@isset($d->Deliverypartner_detail->Citylist) {{$d->Deliverypartner_detail->Citylist->city}} @endisset</td>
                            <td>{{$d->phone}}</td>
                            
                           
                            <td><img src="{{BASE_URL.UPLOADS_PROFILE.$d->profile_pic}}" width="100px" alt=""></td>
                            <td>
                            @if(isset($d->Foodrequest))
                              @php 
                                $total = 0; 
                                $total_count = 0; 
                                $rating = 0; 
                              @endphp

                              @foreach($d->Foodrequest as $d1)
                                @if(isset($d1['OrderRatings']))
                                    @php
                                    $total += $d1['OrderRatings']['delivery_boy_rating'];
                                    $total_count = $total_count+1;
                                    @endphp
                                @endif
                              @endforeach

                            @if($total_count != 0 && $total != 0)
                              <!-- ($total / ($total_count-1)); -->
                              @php 
                                $rating = $total / ($total_count);
                              @endphp
                                
                              <!--isset orderrating checked rating-->

                              @for($x=1;$x<=(float)$rating;$x++) 
                              <i class="fa fa-star" style="color: #e6910d;font-size: 17px;"></i> 
                              @endfor

                              @if(strpos((float)$d->rating,'.'))
                              <i class="fa fa-star-half-o" style="color: #e6910d;font-size: 17px;"></i>
                              @php $x++; @endphp
                              @endif

                              @while ($x<=5)
                              <i class="fa fa-star-o" style="color: #e6910d;font-size: 17px;"></i>
                              @php $x++; @endphp
                              @endwhile 

                              <!-- -->

                            @else <!--isset orderrating false-->
                              @for($l=1;$l<=5; $l++)
                                 <span class="fa fa-star-o" style="color: #e6910d;font-size: 17px;"></span>
                              @endfor
                            @endif <!-- -->
                            @else <!--isset request false-->
                              @for($m=1;$m<=5; $m++)
                               <span class="fa fa-star-o" style="color: #e6910d;font-size: 17px;"></span>
                              @endfor
                            @endif <!-- -->
                            </td>
                            <td>
                            <!-- <button class="table-btn btn btn-icon btn-danger" form="resource-settle-55">Unsettle</button> -->
                            <li id="top-list">
                              <a href="{{url('/')}}/admin/view_delivery_boy_order_details/{{$d->id}}" class="button btn btn-icon btn-black mr-1 link_clr shadow-box"><i class="ft-eye"></i></a>
                              <a href="{{url('/')}}/admin/edit_delivery_boy_details/{{$d->id}}" class="button btn btn-icon btn-success mr-1 link_clr shadow-box"><i class="ft-edit"></i></a>
                              <button type="button" class="btn btn-icon btn-danger mr-1 link_clr shadow-box" data-id="1" data-toggle="modal"  data-target="#{{$d->id}}"><i class="ft-delete"></i></button></li>
                          </td>
                              <!-- <button type="button" class="btn btn-icon btn-success mr-1 link_clr" data-id="1" data-toggle="modal"  data-target="#{{$d->id}}"><i class="ft-delete"></i></button> -->
                            
                          </tr>

                                    <!---  Delete Delivery Partner Model -->

                                <div class="modal animated slideInRight text-left" id="{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                                       <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                         <div class="modal-header">
                                          <h4 class="modal-title" id="myModalLabel76">Delete Delivery Partner</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                         </div>
                                         <form method="post" action="{{url('/')}}/admin/delete_delivery_partner">
                                            <div class="modal-body">
                                              
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                  <input type="hidden" name="id" value="{{$d->id}}">
                                                 <div class="form-group">
                                                    <label for="eventName2">Are you sure to delete delivery partner : {{$d->partner_id}}</label>
                                                  </div>
                                            
                                            </div>
                                             <div class="modal-footer">
                                              <button type="submit" class="btn btn-black mr-1" style="padding: 10px 15px;">
                                                <i class="ft-check-square"></i> Delete
                                                 </button>
                                                <button type="button" class="btn btn-danger mr-1" data-dismiss="modal" style="padding: 10px 15px;">
                                                   <i class="ft-x"></i> Cancel
                                                    </button>
                                                  
                                            </div>
                                          </form>
                                      </div>
                                    </div>
                                  </div>
                                   <!--  Delete Delivery Partner Model Ends-->
                          <?php $i++; ?>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- // Basic form layout section end -->
      </div>
    </div>
    @endsection     
 