@extends('layout.master')

@section('title')

{{APP_NAME}}
@endsection

@section('content')
  <div class="content-wrapper">
      <div class="content-body">
        <section id="icon-tabs">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <!--<div class="card-header">
                   <h4 class="card-title"><i class="fa fa-list-alt"></i> &nbsp;CREATE CUISINES</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a> 
                   <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div> 
                </div>-->
                <!--  <hr> -->

                <div class="card-content collapse show">
                  <div class="card-body">
                     <form method="post" action="{{url('/')}}/admin/store_restaurant_menu">
                            <div class="modal-body">
                              
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                 <div class="form-group">
                                    <label for="eventName2">Menu Name:</label>
                                    <input type="text" class="form-control" name="menu_name" required="">
                                  </div>
                            
                             <div class="form-group">
                              <button type="submit" class="btn btn-success mr-1" style="padding: 10px 15px;">
                                <i class="ft-check-square"></i> Save
                                 </button>
                                <a href="{{url('/')}}/admin/restaurant_menu" class="btn btn-danger mr-1" style="padding: 10px 15px;" role="button"><i class="ft-x"></i> Cancel</a>
                                  
                            </div>
                            </div>
                          </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              </section>
            </div>
          </div>
  


    @endsection     
 