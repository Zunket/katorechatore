@extends('layout.master')

@section('title')

{{APP_NAME}}
@endsection

@section('content')
<style type="text/css">
  #top-list{
  list-style-type: none;
  list-style: none;
}
</style>
 <div class="content-wrapper">

      <div class="content-body">

         <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header">
                    <h4 class="card-title" style="height:20px;"></h4>
                      <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                      <div class="heading-elements">
                      <ul class="list-inline mb-0">
                      </ul>
                      </div>
                    </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead> 
                          <tr>
                            <th>User Name</th>
                            <th>Vendor</th>
                            <th>Message</th>
                            <th>Rating</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1; ?>
                          @foreach($data as $d)
                          <tr>
                            <td>{{@$d->user->name}}
                            <td>{{@$d->restaurant->restaurant_name}}</td>
                            <td>{{@$d->message}}</td>
                            <td>
                                @if($d->rating==1)
                                    <img src="{{url('/')}}/public/uploads/star.png">
                                @elseif($d->rating==2)
                                    <img src="{{url('/')}}/public/uploads/star.png"><img src="{{url('/')}}/public/uploads/star.png">
                                @elseif($d->rating==3)
                                    <img src="{{url('/')}}/public/uploads/star.png"><img src="{{url('/')}}/public/uploads/star.png"><img src="{{url('/')}}/public/uploads/star.png">
                                @elseif($d->rating==4)
                                    <img src="{{url('/')}}/public/uploads/star.png"><img src="{{url('/')}}/public/uploads/star.png"><img src="{{url('/')}}/public/uploads/star.png"><img src="{{url('/')}}/public/uploads/star.png">
                                @elseif($d->rating==5)
                                    <img src="{{url('/')}}/public/uploads/star.png"><img src="{{url('/')}}/public/uploads/star.png"><img src="{{url('/')}}/public/uploads/star.png"><img src="{{url('/')}}/public/uploads/star.png"><img src="{{url('/')}}/public/uploads/star.png">
                                @endif
                            </td>                            
                            <td>
                             <li id="top-list">
                              <button type="button" class="btn btn-icon btn-danger mr-1 link_clr shadow-box" data-id="1" data-toggle="modal"  data-target="#{{$d->id}}"><i class="ft-delete"></i></button></li>
                             </td>
                          </tr>
                          <div class="modal animated slideInRight text-left" id="{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                                       <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                         <div class="modal-header">
                                          <h4 class="modal-title" id="myModalLabel76">Delete Rating</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                         </div>
                                         <form method="post" action="{{url('/')}}/admin/delete_rating">
                                            <div class="modal-body">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input type="hidden" name="id" value="{{$d->id}}">
                                                <div class="form-group">
                                                    <label for="eventName2">Are you sure to delete rating</label>
                                                </div>
                                            </div>
                                             <div class="modal-footer">
                                                <button type="submit" class="btn btn-black mr-1" style="padding: 10px 15px;">
                                                <i class="ft-check-square"></i> Delete
                                                 </button>
                                                <button type="button" class="btn btn-danger mr-1" data-dismiss="modal" style="padding: 10px 15px;">
                                                   <i class="ft-x"></i> Cancel
                                                </button>
                                            </div>
                                          </form>
                                      </div>
                                    </div>
                                  </div>                                   
                          <?php $i++; ?>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    @endsection     
 