@extends('layout.master')

@section('title')

{{APP_NAME}}

@endsection

@section('content')
  <div class="content-wrapper">
      <div class="content-body">
        <section id="icon-tabs">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <!-- <h4 class="card-title">{{strtoUpper(trans('constants.create'))}} {{strtoUpper(trans('constants.vehicle'))}}</h4> -->
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                  <!-- <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div> -->
                </div>
                

                <div class="card-content collapse show">
                  <div class="card-body">
                    <form action="{{url('/')}}/admin/user_add" class="icons-tab-steps wizard-notification" method="post"  enctype="multipart/form-data" >
                       <input type="hidden" name="_token" value="{{csrf_token()}}">
                       <input type="hidden" name="id" value="@if(isset($data->id)){{$data->id}}@endif">
                    <fieldset>
                        <div class="row">
                          <div class="col-md-6">
                             <div class="form-group">
                              <label for="name">User Name<span style="color: red;">*</span></label>
                              <input id="name" type="text" class="form-control" name="name" value="@if(isset($data->name)){{$data->name}}@endif" required="" autofocus="">
                            </div>
                          </div>
                          <div class="col-md-6">
                             <div class="form-group">
                              <label for="name">Email<span style="color: red;">*</span></label>
                              <input id="name" type="text" class="form-control" name="email" value="@if(isset($data->email)){{$data->email}}@endif" required="" autofocus="">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="name">Contact Number<span style="color: red;">*</span></label>
                              <input id="name" type="text" class="form-control" name="phone" value="@if(isset($data->phone)){{$data->phone}}@endif" required="" autofocus="">
                            </div>
                          </div>
                             
                            
                          
                          <div class="col-md-6">
                              @if(isset($data->profile_image) && ($data->profile_image !=""))
                              <img src="{{$data->profile_image}}" style="width: 8em;">@endif
                             <div class="form-group">
                              <label for="name">Profile Image</label>
                              <input id="name" type="file" class="form-control" name="profile_image" value="" autofocus="">
                            </div>

                              
                          </div>
                        </div>
                        <div class="col-md-12">
                             <div class="form-actions" style="text-align: center">
                              <button type="submit" class="btn btn-success mr-1" style="padding: 10px 15px;">
                               <i class="ft-check-square"></i> Save
                                </button>
                              <a href="{{url('/')}}/admin/user_list" class="btn btn-danger mr-1" style="padding: 10px 15px;" role="button"><i class="ft-x"></i> Cancel</a>
                              
                            </div>
                          </div>
                        </fieldset>
                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              </section>
            </div>
          </div>
  


    @endsection     
 