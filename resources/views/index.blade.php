<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/bootstrap-clockpicker.min.css')}}">

  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/vendors.css')}}">
  
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <title>{{ config('app.name') }}</title>
  <link rel="icon" href="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_FAVICON)}}">
  <!-- <title>@yield('title')</title> -->
  <link rel="apple-touch-icon" href="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_FAVICON)}}">
  <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_FAVICON)}}">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
  rel="stylesheet">
  <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/vendors.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/core/menu/menu-types/vertical-menu-modern.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/fonts/simple-line-icons/style.min.css')}}">
  
  
  <!-- END VENDOR CSS-->
  <!-- BEGIN MODERN CSS-->
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/app.css')}}">
  <!-- END MODERN CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/core/menu/menu-types/vertical-menu-modern.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/core/colors/palette-gradient.css')}}">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/assets/css/style.css')}}">
  <!-- END Custom CSS-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}">

  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/forms/toggle/switchery.min.css')}}">

  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/charts/c3.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/plugins/charts/c3-chart.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/vendors/css/charts/c3.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('public/app-assets/css/plugins/charts/c3-chart.css')}}">
  <style>
     .checked {
         color: orange;
        }
       ul {
         list-style-type: none;
        }
        .link_clr{
          color: white;
        }
        .card-body1 {
            flex: 1 1 auto;
           padding: 1.0rem;
        }
        .bg-info1 {
           background-color: #ff4961 !important;
        }  
        .card-header1 {
    padding: 0.5rem 0.5rem;
    margin-bottom: 0;
    background-color: #ff847c;
    border-bottom: 1px solid rgba(0, 0, 0, 0.06);
}
 .card-header2 {
    padding: 0.5rem 0.5rem;
    margin-bottom: 0;
    background-color: #ff4961;
    border-bottom: 1px solid rgba(0, 0, 0, 0.06);
}
.card-header3 {
    padding: 0.5rem 0.5rem;
    margin-bottom: 0;
    background-color: #91ca8f;
    border-bottom: 1px solid rgba(0, 0, 0, 0.06);
}
.card-header4 {
    padding: 0.5rem 0.5rem;
    margin-bottom: 0;
    background-color: #ce9160;
    border-bottom: 1px solid rgba(0, 0, 0, 0.06);
}
.error_message {
  color: red;
}
       
</style>
<script>
   function WebSocketTest() {
            
            if ("WebSocket" in window) {
               console.log("WebSocket is supported by your Browser!");
               
               // Let us open a web socket
               var ws = new WebSocket("ws://{{$_SERVER['HTTP_HOST']}}:8081");
        
               ws.onopen = function() {
                  console.log('Socket connected successfully!..');
               };
        
               ws.onmessage = function (evt) { 
                  try {
                    var received_msg = JSON.parse(evt.data);
                    console.log("Message is received..."+evt.data);
                    let id = received_msg.msg;
                    console.log("Message is received..."+evt.data);
                      if (id == {{Session::get('userid')}}) {
                      // call the alarm function
                      // document.getElementById('mySound').play();
                      var audio = new Audio('{{url('/')}}/public/sound/to-the-point.mp3');
                         var playPromise = audio.play();
                         document.getElementById('notification').hidden = false;
                      }
                  } catch (error) {
                      
                  }
                  
           

                  
               };
        
               ws.onclose = function() { 
                  console.log("Connection is closed..."); 
               };

            } else {
               console.log("WebSocket NOT supported by your Browser!");
            }
         }

         WebSocketTest();
  </script>
 
</head>
<style type="text/css">
  .main-menu.menu-dark .navigation {
    background: {{MENU_COLOR}} !important;
}
.main-menu.menu-dark .navigation > li.open > a {
    background: {{HIGHLIGHT_COLOR}} !important;
}
.main-menu.menu-dark .navigation > li > ul {
    background: {{MENU_COLOR}}  !important;
}
.navbar-semi-dark .navbar-header {
    background: {{MENU_COLOR}}  !important;
}
.main-menu.menu-dark {
    background: {{MENU_COLOR}}  !important;
}
</style>
<body class="vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
  <!-- fixed-top-->
  <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
          <li class="nav-item mr-auto">
            <a class="navbar-brand" href="{{url('/')}}/admin/dashboard" style="padding:0px;">
                <img class="brand-logo" alt="{{ config('app.name') }}" src="{{URL::asset(RESTAURANT_UPLOADS_PATH.SITE_LOGO)}}" style="width: 190px;margin-left: 10px;margin-top: 7px;"> 
            </a>
          </li>
        </ul>
      </div>
      <div class="navbar-container content">
        <div class="collapse navbar-collapse" id="navbar-mobile">
          <ul class="nav navbar-nav mr-auto float-left">
            <!--<li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
             <li class="dropdown nav-item mega-dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">Mega</a>
              <ul class="mega-dropdown-menu dropdown-menu row">
                <li class="col-md-2">
                  <h6 class="dropdown-menu-header text-uppercase mb-1"><i class="la la-newspaper-o"></i> News</h6>
                  <div id="mega-menu-carousel-example">
                    <div>
                      <img class="rounded img-fluid mb-1" src="{{URL::asset('public/app-assets/images/slider/slider-2.png')}}"
                      alt="First slide"><a class="news-title mb-0" href="#">Poster Frame PSD</a>
                      <p class="news-content">
                        <span class="font-small-2">January 26, 2018</span>
                      </p>
                    </div>
                  </div>
                </li>
                <li class="col-md-3">
                  <h6 class="dropdown-menu-header text-uppercase"><i class="la la-random"></i> Drill down menu</h6>
                  <ul class="drilldown-menu">
                    <li class="menu-list">
                      <ul>
                        <li>
                          <a class="dropdown-item" href="layout-2-columns.html"><i class="ft-file"></i> Page layouts & Templates</a>
                        </li>
                        <li><a href="#"><i class="ft-align-left"></i> Multi level menu</a>
                          <ul>
                            <li><a class="dropdown-item" href="#"><i class="la la-bookmark-o"></i>  Second level</a></li>
                            <li><a href="#"><i class="la la-lemon-o"></i> Second level menu</a>
                              <ul>
                                <li><a class="dropdown-item" href="#"><i class="la la-heart-o"></i>  Third level</a></li>
                                <li><a class="dropdown-item" href="#"><i class="la la-file-o"></i> Third level</a></li>
                                <li><a class="dropdown-item" href="#"><i class="la la-trash-o"></i> Third level</a></li>
                                <li><a class="dropdown-item" href="#"><i class="la la-clock-o"></i> Third level</a></li>
                              </ul>
                            </li>
                            <li><a class="dropdown-item" href="#"><i class="la la-hdd-o"></i> Second level, third link</a></li>
                            <li><a class="dropdown-item" href="#"><i class="la la-floppy-o"></i> Second level, fourth link</a></li>
                          </ul>
                        </li>
                        <li>
                          <a class="dropdown-item" href="color-palette-primary.html"><i class="ft-camera"></i> Color palette system</a>
                        </li>
                        <li><a class="dropdown-item" href="sk-2-columns.html"><i class="ft-edit"></i> Page starter kit</a></li>
                        <li><a class="dropdown-item" href="changelog.html"><i class="ft-minimize-2"></i> Change log</a></li>
                        <li>
                          <a class="dropdown-item" href="https://pixinvent.ticksy.com/"><i class="la la-life-ring"></i> Customer support center</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li class="col-md-3">
                  <h6 class="dropdown-menu-header text-uppercase"><i class="la la-list-ul"></i> Accordion</h6>
                  <div id="accordionWrap" role="tablist" aria-multiselectable="true">
                    <div class="card border-0 box-shadow-0 collapse-icon accordion-icon-rotate">
                      <div class="card-header p-0 pb-2 border-0" id="headingOne" role="tab"><a data-toggle="collapse" data-parent="#accordionWrap" href="#accordionOne"
                        aria-expanded="true" aria-controls="accordionOne">Accordion Item #1</a></div>
                      <div class="card-collapse collapse show" id="accordionOne" role="tabpanel" aria-labelledby="headingOne"
                      aria-expanded="true">
                        <div class="card-content">
                          <p class="accordion-text text-small-3">Caramels dessert chocolate cake pastry jujubes bonbon.
                            Jelly wafer jelly beans. Caramels chocolate cake liquorice
                            cake wafer jelly beans croissant apple pie.</p>
                        </div>
                      </div>
                      <div class="card-header p-0 pb-2 border-0" id="headingTwo" role="tab"><a class="collapsed" data-toggle="collapse" data-parent="#accordionWrap"
                        href="#accordionTwo" aria-expanded="false" aria-controls="accordionTwo">Accordion Item #2</a></div>
                      <div class="card-collapse collapse" id="accordionTwo" role="tabpanel" aria-labelledby="headingTwo"
                      aria-expanded="false">
                        <div class="card-content">
                          <p class="accordion-text">Sugar plum bear claw oat cake chocolate jelly tiramisu
                            dessert pie. Tiramisu macaroon muffin jelly marshmallow
                            cake. Pastry oat cake chupa chups.</p>
                        </div>
                      </div>
                      <div class="card-header p-0 pb-2 border-0" id="headingThree" role="tab"><a class="collapsed" data-toggle="collapse" data-parent="#accordionWrap"
                        href="#accordionThree" aria-expanded="false" aria-controls="accordionThree">Accordion Item #3</a></div>
                      <div class="card-collapse collapse" id="accordionThree" role="tabpanel" aria-labelledby="headingThree"
                      aria-expanded="false">
                        <div class="card-content">
                          <p class="accordion-text">Candy cupcake sugar plum oat cake wafer marzipan jujubes
                            lollipop macaroon. Cake dragée jujubes donut chocolate
                            bar chocolate cake cupcake chocolate topping.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li class="col-md-4">
                  <h6 class="dropdown-menu-header text-uppercase mb-1"><i class="la la-envelope-o"></i> Contact Us</h6>
                  <form class="form form-horizontal">
                    <div class="form-body">
                      <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="inputName1">Name</label>
                        <div class="col-sm-9">
                          <div class="position-relative has-icon-left">
                            <input class="form-control" type="text" id="inputName1" placeholder="John Doe">
                            <div class="form-control-position pl-1"><i class="la la-user"></i></div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="inputEmail1">Email</label>
                        <div class="col-sm-9">
                          <div class="position-relative has-icon-left">
                            <input class="form-control" type="email" id="inputEmail1" placeholder="john@example.com">
                            <div class="form-control-position pl-1"><i class="la la-envelope-o"></i></div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="inputMessage1">Message</label>
                        <div class="col-sm-9">
                          <div class="position-relative has-icon-left">
                            <textarea class="form-control" id="inputMessage1" rows="2" placeholder="Simple Textarea"></textarea>
                            <div class="form-control-position pl-1"><i class="la la-commenting-o"></i></div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12 mb-1">
                          <button class="btn btn-info float-right" type="button"><i class="la la-paper-plane-o"></i> Send </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </li>
              </ul>
            </li>
            <li class="nav-item nav-search"><a class="nav-link nav-link-search" href="#"><i class="ficon ft-search"></i></a>
              <div class="search-input">
                <input class="input" type="text" placeholder="Explore Modern...">
              </div>
            </li> -->
          </ul>
          <ul class="nav navbar-nav float-right">
          <li class="dropdown dropdown-language nav-item">
           <!--  <a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-gb"></i><span class="selected-language"></span></a> -->
              <div class="dropdown-menu" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-gb"></i> English</a>
                <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a>
                <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> Chinese</a>
                <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-de"></i> German</a>
              </div>
            </li>
           <li class="dropdown dropdown-notification nav-item">
            <form action="{{ url('admin/is_read') }}" id="isRead" method="POST">
                       {{ csrf_field() }}
                       </form>  
              <a class="nav-link nav-link-label" href="{{URL('/')}}/admin/neworder_list" data-toggle="dropdown" onclick="event.preventDefault();
               document.getElementById('isRead').submit();"><i class="ficon ft-bell"></i>
                <span class="badge badge-pill badge-default badge-danger badge-default badge-up badge-glow">{{TOTORDERCOUNT}}</span>
              </a>
              <!-- <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                <li class="dropdown-menu-header">
                  <h6 class="dropdown-header m-0">
                    <span class="grey darken-2">Notifications</span>
                  </h6>
                  <span class="notification-tag badge badge-default badge-danger float-right m-0">5 New</span>
                </li>
                <li class="scrollable-container media-list w-100">
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading">You have new order!</h6>
                        <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">30 minutes ago</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-download-cloud icon-bg-circle bg-red bg-darken-1"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading red darken-1">99% Server load</h6>
                        <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Five hour ago</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-alert-triangle icon-bg-circle bg-yellow bg-darken-3"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading yellow darken-3">Warning notifixation</h6>
                        <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Today</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-check-circle icon-bg-circle bg-cyan"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading">Complete the task</h6>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last week</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left align-self-center"><i class="ft-file icon-bg-circle bg-teal"></i></div>
                      <div class="media-body">
                        <h6 class="media-heading">Generate monthly report</h6>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last month</time>
                        </small>
                      </div>
                    </div>
                  </a>
                </li>
                <li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="javascript:void(0)">Read all notifications</a></li>
              </ul> -->
            </li>
             <li class="dropdown dropdown-notification nav-item">
             <!--  <a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-mail">             </i></a> -->
              <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                <li class="dropdown-menu-header">
                  <h6 class="dropdown-header m-0">
                    <span class="grey darken-2">Messages</span>
                  </h6>
                  <span class="notification-tag badge badge-default badge-warning float-right m-0">4 New</span>
                </li>
                <li class="scrollable-container media-list w-100">
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left">
                        <span class="avatar avatar-sm avatar-online rounded-circle">
                          <img src="{{URL::asset('public/app-assets/images/portrait/small/avatar-s-19.png')}}" alt="avatar"><i></i></span>
                      </div>
                      <div class="media-body">
                        <h6 class="media-heading">Margaret Govan</h6>
                        <p class="notification-text font-small-3 text-muted">I like your portfolio, let's start.</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Today</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left">
                        <span class="avatar avatar-sm avatar-busy rounded-circle">
                          <img src="{{URL::asset('public/app-assets/images/portrait/small/avatar-s-2.png')}}" alt="avatar"><i></i></span>
                      </div>
                      <div class="media-body">
                        <h6 class="media-heading">Bret Lezama</h6>
                        <p class="notification-text font-small-3 text-muted">I have seen your work, there is</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Tuesday</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left">
                        <span class="avatar avatar-sm avatar-online rounded-circle">
                          <img src="{{URL::asset('public/app-assets/images/portrait/small/avatar-s-3.png')}}" alt="avatar"><i></i></span>
                      </div>
                      <div class="media-body">
                        <h6 class="media-heading">Carie Berra</h6>
                        <p class="notification-text font-small-3 text-muted">Can we have call in this week ?</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Friday</time>
                        </small>
                      </div>
                    </div>
                  </a>
                  <a href="javascript:void(0)">
                    <div class="media">
                      <div class="media-left">
                        <span class="avatar avatar-sm avatar-away rounded-circle">
                          <img src="{{URL::asset('public/app-assets/images/portrait/small/avatar-s-6.png')}}" alt="avatar"><i></i></span>
                      </div>
                      <div class="media-body">
                        <h6 class="media-heading">Eric Alsobrook</h6>
                        <p class="notification-text font-small-3 text-muted">We have project party this saturday.</p>
                        <small>
                          <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">last month</time>
                        </small>
                      </div>
                    </div>
                  </a>
                </li>
                <li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="javascript:void(0)">Read all messages</a></li>
              </ul>
            </li>
            <li class="dropdown dropdown-user nav-item">
              <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                <span class="mr-1">{{trans('constants.welcome_title')}},
                  <span class="user-name text-bold-700">{{ucfirst(session()->get('user_name'))}}</span>
                </span>
                <span class="avatar avatar-online">
                  <img src="{{URL::asset('public/app-assets/images/portrait/small/avatar-s-19.png')}}" alt="avatar"><i></i></span>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{URL('/admin/setting/edit_store/'.session()->get('userid'))}}"><i class="ft-user"></i>Edit</a>
                <a class="dropdown-item" href="{{URL('/admin/setting/edit_account/'.session()->get('userid'))}}"><i class="ft-mail"></i>Account</a>
                <a class="dropdown-item" href="{{URL('/')}}/admin/neworder_list"><i class="ft-check-square"></i>Order</a>
                <!--<a class="dropdown-item" href="#"><i class="ft-message-square"></i> Chats</a>-->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{url('/')}}/admin/logout"><i class="ft-power"></i> {{trans('constants.logout_txt')}}</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
     @include('includes.navBar')
 </div>

   <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->
        <!-- FOR NEW ORDER NOTIFICATION ALERT -->
        <div class="row">
          <div class="col-md-3">
          </div>
          <div class="col-md-3">
          </div>
          <div class="col-md-6">
             <div class="card" id="notification">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="p-2 text-center bg-danger rounded-left">
                            <i class="fa fa-user-circle-o font-small-3 text-white"></i>
                        </div>
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">New Order Received. To view click <a href="{{url('/')}}/admin/orders/new">Here</a> <a onclick="myFunction()" style="float:right"> x </a></h5>
                           
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
 <script type="text/javascript">
    document.getElementById('notification').hidden = true;
</script>
  <!--  NEW ORDER NOTIFICATION ALERT ENDS-->

      <section id="minimal-statistics-bg">
          <div class="row">
            @if(Session::get('role') == 1)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <!-- <div class="p-2 text-center bg-danger rounded-left">
                             <i class="fa fa-user-circle-o font-large-2 text-white"></i> 
                        </div> -->
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All Users</h5>
                            <h5 class="text-bold-400">{{$total_users}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
            @if(Session::get('role') == 2)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All Orders</h5>
                            <h5 class="text-bold-400">{{$total_res_orders}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
             @if(Session::get('role') == 1)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="py-1 px-2 media-body">
                            <h5 class="success">All Delivery Boy</h5>
                            <h5 class="text-bold-400">{{$total_delivery_partners}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
             @if(Session::get('role') == 2)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="py-1 px-2 media-body">
                            <h5 class="success">Today Orders</h5>
                            <h5 class="text-bold-400">{{$today_orders}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
            @if(Session::get('role') == 2)
             <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">Today Comission</h5>
                            <h5 class="text-bold-400">${{$today_res_comission}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
             @if(Session::get('role') == 1)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">All Restaurant</h5>
                            <h5 class="text-bold-400">{{$total_restaurants}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
             @if(Session::get('role') == 2)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">

                        <div class="py-1 px-2 media-body">
                            <h5 class="warning">All Comission</h5>
                            <h5 class="text-bold-400">{{env('CURRENCY_CODE')}} {{$total_res_comission}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
             @if(Session::get('role') == 1)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="py-1 px-2 media-body">
                            <h5 class="warning">All Earnings</h5>
                            <h5 class="text-bold-400">{{env('CURRENCY_CODE')}} {{$total_earnings}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
          </div>
        </section>
         <section id="minimal-statistics-bg">
          <div class="row">
             @if(Session::get('role') == 1)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All Admin Comission</h5>
                            <h5 class="text-bold-400">{{env('CURRENCY_CODE')}} {{$total_admin_comission}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
             @if(Session::get('role') == 1)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="py-1 px-2 media-body">
                            <h5 class="success">Restaurant Comission</h5>
                            <h5 class="text-bold-400">{{env('CURRENCY_CODE')}} {{$total_restaurant_comission}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
             @if(Session::get('role') == 1)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="py-1 px-2 media-body">
                            <h5 class="info">Delivery Comission</h5>
                            <h5 class="text-bold-400">{{env('CURRENCY_CODE')}} {{$total_delivery_boy_comission}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
             @if(Session::get('role') == 1)
            <div class="col-xl-3 col-md-6 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="py-1 px-2 media-body">
                            <h5 class="danger">All Orders</h5>
                            <h5 class="text-bold-400">{{$total_res_orders}}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endif
          </div>
        </section>

        <!-- PRODUCTS SALES -->
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" style="padding-top:15px;">Weekly & Monthly Reports</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <div id="pie-chart" class="height-400 "></div>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="col-md-8">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">PRODUCTS SALES</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <div id="basic-area" class="height-400 echart-container"></div>
                  </div>
                </div>
              </div>
            </div> -->
            
          </div>
          <div class="content-body">
        <!-- Basic form layout section start -->


        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header">
                 <!--  <h4 class="card-title">NEW ORDER LISTS</h4> -->
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                </div>
               <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead> 
                          <tr>
                            <th>SI.No</th>
                            <th>Customer Name</th>
                            <th>Delivery People</th>
                            <!-- <th>Restaurant</th> -->
                            <th>Address</th>
                            <th>Cost</th>
                            <!-- <th>Status</th>  -->
                            <!-- <th>Order List</th> -->
                            <th>Order List</th>
                            <!-- <th>Action</th> -->
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1; ?>
                          @foreach($data as $d)
                          <tr>
                            <td>{{$i}}</td>
                            <td>{{$d->user_name?$d->user_name:'N/A'}}</td>
                            <td>Test Boy</td>
                            <!-- <td></td> -->
                            <td>{{$d->delivery_address}}</td>
                            <td> {{env('CURRENCY_CODE')}} {{$d->bill_amount}}</td>
                            <!-- <td> <span class="btn btn-info">COMPLETED</span> </td> -->
                            <!-- <td><button class="btn btn-primary" data-id="1">Order List</button></td> -->
                             <td>   
                              <div class="form-group">       
                                 <button type="button" class="btn btn-success shadow-box" data-id="1" data-toggle="modal"  data-target="#{{$d->order_id}}">  View Order  </button>

                                  <div class="modal animated slideInRight text-left" id="{{$d->order_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
                                   <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                     <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel76">View Order</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                     </div>
            <div class="modal-body">
                <div class="row m-0">
                    <dl class="order-modal-top">
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Order ID</dt>
                            <dd class="col-sm-9 order-txt orderid"><span>: </span>{{$d->order_id}}</dd>
                        </div>
                       <!--  <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Restaurant Name</dt>
                            <dd class="col-sm-9 order-txt rest_name"><span>: </span>Lido azul</dd>
                        </div> -->
                        
                        @if($d->user_name !='')
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Customer Name</dt>
                            <dd class="col-sm-9 order-txt cust_name"><span>: </span>{{$d->user_name}}</dd>
                        </div>
                        @endif
                        
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Address</dt>
                            <dd class="col-sm-9 order-txt address"><span>: </span>{{$d->delivery_address}}</dd>
                        </div>
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Phone Number</dt>
                            <dd class="col-sm-9 order-txt cust_phone"><span>: </span>{{$d->phone}}</dd>
                        </div>
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Delivery Date</dt>
                            <dd class="col-sm-9 order-txt cust_delivery_date"><span>: </span>{{$d->ordered_time}}</dd>
                        </div>
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Shop Rating</dt>
                            <dd class="col-sm-9 order-txt cust_order_note"><span>: {{$d->rating}} </span></dd>
                        </div>
                        <!--<div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Note</dt>
                            <dd class="col-sm-9 order-txt rate_shop"><span>: -- </span></dd>
                        </div>
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Delivery boy rating</dt>
                            <dd class="col-sm-9 order-txt rate_deliveryboy"><span>: -- </span></dd>
                        </div> -->
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Total Amount</dt>
                            <dd class="col-sm-9 order-txt tot_amt"><span>: </span>{{env('CURRENCY_CODE')}} {{$d->bill_amount}}</dd>
                            <br>
                            <br>
                        </div>
                        @if($d->status == 7)
                        <div class="row m-0">
                            <dt class="col-sm-3 order-txt p-0">Status</dt>
                            <dd class="col-sm-9 order-txt tot_amt"><span>: </span>Completed</dd>
                            <br>
                            <br>
                        </div>
                        @endif
                        

                       <!--   <div class="row m-0" id="order_status_list">
                          <dd class="col-sm-3 order-txt p-0">ORDERED</dd>                
                          <dd class="col-sm-9 order-txt "> 2018-03-17 14:24:15</dd>
                          <dd class="col-sm-3 order-txt p-0">RECEIVED</dd>                
                          <dd class="col-sm-9 order-txt "> 2018-03-17 14:24:25</dd>
                          <dd class="col-sm-3 order-txt p-0">ASSIGNED</dd>                
                          <dd class="col-sm-9 order-txt "> 2018-03-17 14:24:47</dd>
                          <dd class="col-sm-3 order-txt p-0">PROCESSING</dd>                
                          <dd class="col-sm-9 order-txt "> 2018-03-17 14:24:56</dd>
                          <dd class="col-sm-3 order-txt p-0">REACHED</dd>                
                          <dd class="col-sm-9 order-txt "> 2018-03-17 14:25:00</dd>
                          <dd class="col-sm-3 order-txt p-0">PICKEDUP</dd>                
                          <dd class="col-sm-9 order-txt "> 2018-03-17 14:25:04</dd>
                          <dd class="col-sm-3 order-txt p-0">ARRIVED</dd>                
                          <dd class="col-sm-9 order-txt "> 2018-03-17 14:25:07</dd>
                          <dd class="col-sm-3 order-txt p-0">COMPLETED</dd>                
                          <dd class="col-sm-9 order-txt "> 2018-03-17 14:25:08</dd>
                        </div> -->
                        <hr>
                    </dl>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <!-- <th>Product Image</th> -->
                                    <th>Product Name</th>
                                    <th>Note</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Cost</th>
                                </tr>
                            </thead>
                            <tbody class="cartstbl">
                              @foreach($data1 as $d1)
                              @if($d1->request_id == $d->request_id)
                              <tr>
                                <!-- <td>
                                  <img src="http://ecx.images-amazon.com/images/I/51bRhyVTVGL._SL50_.jpg" width="100px" alt=""></td> -->
                                  <td>{{$d1->food_name}}</td>
                                  <td>null</td>
                                  <td>{{$d1->price}}</td>
                                  <td>{{$d1->quantity}}</td>
                                  <td>{{$d1->price * $d1->quantity}}</td>
                                </tr>
                                @endif
                                @endforeach
                              </tbody>
                            <tfoot>
                              <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Packaging Charge</th>
                                    <th ><span>: </span> {{$d->restaurant_packaging_charge}}</th>
                                    <th> </th>
                                </tr>
                                 <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Discount</th>
                                    <th class="discount"><span>: </span> {{$d->offer_discount}}</th>
                                    <th> </th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Delivery Charge</th>
                                    <th class="delivery_charge"><span>: </span> {{$d->delivery_charge}}</th>
                                    <th> </th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Tax</th>
                                    <th class="tax"><span>: </span> {{$d->tax}}</th>
                                    <th> </th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Total</th>
                                    <th class="tot_amt"><span>: </span> {{$d->bill_amount}}</th>
                                    <th> </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
                            </div>
                          </div>
                        </div>
                      </td>
                      <!-- <td>
                        @if($d->order_status==0)
                        <a href="{{url('/')}}/admin/accept_request/{{$d->request_id}}" class="btn btn-info">Accept</a>
                        <a href="{{url('/')}}/admin/cancel_request/{{$d->request_id}}" class="btn btn-info">Cancel Order</a>
                        @endif
                         @if($d->order_status==1)
                        <a href="{{url('/')}}/admin/assign_request/{{$d->request_id}}" class="btn btn-info">Assign</a>
                        <a href="{{url('/')}}/admin/cancel_request/{{$d->request_id}}" class="btn btn-info">Cancel Order</a>
                        @endif
                        @if($d->order_status==2)
                        <a href="#">Delivery boy assigned</a>
                        @endif
                        @if($d->order_status==3)
                        <a href="#">Food delivered to Delivery boy</a>
                        @endif
                        @if($d->order_status==4)
                        <a href="#">Towards Customer</a>
                        @endif
                        @if($d->order_status==5)
                        <a href="#">Reached Customer</a>
                        @endif
                        @if($d->order_status==6)
                        <a href="#">Delivered to Customer</a>
                        @endif
                        @if($d->order_status==7)
                        <a href="#">Completed</a>
                        @endif
                        @if($d->order_status == 10)
                        <a href="#">Cancelled</a>
                        @endif
                      </td> -->
                 </tr>
                 <?php $i++; ?>
                 @endforeach

                  </tbody>
                 </table>
                </div>
                <br><br>
           <!--   <div class="card-block">
               <h3>Total Earning:- </h3>
                     <div class="row m-1">
                        <dt class="col-sm-3 order-txt p-0">Total Earning</dt>
                        <dd class="col-sm-9 order-txt "><span>: ₹58067.00</span></dd>
                    </div>
                    <div class="row m-1">
                        <dt class="col-sm-3 order-txt p-0">Commision from Food Items</dt>
                        <dd class="col-sm-9 order-txt "><span>: ₹2519.00</span> </dd>
                    </div>
                    <div class="row m-1">
                        <dt class="col-sm-3 order-txt p-0">Commision from Delivery Charge</dt>
                        <dd class="col-sm-9 order-txt "><span>: ₹53.50</span> </dd>
                    </div>
                    <div class="row m-1">
                        <dt class="col-sm-3 order-txt p-0">Total Commision </dt>
                        <dd class="col-sm-9 order-txt "><span>: ₹2572.50</span> </dd>
                    </div>
                </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- // Basic form layout section end -->
      </div>

            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" style="padding-top:15px;">TOTAL SALES</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <!-- <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div> -->
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <div id="basic-area" class="height-400 echart-container"></div>
                  </div>
                </div>
              </div>
            </div>


      </div>
    </div>
  </div>
   <script src="{{URL::asset('public/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/core/app.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/dropdowns/dropdowns.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/tables/components/table-components.js')}}"
  type="text/javascript"></script>


   <script src="{{URL::asset('public/app-assets/vendors/js/charts/d3.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/c3.min.js')}}" type="text/javascript"></script>
  
  <script src="{{URL::asset('public/app-assets/js/scripts/charts/c3/bar-pie/bar.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/charts/c3/bar-pie/column.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/charts/c3/bar-pie/donut.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/charts/c3/bar-pie/pie.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/charts/c3/bar-pie/stacked-bar.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/charts/c3/bar-pie/stacked-column.js')}}"
  type="text/javascript"></script>

 <script src="{{URL::asset('public/app-assets/vendors/js/charts/echarts/echarts.js')}}" type="text/javascript"></script>
   <script src="{{URL::asset('public/app-assets/vendors/js/charts/echarts/chart/line.js')}}" type="text/javascript"></script>
   <script src="{{URL::asset('public/app-assets/vendors/js/charts/echarts/chart/bar.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('public/app-assets/js/scripts/charts/echarts/line-area/basic-area.js')}}"
  type="text/javascript"></script>

  <!--FOR NEW ORDER NOTIFICATION ALERT  -->
<script>

function myFunction() {
  document.getElementById('notification').hidden = true;
}
</script>
 <!--FOR NEW ORDER NOTIFICATION ALERT ENDS -->
 
  <script type="text/javascript">
  $(window).on("load", function(){

    // Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
    var pieChart = c3.generate({
        bindto: '#pie-chart',
        color: {
            pattern: ['#ff847c','#ff4961','#91ca8f', '#ce9160']
        },

        data: {
            // iris data from R
            columns: [
               
            ],
            type : 'pie',
            onclick: function (d, i) { console.log("onclick", d, i); },
            onmouseover: function (d, i) { console.log("onmouseover", d, i); },
            onmouseout: function (d, i) { console.log("onmouseout", d, i); }
        }
    });

    // Instantiate and draw our chart, passing in some options.
    setTimeout(function () {
        pieChart.load({
            columns: [
                ["Current Week Earnings", {{$current_week}}],
                ["Current Month Earnings", {{$current_month}}],
                ["Last Week Earnings", {{$last_week}}],
                ["Last Month Earnings", {{$last_month}}],
            ]
        });
    }, 1500);

   
    $(".menu-toggle").on('click', function() {
        pieChart.resize();
    });


    var pieChart1 = c3.generate({
        bindto: '#pie-chart1',
        color: {
            pattern: ['#91ca8f','#ce9160']
        },

        data: {
            // iris data from R
            columns: [
               
            ],
            type : 'pie',
            onclick: function (d, i) { console.log("onclick", d, i); },
            onmouseover: function (d, i) { console.log("onmouseover", d, i); },
            onmouseout: function (d, i) { console.log("onmouseout", d, i); }
        }
    });

    // Instantiate and draw our chart, passing in some options.
    setTimeout(function () {
        pieChart1.load({
            columns: [
                ["Last Year Earnings", ],
                ["Current Year Earnings", ],
               
            ]
        });
    }, 1500);

   
    $(".menu-toggle").on('click', function() {
        pieChart1.resize();
    });
});
</script>


 @if(Session::get('role') == 1)
<script type="text/javascript">
  
  $(window).on("load", function(){

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: '../../../app-assets/vendors/js/charts/echarts'
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],


        // Charts setup
        function (ec) {
            // Initialize chart
            // ------------------------------
            var myChart = ec.init(document.getElementById('basic-area'));

            // Chart Options
            // ------------------------------
            chartOptions = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['Admin Comission', 'Store Comission', 'DeliveryBoy Comission']
                },

                // Add custom colors
                color: ['#FF4961', '#40C7CA', '#FF9149'],

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'
                    ]
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: 'Admin Comission',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'}}},
                        data: [{{$admin_earnings}}]
                    },
                    {
                        name: 'Store Comission',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'}}},
                        data: [{{$restaurant_earnings}}]
                    },
                    {
                        name: 'DeliveryBoy Comission',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'}}},
                        data: [{{$delivery_boy_earnings}}]
                    }
                ]
            };

            // Apply options
            // ------------------------------

            myChart.setOption(chartOptions);

            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        // Resize chart
                        myChart.resize();
                    }, 200);
                }
            });
        }
    );
});
</script>
@endif

 @if(Session::get('role') == 2)
<script type="text/javascript">
  
  $(window).on("load", function(){

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: '../../../app-assets/vendors/js/charts/echarts'
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],


        // Charts setup
        function (ec) {
            // Initialize chart
            // ------------------------------
            var myChart = ec.init(document.getElementById('basic-area'));

            // Chart Options
            // ------------------------------
            chartOptions = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['Admin Comission', 'Store Comission', 'DeliveryBoy Comission']
                },

                // Add custom colors
                color: ['#FF4961', '#40C7CA', '#FF9149'],

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'
                    ]
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: 'Admin Comission',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'}}},
                        data: [{{$res_admin_earnings}}]
                    },
                    {
                        name: 'Store Comission',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'}}},
                        data: [{{$res_restaurant_earnings}}]
                    },
                    {
                        name: 'DeliveryBoy Comission',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'}}},
                        data: [{{$res_delivery_boy_earnings}}]
                    }
                ]
            };

            // Apply options
            // ------------------------------

            myChart.setOption(chartOptions);

            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        // Resize chart
                        myChart.resize();
                    }, 200);
                }
            });
        }
    );
});
</script>
@endif
