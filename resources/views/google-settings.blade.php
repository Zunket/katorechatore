@extends('layout.master')

@section('title')
{{APP_NAME}}
@endsection

@section('content')
 <div class="content-wrapper">
     <!--  <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new"> -->
          <!-- <h3 class="content-header-title mb-0 d-inline-block">{{ strtoUpper(trans('constants.google_setting')) }}</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}/admin/dashboard">{{strtoUpper(trans('constants.dashboard'))}}</a></li>
                <li class="breadcrumb-item"><a href="#">{{ strtoUpper(trans('constants.google_setting')) }}</a>
                </li>
              </ol>
            </div>
          </div> -->
        <!-- </div>
      </div> -->
      <div class="content-body">
        <section id="icon-tabs">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title"> &nbsp;</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                  <!-- <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div> -->
                </div>
                   
                <div class="card-content collapse show">
                  <div class="card-body">
                    <form action="{{url('/')}}/admin/update-setting" class="icons-tab-steps wizard-notification" method="post" enctype="multipart/form-data">
                       <input type="hidden" name="_token" value="{{csrf_token()}}">
                       <input type="hidden" name="type" value="google">
                     <fieldset>
                      <div class="row">
                       <div class="col-md-12">  
                       @if(isset($data['google_api_key']))
                         <div class="form-group row">                        
                           <label class="col-md-2">{{ trans('constants.google_api_key') }} </label>                          
                           <div class="col-md-10">
                               <input type="text" name="google_api_key" class="form-control" placeholder="Title of the message" required="" value="@if(isset($data['google_api_key'])){{$data['google_api_key']}}@endif">
                          </div>
                       </div>
                       @endif
                       @if(isset($data['firebase_url']))
                         <div class="form-group row">                        
                           <label class="col-md-2">{{ trans('constants.firebase_url') }} </label>                          
                           <div class="col-md-10">
                               <input type="text" name="firebase_url" class="form-control" placeholder="Title of the message" required="" value="@if(isset($data['firebase_url'])){{$data['firebase_url']}}@endif">
                          </div>
                       </div>
                       @endif
                       @if(isset($data['user_notification_key']))
                         <div class="form-group row">                        
                           <label class="col-md-2">{{ trans('constants.user_notification_key') }} </label>                          
                           <div class="col-md-10">
                               <input type="text" name="user_notification_key" class="form-control" placeholder="Title of the message" required="" value="@if(isset($data['user_notification_key'])){{$data['user_notification_key']}}@endif">
                          </div>
                       </div>
                       @endif
                       @if(isset($data['partner_notification_key']))
                         <div class="form-group row">                        
                           <label class="col-md-2">{{ trans('constants.partner_notification_key') }} </label>                          
                           <div class="col-md-10">
                               <input type="text" name="partner_notification_key" class="form-control" placeholder="Title of the message" required="" value="@if(isset($data['partner_notification_key'])){{$data['partner_notification_key']}}@endif">
                          </div>
                       </div>
                       @endif
                       <div class="card card-block card-inverse card-primary">
                    <blockquote class="card-blockquote">
                        <i class="fa fa-3x fa-cc-stripe pull-right"></i>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <label for="stripe_secret_key" class="col-form-label">
                                    Stripe (Card Payments)
                                </label>
                            </div>
                            <div class="col-md-10">
                                <input @if($data['CARD'] == 1) checked  @endif  name="CARD" id="stripe_check" onchange="cardselect()" type="checkbox" class="js-switch" data-color="#43b968">
                            </div>
                        </div>
                        <div id="card_field" @if($data['CARD'] == 0) style="display: none;" @endif>
                            <div class="form-group row">
                              <div class="col-md-2">
                                <label for="stripe_secret_key" class="col-xs-4 col-form-label">Stripe Secret key</label>
                              </div>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" value="@if(isset($data['stripe_sk_key'])){{$data['stripe_sk_key']}}@endif" name="stripe_sk_key" id="stripe_secret_key"  placeholder="Stripe Secret key">
                                </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-2">
                                <label for="stripe_publishable_key" class="col-xs-4 col-form-label">Stripe Publishable key</label>
                              </div>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" value="@if(isset($data['stripe_pk_key'])){{$data['stripe_pk_key']}}@endif" name="stripe_pk_key" id="stripe_publishable_key"  placeholder="Stripe Publishable key">
                                </div>
                            </div>
                        </div>
                    </blockquote>
                </div>
                       <!-- @if(isset($data['stripe_pk_key']))
                         <div class="form-group row">                        
                           <label class="col-md-2">{{ trans('constants.stripe_pk_key') }} </label>                          
                           <div class="col-md-10">
                               <input type="text" name="stripe_pk_key" class="form-control" placeholder="Title of the message" required="" value="@if(isset($data['stripe_pk_key'])){{$data['stripe_pk_key']}}@endif">
                          </div>
                       </div>
                       @endif
                       @if(isset($data['stripe_sk_key']))
                         <div class="form-group row">                        
                           <label class="col-md-2">{{ trans('constants.stripe_sk_key') }} </label>                          
                           <div class="col-md-10">
                               <input type="text" name="stripe_sk_key" class="form-control" placeholder="Title of the message" required="" value="@if(isset($data['stripe_sk_key'])){{$data['stripe_sk_key']}}@endif">
                          </div>
                       </div>
                       @endif -->
                       <div class="card card-block card-inverse card-primary">
                        <blockquote class="card-blockquote">
                            <i class="fa fa-3x fa-money pull-right"></i>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <label for="cash-payments" class="col-form-label">
                                        Cash Payments
                                    </label>
                                </div>
                                <div class="col-md-10">
                                    <input @if($data['CASH'] == 1) checked  @endif name="CASH" id="cash-payments" onchange="cardselect()" type="checkbox" class="js-switch" data-color="#43b968">
                                </div>
                            </div>
                        </blockquote>
                        </div>
                        <div class="card card-block">
          <blockquote class="card-blockquote">
             <div class="form-group row">
                <div class="col-md-2">
                  <label for="stripe_secret_key" class="col-form-label">
                    Paypal Setting
                  </label>
                </div>
                <div class="col-xs-6">
                  <input @if($data['PAYPAL'] == 1) checked  @endif  name="PAYPAL" id="paypal_check" onchange="paypalselect()" type="checkbox" class="js-switch" data-color="#43b968">
                </div>
              </div>
                 <div id="paypal_field" @if($data['PAYPAL'] == 0) style="display: none;" @endif>
              <div class="form-group row">
                <div class="col-md-2">
                <label for="PAYPAL_CLIENT_ID" class="col-xs-4 col-form-label">Paypal Client Id</label>
              </div>
                <div class="col-md-10">
                  <input class="form-control" type="text" value="@if(isset($data['paypal_client_id'])){{$data['paypal_client_id']}}@endif" name="paypal_client_id" required id="PAYPAL_CLIENT_ID" placeholder=">Paypal Client Id">
                </div>
              </div>
              
              <div class="form-group row">
                <div class="col-md-2">
                <label for="PAYPAL_SECRET" class="col-xs-4 col-form-label">Pay Secret Key</label>
              </div>
                <div class="col-md-10">
                  <input class="form-control" type="text" value="@if(isset($data['paypal_sk_key'])){{$data['paypal_sk_key']}}@endif" name="paypal_sk_key" required id="PAYPAL_SECRET" placeholder="Contact Email">
                </div>
              </div>
            </blockquote>
          </div>
        </div>
                        <!-- @if(isset($data['paypal_client_id']))
                         <div class="form-group row">                        
                           <label class="col-md-2">PayPal Client Id </label>                          
                           <div class="col-md-10">
                               <input type="text" name="paypal_client_id" class="form-control" placeholder="Title of the message" required="" value="@if(isset($data['paypal_client_id'])){{$data['paypal_client_id']}}@endif">
                          </div>
                       </div>
                       @endif
                       @if(isset($data['paypal_sk_key']))
                         <div class="form-group row">                        
                           <label class="col-md-2">PayPal Secret Key</label>                          
                           <div class="col-md-10">
                               <input type="text" name="paypal_sk_key" class="form-control" placeholder="Title of the message" required="" value="@if(isset($data['paypal_sk_key'])){{$data['paypal_sk_key']}}@endif">
                          </div>
                       </div>
                       @endif -->
                       <div class="form-group row">
                        <label class="col-md-2"></label>
                          <div class="col-md-10">
                            <button type="submit" class="btn btn-primary mr-1" style="padding: 10px 15px;">Update Settings</button> &nbsp;
                             <!-- <button type="button" class="btn btn-success mr-1" style="padding: 10px 15px;">Schedule Push</button> -->
                        </div>
                       </div>
                      </div>
                     </div>
                    </div>
                   </fieldset>
                  </form>
                 </div>
                </div>
               </div>
              </div>
             </div>
            </section>
           </div>
   
<script type="text/javascript">
function cardselect()
{
    if($('#stripe_check').is(":checked")) {
        $("#card_field").fadeIn(700);
    } else {
        $("#card_field").fadeOut(700);
    }
}
function paypalselect()
{
    if($('#paypal_check').is(":checked")) {
        $("#paypal_field").fadeIn(700);
    } else {
        $("#paypal_field").fadeOut(700);
    }
}
</script>
</script>

    @endsection     
 