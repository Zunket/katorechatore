<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    //
    protected $table = 'requests';

    /**
    * set relationship to food request.
    *
    */
    public function user()
    {
        return $this->hasMany('App\Model\Users','user_id','id');
    }
}
