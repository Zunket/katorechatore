<?php

namespace App\Http\Middleware;

use Closure;
class checkApiGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $errors = array();
        $errors['status'] = false;

        $auth_id = $request->header('authId');
        $authToken = $request->header('authToken');
        if ($auth_id == 0) {
            $errors['message'] = 'Auth id , Auth token doesn`t match';
            return response()->json($errors,401);
        }

        $request->authId = $auth_id;
        $request->authToken = $authToken;
        return $next($request);
    }
}
