<?php

namespace App\Http\Controllers\admin;
                                    
use Validator;
use App\Model\PushNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController;
use DB;

class NoteboardController extends BaseController
{

    /**
     * function to get notification list 
     * @param no param
     * @return array o blade file
     */

	public function noticeboard_list()
	{
		
		return view('noticeboard_list');
	}

    /**
     * function to View add_noticeboard blade file 
     * @param no param
     * @return view add_noticeboard
     */

	public function add_noticeboard()
	{
		return view('add_noticeboard');
	}

    /**
     * function to View custum push blade file 
     * @param no param
     * @return view custumpush
     */

	public function custumpush()
	{
		return view('custumpush');
	}
	public function notification_list()
	{
		$data = PushNotification::get();
		$total_users = $this->users::count();
		$total_delivery_boy = $this->deliverypartners::count();
		return view('pushnotification_list',compact('data','total_users','total_delivery_boy'));
	}

	public function delete_notification(Request $request){
		
         PushNotification::where('id',$request->id)->delete();
         return back()->with('success','Notification Removed');
    }

    /**
     * function to send push notification based send to user , provider or all 
     * @param Request param
     * @return back with success response
     */

	public function send_custumpush(Request $request)
	{

		$validator = Validator::make($request->all(), [
            'send_to' => 'required',
            'title' => 'required',
            'message' => 'required',
        ]);
		

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages)->withInput();

        }else{

        	$send_to = $request->send_to;
        	$message = $request->message;
        	$title = $request->title;

			if($send_to=='ALL') $data = $this->users::get(); $provider = $this->deliverypartners::get();	
			if($send_to=='USERS') $data = $this->users::get();
			if($send_to=='PROVIDERS') $data = $this->deliverypartners::get();

		    $push_notification = new PushNotification;
            $push_notification->title = $title;
            $push_notification->notification_text = $message;
            $push_notification->send_to = $send_to;
            $push_notification->from_user = 0;
            $UserNew = array();
			foreach ($data as $key => $value) {
				# code...
                $userNew[] = $value['id'];
				if($value->device_token!='' && $value->device_type!='')
				{

					
					$this->user_send_push_notification($value->device_token, $value->device_type, $title, $message);
				}
				$userids = implode(',',$userNew);
			}
            
			/*if($send_to=='ALL')
			{
                $providerNew = array();
				foreach ($provider as $key => $value) {
					$providerNew[] = $value['id'];
					# code...
				if(isset($value->device_token) && $value->device_token!='' && isset($value->device_type) && $value->device_type!='')
					{
					
						$this->user_send_push_notification($value->device_token, $value->device_type, $title, $message);
					}
				}
				$providerids = implode(',',$providerNew);
			}*/
			$push_notification->to_user = $userids;
			$saved = $push_notification->save();
        }

		return back()->with('success',trans('constants.push_notification').trans('constants.send_success_msg'));
	}
}