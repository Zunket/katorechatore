<?php

namespace App\Http\Controllers\admin;
                                    
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController;
use DB;
use Auth;
use Hash;
use App\Model\Restaurants;
class LoginController extends BaseController
{

    /**
     * return to view login screen
     * 
     * @return view page
     */
    public function index()
    {
        //dd('hii');
        return view('login');
    }

    
    /**
     * admin and restaurant login check
     * 
     * @param object $request
     * 
     * @return view page to home screen
     */
	public function login(Request $request){

        // if(!$request->email || !$request->password){

        //     return back()->with('error','Wrong email or password try again');
        // }

        $credentials = $request->only('email', 'password');
               //dd(Hash::make($request->password));
       
        if(Auth::attempt($credentials)){

            $request->session()->put('userid', auth()->user()->id);
            $request->session()->put('user_name', auth()->user()->name);
            $request->session()->put('role', 1);
            return redirect('admin/dashboard');
        }else
        {
            if(Auth::guard('restaurant')->attempt($credentials))
            { 
                $request->session()->put('userid', auth()->guard('restaurant')->user()->id);
                $request->session()->put('user_name', auth()->guard('restaurant')->user()->restaurant_name);
                $request->session()->put('role',2);

                $url = 'admin/dashboard/';
                return redirect($url);
            }else{
                return back()->with('error','Wrong email or password try again');
            }
        }

    }

    public function logout(Request $request)
    {
       $request->session()->forget('userid');
       Auth::logout();
       return redirect('/admin')->with('success','Logout success');
    }

    public function registerLogin(){
        
        return view('register.index');
    }


    public function register(Request $request){
      
      
            $this->validate($request,[
                'restaurant_name' => 'required|unique:restaurants',
                'email' => 'required|email|unique:restaurants',
                'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'min:6'
            ],[
                'restaurant_name.required' => 'Restaurant  name field is required.',
                'restaurant_name.unique' => 'Restaurant  name has already been taken.'
            ]);
            try {
              $user = new  Restaurants;
              $user->restaurant_name = $request->restaurant_name;
              $user->email = $request->email;
              $password = bcrypt($request->password);
              $user->password = $password;
              if($user->save()){
                return redirect()->back()->with('success','User Registered Successfully.');
              }else{
                return redirect()->back()->with('error','Something Went Wrong.');
              }

            } catch (\Exception $e) {
                return redirect()->back()->with('error','Something Went Wrong.');
            }
    }
}