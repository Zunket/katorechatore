<?php

namespace App\Http\Controllers\admin;
                                    
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController;
use Carbon\Carbon;
use App\Model\Deliverypartners;
use App\Model\DriverList;
use App\Model\City_geofencing;
use DB;
use Hash;

class RestaurantController extends BaseController
{

    public function restaurant_list(Request $request)
    {
        $data = $this->restaurants->with('Foodrequest')->get();
        $total_restaurants = DB::table('restaurants')->select('id')->count();
        $active_total_restaurants = DB::table('restaurants')->where('status',1)->select('id')->count();
        $inactive_total_restaurants = DB::table('restaurants')->where('status',0)->select('id')->count();
       //dd($data[0]->Foodrequest[0]);
        return view('restaurant_list',['data'=>$data,'total_restaurants'=>$total_restaurants,'active_total_restaurants'=>$active_total_restaurants,'inactive_total_restaurants'=>$inactive_total_restaurants]);
    }

    public function restaurant()
    {
        $city = $this->addcity->get();
        $area = $this->addarea->get();

        $cuisines = $this->cuisines->get();
        $document = $this->document->where('document_for',2)->where('status',1)->get();
        $title = "ADD RESTAURANT";

        return view('/add_restaurant',['city'=>$city,'area'=>$area,'title'=>$title,'cuisines'=>$cuisines,'document'=>$document]);
    }

    public function edit_restaurant($id,Request $request)
    {

        $city = $this->addcity->get();
        $area = $this->addarea->get();
        $cuisines = $this->cuisines->get();
        $title = "EDIT RESTAURANT";
        $restaurants = $this->restaurants;
        $restaurant_detail = $restaurants->where('id',$id)->with(['Cuisines','Document','RestaurantBankDetails'])->first();
        $document = $this->document->where('document_for',2)->where('status',1)->get();
        //dd($restaurant_detail->document);
        $cuisine_ids = array();
        foreach($restaurant_detail->Cuisines as $val){
            $cuisine_ids[] = $val->id;
        }
       // dd($restaurant_detail);
        return view('add_restaurant',['cuisine_ids'=>$cuisine_ids,'data'=>$restaurant_detail,'city'=>$city,'area'=>$area,'title'=>$title,'cuisines'=>$cuisines,'document'=>$document]);
    }
    
    public function review(){
        $data = $this->review->with('user','restaurant')->get();
        return view('review',['data'=>$data]);
    }
    
    public function delete_rating(Request $request){
         $this->review->where('id',$request->id)->delete();
         return back()->with('success','Rating Removed');
    }

    public function add_to_restaurants(Request $request)
    {
     //dd($request->all());
        $rules = array(
            'name' => 'required|max:50',
            'password' => 'required',
            'city' => 'required',
            'area' => 'required',
            'status' => 'required',
            'opening_time' => 'required',
            'closing_time' => 'required',
            'weekend_opening_time' => 'required',
            'weekend_closing_time' => 'required',
            'estimated_delivery_time' => 'required',
            'address' => 'required',
            'packaging_charge' => 'required',
            //'offer_percentage' => 'required',
            'delivery_type' => 'required|array',
            'cuisines' => 'required|array',
            /*'account_name' => 'required',
            'account_address' => 'required',
            'account_no' => 'required',
            'bank_name' => 'required',
            'branch_name' => 'required',
            'branch_address' => 'required',*/
        );
        if($request->id!='')
        {
            $rules['email'] = 'required|unique:restaurants,email,'.$request->id;
            $rules['phone'] = 'required|numeric|unique:restaurants,phone,'.$request->id;
        }else
        {
            $rules['image'] = 'required|max:2048|mimes:jpeg,jpg,bmp,png';
            $rules['email'] = 'required|unique:restaurants,email';
            $rules['phone'] = 'required|numeric|unique:restaurants,phone';
            $rules['status'] = 'required';
        }
        // foreach($request->document as $key=>$value){
        //     $rules['document.document.*'] = 'max:2048';
        // }
       // dd($rules);
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
            //dd($request->document);
                $restaurants = $this->restaurants;
                $custom = $this->custom;
                $name = $request->name;
                $email = $request->email;
                $phone = $request->phone;
                $city = $request->city;
                $area = $request->area;
                $discount_type = $request->discount_type;
                $target_amount = $request->target_amount;
                $offer_amount = $request->offer_amount;
                $admin_commision =  $request->admin_commision;
                 if(isset($request->status))
                {
                 $status = $request->status;
                }
                else
                {
                $status=0;
                }
                $opening_time = date("H:i:s",strtotime($request->opening_time));
                $closing_time = date("H:i:s",strtotime($request->closing_time));
                $weekend_opening_time = date("H:i:s",strtotime($request->weekend_opening_time));
                $weekend_closing_time = date("H:i:s",strtotime($request->weekend_closing_time));
                if($request->image)
                {
                    $image = $custom->restaurant_upload_image($request,'image');
                }else
                {
                    $image=PROFILE_ICON;
                }
                $packaging_charge = $request->packaging_charge;
                
                if($request->shop_description)
                {
                $shop_description = $request->shop_description;
                }else
                {
                    $shop_description = "";
                }
                $estimated_delivery_time = $request->estimated_delivery_time;
                $address = $request->address;

                if($request->id)
                {
                    $restaurants_det = $restaurants->find($request->id);
                    if(!$request->image)
                    {
                        $image = $restaurants_det->image;
                    }
                    $restaurants_det->restaurant_name = $name;
                    $restaurants_det->image = $image;
                    $restaurants_det->email = $email;
                    $restaurants_det->org_password = $request->password;
                    $restaurants_det->password = Hash::make($request->password);
                    $restaurants_det->phone = $phone;
                    $restaurants_det->city = $city;
                    $restaurants_det->area = $area;
                    $restaurants_det->discount_type = $discount_type;
                    $restaurants_det->target_amount = $target_amount;
                    $restaurants_det->offer_amount = $offer_amount;
                    $restaurants_det->admin_commision = $admin_commision;
                    $restaurants_det->restaurant_delivery_charge = $request->restaurant_delivery_charge;
                    $restaurants_det->driver_commision = $request->driver_commision;
                    //$restaurants_det->discount = $offer_percentage;
                    $restaurants_det->shop_description = $shop_description;
                    $restaurants_det->is_open = 0;
                    $restaurants_det->estimated_delivery_time = $estimated_delivery_time;
                    $restaurants_det->packaging_charge = $packaging_charge;
                    $restaurants_det->address = $address;
                    $restaurants_det->opening_time = $opening_time;
                    $restaurants_det->closing_time = $closing_time;
                    $restaurants_det->weekend_opening_time = $weekend_opening_time;
                    $restaurants_det->weekend_closing_time = $weekend_closing_time;
                    $restaurants_det->status = $status;
                    $restaurants_det->is_vip = $request->is_vip;
                    $restaurants_det->is_24x7 = $request->is_24x7;
                    $restaurants_det->is_home_service = $request->is_home_service;
                    $restaurants_det->delivery_type = json_encode($request->delivery_type);

                    $restaurants_det->save();
                                
                    $cuisines = $this->cuisines->find($request->cuisines);
                    //update many to many relationship data
                    $restaurants_det->Cuisines()->sync($cuisines);

                    //data insert into document many to many
                    $sync_data=array();
                    if(!empty($request->document)){
                        foreach($request->document as $key=>$value){
                            if($_FILES['document']['name'][$key]['document']!='')
                            {
                                $expiry_date='';
                                if(isset($value['date']) && $value['date']!=null) $expiry_date=date("Y-m-d",strtotime($value['date']));

                                $filename = strtotime(date("Y-m-d")).basename($_FILES['document']['name'][$key]['document']);
                                move_uploaded_file($_FILES["document"]["tmp_name"][$key]['document'], 'public/uploads/Restaurant Document/'.$filename);                                
                                
                                $sync_data[$key] = ['document' => $filename,'expiry_date'=>$expiry_date];

                                // if($expiry_date!='' && $_FILES['document']['name'][$key]['document']!='')
                                //     $sync_data[$key] = ['document' => $filename,'expiry_date'=>$expiry_date];
                                // elseif($expiry_date==''  && $_FILES['document']['name'][$key]['document']!='')
                                //     $sync_data[$key] = ['document' => $filename, ,'expiry_date'=>$expiry_date];
                                // elseif($expiry_date!=''  && $_FILES['document']['name'][$key]['document']=='')
                                //     $sync_data[$key] = ['expiry_date'=>$expiry_date];
                            }
                        }
                        $restaurants_det->Document()->sync($sync_data);
                    }
                    $restaurant_bank_details = $this->restaurant_bank_details->where('restaurant_id',$request->id)->first();
                    if(empty($restaurant_bank_details)) $restaurant_bank_details = $this->restaurant_bank_details;
                    $restaurant_bank_details->account_name = $request->account_name;
                    $restaurant_bank_details->account_address = $request->account_address;
                    $restaurant_bank_details->account_no = $request->account_no;
                    $restaurant_bank_details->bank_name = $request->bank_name;
                    $restaurant_bank_details->branch_name = $request->branch_name;
                    $restaurant_bank_details->branch_address = $request->branch_address;
                    $restaurant_bank_details->swift_code = $request->swift_code;
                    $restaurant_bank_details->routing_no = $request->routing_no;
                    $restaurants_det->RestaurantBankDetails()->save($restaurant_bank_details);
                    $msg = "update_success_msg";

                }else
                {
                    
                    $check_email_phone = $restaurants->where('email',$request->email)->orwhere('phone',$request->phone)->first();
                    if($check_email_phone){
                        return back()->with('error', 'Email/Phone already exists');
                    }
                    $restaurants_det = $restaurants;
                    $restaurants->restaurant_name = $name;
                    $restaurants->image = $image;
                    $restaurants->email = $email;
                    $restaurants->org_password = $request->password;
                    $restaurants->password = Hash::make($request->password);
                    $restaurants->phone = $phone;
                    $restaurants->city = $city;
                    $restaurants->area = $area;
                    $restaurants->discount_type = $discount_type;
                    $restaurants->target_amount = $target_amount;
                    $restaurants->offer_amount = $offer_amount;
                    $restaurants->admin_commision = $admin_commision;
                    $restaurants->restaurant_delivery_charge = $request->restaurant_delivery_charge;
                    $restaurants->driver_commision = $request->driver_commision;
                    //$restaurants->discount = $offer_percentage;
                    $restaurants->shop_description = $shop_description;
                    $restaurants->is_open = 0;
                    $restaurants->estimated_delivery_time = $estimated_delivery_time;
                    $restaurants->packaging_charge = $packaging_charge;
                    $restaurants->address = $address;
                    $restaurants->lat = $request->latitude;
                    $restaurants->lng = $request->longitude;
                    $restaurants->opening_time = $opening_time;
                    $restaurants->closing_time = $closing_time;
                    $restaurants->weekend_opening_time = $weekend_opening_time;
                    $restaurants->weekend_closing_time = $weekend_closing_time;
                    $restaurants->status = $status;
                    $restaurants->delivery_type = json_encode($request->delivery_type);
                    $restaurants->is_vip = $request->is_vip;
                    $restaurants->is_24x7 = $request->is_24x7;
                    //$restaurants->is_home_service = $request->is_home_service;
                    $restaurants->save();
                    
                    $cuisines = $this->cuisines->find($request->cuisines);
                    $restaurants->Cuisines()->attach($cuisines);

                    //$food_quantity = $this->document->find($request->food_quantity);
                    $sync_data=array();
                    if(!empty($request->document)){
                        foreach($request->document as $key=>$value){
                            if($_FILES['document']['name'][$key]['document']!='')
                            {
                                $expiry_date='';
                                if(isset($value['date'])) $expiry_date=date("Y-m-d",strtotime($value['date']));

                                $filename = strtotime(date("Y-m-d")).basename($_FILES['document']['name'][$key]['document']);
                                move_uploaded_file($_FILES["document"]["tmp_name"][$key]['document'], 'public/uploads/Restaurant Document/'.$filename);
                                // $filename = $this->base_image_upload($request,'document','Restaurant Documents');
                                
                                $sync_data[$key] = ['document' => $filename,'expiry_date'=>$expiry_date];

                                // if($expiry_date=='')
                                //     $sync_data[$key] = ['document' => $filename];
                                // else
                                //     $sync_data[$key] = ['document' => $filename,'expiry_date'=>$expiry_date];
                            }
                        }
                        //dd($sync_data);
                        $restaurants->Document()->attach($sync_data);
                    }

                    $this->restaurant_bank_details->account_name = $request->account_name;
                    $this->restaurant_bank_details->account_address = $request->account_address;
                    $this->restaurant_bank_details->account_no = $request->account_no;
                    $this->restaurant_bank_details->bank_name = $request->bank_name;
                    $this->restaurant_bank_details->branch_name = $request->branch_name;
                    $this->restaurant_bank_details->branch_address = $request->branch_address;
                    $this->restaurant_bank_details->swift_code = $request->swift_code;
                    $this->restaurant_bank_details->routing_no = $request->routing_no;
                    $restaurants->RestaurantBankDetails()->save($this->restaurant_bank_details);

                    $msg = "add_success_msg";
                }
                
        }

        return redirect('/admin/store_list')->with('success',trans('constants.'.$msg,['param'=>'Restaurant']));

    }

    public function status_enable(Request $request)
    {
   
    $approve=$this->restaurants->where('id',$request->id)->update(['status'=>1]);

    return back()->with('success','Restaurant Enabled');

    }

        public function status_disable(Request $request)
    {
   
    $approve=$this->restaurants->where('id',$request->id)->update(['status'=>0]);

    return back()->with('success','Restaurant Disabled');

    }


    public function delete_restaurant($id,Request $request)
    {
        $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
            $id = $request->id;
            $restaurants = $this->restaurants;
            $foodlist = $this->foodlist;
            $banner = $this->banner;

            $restaurants->where('id',$id)->delete();
            $foodlist->where('restaurant_id',$id)->delete();
            $banner->where('restaurant_id',$id)->delete();

            return back()->with('success','Restaurant Removed');
        }
    }

    public function restaurant_view($id)
    {
        $restaurant_view = $this->restaurants->with(['city_list','Area'])->find($id);
        $count = $this->review->where('restaurant_id',$restaurant_view->id)->count();
        $rating_sum = $this->review->where('restaurant_id',$restaurant_view->id)->sum('rating');
        $c = $count * 5;
        if($c !=''){
        $rating = (@$rating_sum * 5) / @$c;
    }else{
        $rating=0;
    }
    //     for($x=1;$x<=$starNumber;$x++) {
    //     echo '<img src="path/to/star.png" />';
    // }
    // if (strpos($starNumber,'.')) {
    //     echo '<img src="path/to/half/star.png" />';
    //     $x++;
    // }
    // while ($x<=5) {
    //     echo '<img src="path/to/blank/star.png" />';
    //     $x++;
    // }
        return view('restaurant_view',['restaurant'=>$restaurant_view,'rating'=>round($rating,1)]);
    }

    public function product_list(Request $request)
    {

        if($request->session()->get('role')!=1){
        $restaurant_id = $request->session()->get('userid');

        $data = DB::table('food_list')->where('food_list.restaurant_id',$restaurant_id)
                                      ->join('category','category.id','=','food_list.category_id')
                                      ->join('menu','menu.id','=','food_list.menu_id')
                                      ->select('food_list.id as food_id','food_list.status as food_status','food_list.*','category.*','menu.*')
                                      ->get();

        $total_food = DB::table('food_list')->select('id')->where('food_list.restaurant_id',$restaurant_id)
        ->join('category','category.id','=','food_list.category_id')
                                      ->join('menu','menu.id','=','food_list.menu_id')
                                      ->count();
        $active_total_food = DB::table('food_list')->where(['food_list.status'=>1,'food_list.restaurant_id'=>$restaurant_id])
        ->join('category','category.id','=','food_list.category_id')
                                      ->join('menu','menu.id','=','food_list.menu_id')
                                      ->select('id')->count();
        $inactive_total_food = DB::table('food_list')->where(['food_list.status'=>0,'food_list.restaurant_id'=>$restaurant_id])
        ->join('category','category.id','=','food_list.category_id')
                                      ->join('menu','menu.id','=','food_list.menu_id')
                                      ->select('id')->count();

        }   else{
        $data = DB::table('food_list')->join('category','category.id','=','food_list.category_id')
                                      ->join('menu','menu.id','=','food_list.menu_id')
                                      ->select('food_list.id as food_id','food_list.status as food_status','food_list.*','category.*','menu.*')
                                      ->get();
        $total_food = DB::table('food_list')->join('category','category.id','=','food_list.category_id')
                                      ->join('menu','menu.id','=','food_list.menu_id')
                                      ->select('id')->count();
        $active_total_food = DB::table('food_list')->where('food_list.status',1)
        ->join('category','category.id','=','food_list.category_id')
                                      ->join('menu','menu.id','=','food_list.menu_id')
                                      ->select('id')->count();
        $inactive_total_food = DB::table('food_list')->where('food_list.status',0)
        ->join('category','category.id','=','food_list.category_id')
                                      ->join('menu','menu.id','=','food_list.menu_id')
                                      ->select('id')->count();
        }

        

        return view('product_list',['data'=>$data,'total_food'=>$total_food,'active_total_food'=>$active_total_food,'inactive_total_food'=>$inactive_total_food]);
    }

    public function add_product(Request $request)
    {
        if($request->session()->get('role')==1){
            $restaurant = $this->restaurants->get();
            $menu_list = $this->menu->get();
        }else{
            $restaurant = array();
            $restaurant_id = $request->session()->get('userid');
            $menu_list = $this->menu->where('restaurant_id',$restaurant_id)->get();
        }
        
        $category_list = $this->category->get();
        $add_ons = $this->add_ons->get();
        $food_quantity = $this->food_quantity->get();

        return view('add_product',['category'=>$category_list,'menu'=>$menu_list, 'add_ons'=>$add_ons,'restaurant'=>$restaurant,'food_quantity'=>$food_quantity]);
    }

     public function base_image_upload_product($request,$key)    
    {        
        $imageName = $request->file($key)->getClientOriginalName();       
         $ext = $request->file($key)->getClientOriginalExtension();
         $imageName = self::generate_random_string().'.'.$ext;        
         $request->file($key)->move('public/uploads/product/',$imageName);       
         return $imageName;
    }

    public function add_to_product(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:30',
            'description' => 'required|max:100',
            'category' => 'required',
            'menu' => 'required',
            'price' => 'required',
            'tax' => 'required',
            'packaging_charge' => 'required',
        ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());
            return back()->with('error', $error_messages);
        }else
        {
            if($request->session()->get('role')==1){
                $restaurant_id = $request->restaurant_name;
            }else{
                $restaurant_id = $request->session()->get('userid');
            }
            if(!empty($request->food_quantity_price))
                $food_quantity_price = array_filter($request->food_quantity_price, function($value) { return $value !== ''; });

            
            //dd($request->food_quantity_default);
            $name = $request->name;
            $description = $request->description;
            $category = $request->category;
            $menu = $request->menu;
            if(isset($request->status))
            {
                $status = $request->status;
            }
            else
            {
                $status=0;
            }
            $price = $request->price;
            $tax = $request->tax;
            $packaging_charge = $request->packaging_charge;
            $food_type = (int)$request->food_type;

               
           //print_r($image);exit();            
            if($request->id)
            {
                $foodlist = $this->foodlist->find($request->id);

                if($request->hasFile('image'))
                {
                    $image = self::base_image_upload_product($request,'image');
                    $foodlist->image = $image;
                }

                $foodlist->restaurant_id = $restaurant_id;
                $foodlist->name = $name;
                $foodlist->description = $description;
                $foodlist->category_id = $category;
                $foodlist->menu_id = $menu;
                $foodlist->status = $status;
                $foodlist->price = $price;
                $foodlist->tax = $tax;
                $foodlist->packaging_charge = $packaging_charge;
                $foodlist->is_veg = $food_type;
                $foodlist->status = $status;
                $foodlist->save();

                $add_ons = $this->add_ons->find($request->add_ons);
                //update many to many relationship data
                $foodlist->Add_ons()->sync($add_ons);

                $food_quantity = $this->food_quantity->find($request->food_quantity);
                $sync_data=array();
                for($i = 0; $i < count($food_quantity); $i++){
                    $default=0;
                    //get default based on the id passed from the default key in view
                    if((int)$request->food_quantity_default==$food_quantity[$i]->id) $default=1;
                    
                    if($food_quantity_price[$food_quantity[$i]->id]!=''){
                        $sync_data[$food_quantity[$i]->id] = ['price' => $food_quantity_price[$food_quantity[$i]->id],'is_default'=>$default];
                    }
                }
                $foodlist->FoodQuantity()->sync($sync_data);

                $trans_msg = "update_success_msg";

            }else
            {
                if($request->hasFile('image'))
                {
                    $image = self::base_image_upload_product($request,'image');
                    $this->foodlist->image = $image;
                }
                $this->foodlist->restaurant_id = $restaurant_id;
                $this->foodlist->name = $name;
                $this->foodlist->description = $description;
                $this->foodlist->category_id = $category;
                $this->foodlist->menu_id = $menu;
                $this->foodlist->status = $status;
                $this->foodlist->price = $price;
                $this->foodlist->tax = $tax;
                $this->foodlist->image = $image;
                $this->foodlist->packaging_charge = $packaging_charge;
                $this->foodlist->is_veg = $food_type;
                $this->foodlist->status = $status;
                $this->foodlist->save();

                $add_ons = $this->add_ons->find($request->add_ons);
                $this->foodlist->Add_ons()->attach($add_ons);   

                $food_quantity = $this->food_quantity->find($request->food_quantity);
                $sync_data=array();
                for($i = 0; $i < count($food_quantity); $i++){
                    $default=0;
                    if((int)$request->food_quantity_default==$food_quantity[$i]->id) $default=1;
                    $sync_data[$food_quantity[$i]->id] = ['price' => $food_quantity_price[$food_quantity[$i]->id],'is_default'=>$default];
                }
                //dd($sync_data);
                $this->foodlist->FoodQuantity()->attach($sync_data);
                
                $trans_msg = "add_success_msg";
            }
            return redirect('/admin/product_list')->with('success',trans('constants.'.$trans_msg,[ 'param' => 'Product']));
        }
    }

     public function food_status_enable(Request $request)
    {
   
    $approve=$this->foodlist->where('id',$request->id)->update(['status'=>1]);

    return back()->with('success','Food Enabled');

    }

        public function food_status_disable(Request $request)
    {
   
    $approve=$this->foodlist->where('id',$request->id)->update(['status'=>0]);

    return back()->with('success','Food Disabled');

    }



    /**
     * edit product page with neccessity data
     * 
     * @param array $request, int $id
     * 
     * @return view page with array
     */
    public function edit_product_list(Request $request, $id)
    {
        $product_list = $this->foodlist->with('Add_ons','FoodQuantity')->find($id);
        //dd($product_list);
        $category = $this->category->get();
        if($request->session()->get('role')==1){
            $menu = $this->menu->where('restaurant_id',$product_list->restaurant_id)->get();
            $add_ons = $this->add_ons->where('restaurant_id',$product_list->restaurant_id)->get();
            $restaurant = $this->restaurants->get();
        }else{
            $restaurant = array();
            $menu = $this->menu->where('restaurant_id',$request->session()->get('userid'))->get();
            $add_ons = $this->add_ons->where('restaurant_id',$request->session()->get('userid'))->get();
        }
        $food_quantity = $this->food_quantity->get();
        $addon_ids = $foodquantity_ids = array();
        foreach($product_list->Add_ons as $val){
            $addon_ids[] = $val->id;
        }
        foreach($product_list->FoodQuantity as $val){
            $foodquantity_ids[] = $val->id;
        }

        return view('/edit_product_list',['product_list'=>$product_list,'category'=>$category,'menu'=> $menu, 'add_ons'=>$add_ons,'addon_ids'=>$addon_ids,'restaurant'=>$restaurant,'food_quantity'=>$food_quantity,'foodquantity_ids'=>$foodquantity_ids]);
    }

     public function update_product_list(Request $request)
       {
    
         
          $update =  $this->foodlist->find($request->id);
          $update->name = $request->name;
          $update->description = $request->description;
          $update->category_id = $request->category;
          $update->menu_id = $request->menu;
          $update->status = $request->status;
          $update->price = $request->price;
          $update->tax = $request->tax;
          $update->image = $image;
          $update->packaging_charge = $request->packaging_charge;
          $update->save();

         

          return redirect('/admin/edit_product_list')->with('success','Product Updated Successfully');  

       }

        public function delete_product_list($food_id)
       {
        $delete =  $this->foodlist->where('id',$food_id)->delete();

        return back()->with('success','Product Deleted Successfully');  
       }
       public function delete_city($id)
       {
        $delete =  DB::table('add_city')->where('id',$id)->delete();

        return back()->with('success','City Deleted Successfully');  
       }
       public function delete_country($id)
       {
        $delete =  DB::table('country')->where('id',$id)->delete();

        return back()->with('success','Country Deleted Successfully');  
       }
       public function delete_state($id)
       {
        $delete =  DB::table('state')->where('id',$id)->delete();

        return back()->with('success','State Deleted Successfully');  
       }

       public function delete_vehicle($id)
       {
        $delete =  $this->vehicle->where('id',$id)->delete();

        return back()->with('success','Vehicle Deleted Successfully');  
       }
       public function delete_document($id)
       {
        $delete =  $this->document->where('id',$id)->delete();

        return back()->with('success','Document Deleted Successfully');  
       }
       public function delete_reason($id)
       {
        $delete =  $this->cancellation_reason->where('id',$id)->delete();

        return back()->with('success','Reason Deleted Successfully');  
       }
       

    public function restaurant_menu(Request $request)
    {
         $restaurant_id = $request->session()->get('userid');


        $data = DB::table('menu')->where('menu.restaurant_id',$restaurant_id)->get();

        $total_menu = DB::table('menu')->select('id')->where('menu.restaurant_id',$restaurant_id)->count();
        $active_total_menu = DB::table('menu')->where(['status'=>1,'menu.restaurant_id'=>$restaurant_id])->select('id')->count();
        $inactive_total_menu = DB::table('menu')->where(['status'=>0,'menu.restaurant_id'=>$restaurant_id])->select('id')->count();

        return view('restaurant_menu',['data'=>$data,'total_menu'=>$total_menu,'active_total_menu'=>$active_total_menu,'inactive_total_menu'=>$inactive_total_menu]);
    }
    public function add_restaurant_menu(){
        return view('add_restaurant_menu');
    }
    public function editrestaurant_menu(Request $request){
        $data = DB::table('menu')->where('id',$request->id)->first();

        return view('edit_restaurant_menu',compact('data'));
    }

    public function store_restaurant_menu(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'menu_name' => 'required',
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
             $restaurant_id = $request->session()->get('userid');
            $menu_name = $request->menu_name;

            $restaurant_menu = $this->menu;

            $data = array();

            $data[] = array(
                'restaurant_id'=>$restaurant_id,
                'menu_name'=>$menu_name
            );

            $check = $restaurant_menu->where('restaurant_id',$restaurant_id)->where('menu_name',$menu_name)->count();

            if($check==0)
            {

                $restaurant_menu->insert($data);
            }else
            {
                 return redirect('/admin/restaurant_menu')->with('error','Menu already exist');
            }

        }

        return redirect('/admin/restaurant_menu')->with('success','Menu added Successfully');
} 

       public function edit_restaurant_menu(Request $request)
       {
       
          $update = $this->menu->find($request->id);
          $update->menu_name = $request->menu_name;
          $update->save();
          return redirect('/admin/restaurant_menu')->with('success','Menu Updated Successfully');  

       }

       public function del_restaurant_menu($id)
       {
        $delete =  $this->menu->where('id',$id)->delete();

        return back()->with('success','Menu Deleted Successfully');  
       }


 public function menu_status_enable(Request $request)
    {
   
    $approve=$this->menu->where('id',$request->id)->update(['status'=>1]);

    return back()->with('success','Menu Enabled');

    }

        public function menu_status_disable(Request $request)
    {
   
    $approve=$this->menu->where('id',$request->id)->update(['status'=>0]);

    return back()->with('success','Menu Disabled');

    }

    public function dispatcher(Request $request)
    {

        $restaurant_id = $request->session()->get('userid');
        $pending_orders = DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->where('status',0)
        ->join('users','users.id','=','requests.user_id')
        ->select('users.name as customer_name','users.phone as phone','requests.id as request_id','users.*','requests.*')
        ->get();

        $accepted_orders = DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->where('status',1)
        ->join('users','users.id','=','requests.user_id')
        ->select('users.name as customer_name','users.phone as phone','users.*','requests.*')
        ->get();

        $ongoing_orders = DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->whereIn('status',[2,3,4,5])
        ->join('users','users.id','=','requests.user_id')
        ->select('users.name as customer_name','users.phone as phone','users.*','requests.*')
        ->get();

        $completed_orders = DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->where('status',7)
        ->join('users','users.id','=','requests.user_id')
        ->select('users.name as customer_name','users.phone as phone','users.*','requests.*')
        ->get();

        $data1 = DB::table('requests')->where('requests.restaurant_id',$restaurant_id)
                                     ->join('request_detail','request_detail.request_id','=','requests.id')
                                     ->join('food_list','food_list.id','=','request_detail.food_id')
                                     ->select('food_list.name as food_name','request_detail.*','food_list.*','requests.id as request_id')
                                     ->get();

        return view('/dispatcher',['pending_orders'=>$pending_orders,'accepted_orders'=>$accepted_orders,'ongoing_orders'=>$ongoing_orders,'completed_orders'=>$completed_orders,'data1'=>$data1]);
    } 

    public function restaurant_report(Request $request) 
    {
       $restaurant_id = $request->session()->get('userid');
       if($restaurant_id != Null)
       {

         $restaurant_details = DB::table('requests')
       ->where('requests.restaurant_id',$restaurant_id)
       ->join('users','users.id','=','requests.user_id')
       ->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
       ->join('restaurants','restaurants.id','=','requests.restaurant_id')
       ->select('users.name as customer_name','users.phone as phone','delivery_partners.name as delivery_boy_name','delivery_partners.phone as delivery_boy_phone','restaurants.restaurant_name','requests.id as request_id','users.*','requests.*','delivery_partners.*')
       ->get();

       }else{

         $restaurant_details = DB::table('requests')
       
       ->join('users','users.id','=','requests.user_id')
       ->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
       ->select('users.name as customer_name','users.phone as phone','delivery_partners.name as delivery_boy_name','delivery_partners.phone as delivery_boy_phone','requests.id as request_id','users.*','requests.*','delivery_partners.*')
       ->get();
       }

      

       $month=date('m');
         $prev_month=date('m',strtotime("-1 month"));

        $current_month=DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->whereMonth('requests.created_at', '=', $month)
        ->select('restaurant_commision')->sum('restaurant_commision');
         //print_r($current_month);exit();
         $last_month=DB::table('requests')
         ->where('requests.restaurant_id',$restaurant_id)
         ->whereMonth('requests.created_at', '=', $prev_month)
         ->select('restaurant_commision')->sum('restaurant_commision');

         $current_week=DB::table('requests')
         ->where('requests.restaurant_id',$restaurant_id)
         ->whereBetween('requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
         ->select('restaurant_commision')->sum('restaurant_commision');

         $previous_week = strtotime("-1 week +1 day");

        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);
        $last_week_start=date('Y-m-d',$start_week)." 00:00:00";
        $last_week_end=date('Y-m-d',$end_week)." 23:59:59";

        $last_week=DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->whereBetween('requests.created_at', [$last_week_start, $last_week_end])
        ->select('restaurant_commision')->sum('restaurant_commision');

        $year=date('Y');
        $prev_year1=date('Y',strtotime("-1 month"));
        $prev_year2=date('Y',strtotime("-1 year"));

        $current_year=DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->whereYear('requests.created_at', '=', $year)
        ->select('restaurant_commision')->sum('restaurant_commision');

        $last_year=DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->whereYear('requests.created_at', '=', $prev_year2)
        ->select('restaurant_commision')->sum('restaurant_commision');

        $total_restaurant_report = DB::table('requests')->select('id')->count();
        $total_admin_commision = DB::table('requests')->select('restaurant_commision')->sum('restaurant_commision');
        $total_restaurant_commision=DB::table('requests')->select('admin_commision')->sum('admin_commision');
        return view('/restaurant_report',['restaurant_details'=>$restaurant_details,'current_month'=>$current_month,'last_month'=>$last_month,'current_week'=>$current_week,'last_week'=>$last_week,'current_year'=>$current_year,'last_year'=>$last_year,'total_restaurant_report'=>$total_restaurant_report,'total_admin_commision'=>$total_admin_commision,'total_restaurant_commision'=>$total_restaurant_commision]);
    } 

     public function orderwise_report_pagination(Request $request)
    {        
        $restaurant_id = $request->session()->get('userid');
        $role = $request->session()->get('role');

        $query = $this->foodrequest->query();
        $query = $query->when(($role!=1), 
                    function($q) use($restaurant_id){
                        return $q->where('restaurant_id',$restaurant_id);
                    });

        $limit = $request->iDisplayLength;
        $offset = $request->iDisplayStart;
        //check limit and offset
        $query = $query->when(($limit!='-1' && isset($offset)), 
                            function($q) use($limit, $offset){
                                return $q->offset($offset)->limit($limit);
                            });

        
        $orderwise_details = $query->get();

        $query1 =$this->foodrequest->query();
        $query1 = $query1->when(($role!=1), 
                    function($q) use($restaurant_id){
                        return $q->where('restaurant_id',$restaurant_id);
                    });
        $total_orders = $query1->get();

        $column=array();
        $data=array();
        foreach ($orderwise_details as $key => $value) {
            switch ((int) $value->status) {
              case 0:
                $status = 'New Order';
              break;
              case 1:
                $status = 'Order Accepted';
              break;
              case 2:
                $status = 'Delivery boy assigned';
              break;
              case 3:
                $status = 'Food delivered to Delivery boy';
              break;
              case 4:
                $status = 'Towards Customer';
              break;
              case 5:
                $status = 'Reached Customer';
              break;
              case 6:
                $status = 'Delivered to Customer';
              break;
              case 7:
                $status = 'Completed';
              break;
              
              default:
                $status = ' Cancelled';
                break;
            }
            
            $col['id']=$value->id;
            $col['order_id']=$value->order_id;
            $col['customer_name']=isset($value->Users)?$value->Users->name:"";
            $col['customer_phone']=isset($value->Users)?$value->Users->phone:"";
            $col['delivery_boy_name']=isset($value->Deliverypartners)?$value->Deliverypartners->name:"";
            $col['delivery_boy_phone']=isset($value->Deliverypartners)?$value->Deliverypartners->phone:"";
            $col['restaurant_name']=isset($value->Restaurants)?$value->Restaurants->restaurant_name:"";
            $col['item_total']=$value->item_total;
            $col['tax']=DEFAULT_CURRENCY_SYMBOL.$value->tax;
            $col['offer_discount']=DEFAULT_CURRENCY_SYMBOL.$value->offer_discount;
            $col['admin_commision'] = DEFAULT_CURRENCY_SYMBOL.$value->admin_commision;
            $col['delivery_boy_commision'] =DEFAULT_CURRENCY_SYMBOL.$value->delivery_boy_commision;
            $col['restaurant_commision']=DEFAULT_CURRENCY_SYMBOL.$value->restaurant_commision;
            $col['status']=$status;

            array_push($column, $col);
        }
        $orderwise_details['sEcho']=$request->sEcho;
        $orderwise_details['aaData']=$column;
        $orderwise_details['iTotalRecords']=count($total_orders);
        $orderwise_details['iTotalDisplayRecords']=count($total_orders);

        return json_encode($orderwise_details);

    }

     public function restaurant_report_filter(Request $request) 
    {

         $start = $request->start .' 00:00:00';
         $end =   $request->end  .' 23:59:59';
       $restaurant_id = $request->session()->get('userid');
       if($restaurant_id != Null)
       {

         $restaurant_details = DB::table('requests')
       ->where('requests.restaurant_id',$restaurant_id)
       ->join('users','users.id','=','requests.user_id')
       ->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
       ->select('users.name as customer_name','users.phone as phone','delivery_partners.name as delivery_boy_name','requests.id as request_id','users.*','requests.*','delivery_partners.*')
       ->whereBetween('requests.created_at',[$start,$end])
       ->get();

       }else{

         $restaurant_details = DB::table('requests')
       
       ->join('users','users.id','=','requests.user_id')
       ->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
       ->select('users.name as customer_name','users.phone as phone','delivery_partners.name as delivery_boy_name','requests.id as request_id','users.*','requests.*','delivery_partners.*')
       ->whereBetween('requests.created_at',[$start,$end])
       ->get();
       }

      

       $month=date('m');
         $prev_month=date('m',strtotime("-1 month"));

        $current_month=DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->whereMonth('requests.created_at', '=', $month)
        ->select('restaurant_commision')->sum('restaurant_commision');
         //print_r($current_month);exit();
         $last_month=DB::table('requests')
         ->where('requests.restaurant_id',$restaurant_id)
         ->whereMonth('requests.created_at', '=', $prev_month)
         ->select('restaurant_commision')->sum('restaurant_commision');

         $current_week=DB::table('requests')
         ->where('requests.restaurant_id',$restaurant_id)
         ->whereBetween('requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
         ->select('restaurant_commision')->sum('restaurant_commision');

         $previous_week = strtotime("-1 week +1 day");

        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);
        $last_week_start=date('Y-m-d',$start_week)." 00:00:00";
        $last_week_end=date('Y-m-d',$end_week)." 23:59:59";

        $last_week=DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->whereBetween('requests.created_at', [$last_week_start, $last_week_end])
        ->select('restaurant_commision')->sum('restaurant_commision');

        $year=date('Y');
        $prev_year1=date('Y',strtotime("-1 month"));
        $prev_year2=date('Y',strtotime("-1 year"));

        $current_year=DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->whereYear('requests.created_at', '=', $year)
        ->select('restaurant_commision')->sum('restaurant_commision');

        $last_year=DB::table('requests')
        ->where('requests.restaurant_id',$restaurant_id)
        ->whereYear('requests.created_at', '=', $prev_year2)
        ->select('restaurant_commision')->sum('restaurant_commision');

        return view('/restaurant_report',['restaurant_details'=>$restaurant_details,'current_month'=>$current_month,'last_month'=>$last_month,'current_week'=>$current_week,'last_week'=>$last_week,'current_year'=>$current_year,'last_year'=>$last_year]);
    } 
     

    public function admin_restaurant_report(Request $request) 
    {
       
        
        $restaurant_details = $this->restaurants
                               ->join('add_city','add_city.id','=','restaurants.city')
                               ->join('add_area','add_area.id','=','restaurants.area')
                               ->select('restaurants.*','add_city.city as city','add_area.area as area')
                              ->get();
       
       // ->join('restaurants','restaurants.id','=','requests.restaurant_id')
       
       // ->select('requests.*','restaurants.*','restaurants.restaurant_name as restaurant_name','restaurants.id as res_id')

      
       // ->get();

      //  $restaurant_details = DB::table('restaurants')->select('restaurant_name as restaurant_name','id')->get();
       

       foreach ($restaurant_details as $key => $value) {
        $value->restaurant_commision = DB::table('requests')->where('restaurant_id',$value->id)->sum('restaurant_commision');
        $value->admin_commision = DB::table('requests')->where('restaurant_id',$value->id)->sum('admin_commision');

        //print_r($value->delivery_boy);exit();
            
      }

        // $tempArray = array();
        // $result = array();
        // foreach ($restaurant_details as $key => $value) {
        //     if (!in_array($value->restaurant_id, $tempArray))
        //     {
        //       array_push($tempArray,$value->restaurant_id);
        //       array_push($result,$value);
        //     }
        // }
        // $restaurant_details = $result;

       $month=date('m');
         $prev_month=date('m',strtotime("-1 month"));

        $current_month=DB::table('requests')
        
        ->whereMonth('requests.created_at', '=', $month)
        ->select('admin_commision')->sum('admin_commision');
         //print_r($current_month);exit();
         $last_month=DB::table('requests')
         
         ->whereMonth('requests.created_at', '=', $prev_month)
         ->select('admin_commision')->sum('admin_commision');

         $current_week=DB::table('requests')
         
         ->whereBetween('requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
         ->select('admin_commision')->sum('admin_commision');

         $previous_week = strtotime("-1 week +1 day");

        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);
        $last_week_start=date('Y-m-d',$start_week)." 00:00:00";
        $last_week_end=date('Y-m-d',$end_week)." 23:59:59";

        $last_week=DB::table('requests')
        
        ->whereBetween('requests.created_at', [$last_week_start, $last_week_end])
        ->select('admin_commision')->sum('admin_commision');

        $year=date('Y');
        $prev_year1=date('Y',strtotime("-1 month"));
        $prev_year2=date('Y',strtotime("-1 year"));

        $current_year=DB::table('requests')
        
        ->whereYear('requests.created_at', '=', $year)
        ->select('admin_commision')->sum('admin_commision');

        $last_year=DB::table('requests')
        
        ->whereYear('requests.created_at', '=', $prev_year2)
        ->select('admin_commision')->sum('admin_commision');
        $total_order = DB::table('requests')->select('id')->count();
        $total_restaurant = DB::table('restaurants')->select('id')->count();
        $total_restaurant_comission = DB::table('requests')->sum('restaurant_commision');
        
        return view('/admin_restaurant_report',['restaurant_details'=>$restaurant_details,'current_month'=>$current_month,'last_month'=>$last_month,'current_week'=>$current_week,'last_week'=>$last_week,'current_year'=>$current_year,'last_year'=>$last_year,'total_order'=>$total_order,'total_restaurant_comission'=>$total_restaurant_comission,'total_restaurant'=>$total_restaurant]);
    } 

    public function admin_report_view($res_id)
    {
       $admin_view = DB::table('restaurants')->where('id',$res_id)->get();
      
        $restaurant_total_earnings=DB::table('requests')
        ->where('restaurant_id',$res_id)
        ->where('status','=',7)
        ->select('restaurant_commision')->sum('restaurant_commision');
        
         //print_r($vendor_total_earnings);exit();
        $restaurant_pending_payouts=DB::table('requests')
         ->where('restaurant_id',$res_id)
        ->whereIn('status',[0,1,2,3,4,5])
        ->select('restaurant_commision')->sum('restaurant_commision');

        $restaurant_admin_earnings=DB::table('requests')
         ->where('restaurant_id',$res_id)
        ->select('admin_commision')->sum('admin_commision');
        

        $data = DB::table('requests')->join('users','users.id','=','requests.user_id')->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
                                     ->select('requests.id as request_id','requests.status as order_status','users.name as user_name','delivery_partners.name as delivery_boy_name','requests.*','users.*')->where('requests.restaurant_id',$res_id)->where('requests.status',7)
                                     ->orderBy('request_id','desc')
                                     ->get();

        $data1 = DB::table('requests')->join('request_detail','request_detail.request_id','=','requests.id')->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
                                     ->join('food_list','food_list.id','=','request_detail.food_id')
                                     ->select('food_list.name as food_name','delivery_partners.name as delivery_boy_name','request_detail.*','food_list.*','requests.id')->where('requests.restaurant_id',$res_id)->where('requests.status',7)
                                     ->get();

        
        return view('/admin_report_view',['admin_view'=>$admin_view,'restaurant_total_earnings'=>$restaurant_total_earnings,'restaurant_pending_payouts'=>$restaurant_pending_payouts,'restaurant_admin_earnings'=>$restaurant_admin_earnings,'data'=>$data,'data1'=>$data1]);
    }
    
    public function delivery_boy_reports($res_id){
        $admin_view = DB::table('restaurants')->where('id',$res_id)->get();
      
        $restaurant_total_earnings=DB::table('requests')
        ->where('restaurant_id',$res_id)
        ->where('status','=',7)
        ->select('restaurant_commision')->sum('restaurant_commision');
        
         //print_r($vendor_total_earnings);exit();
        $restaurant_pending_payouts=DB::table('requests')
         ->where('restaurant_id',$res_id)
        ->whereIn('status',[0,1,2,3,4,5])
        ->select('restaurant_commision')->sum('restaurant_commision');

        $restaurant_admin_earnings=DB::table('requests')
         ->where('restaurant_id',$res_id)
        ->select('admin_commision')->sum('admin_commision');
        

        $data = DB::table('requests')->join('users','users.id','=','requests.user_id')->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
                                     ->select('requests.id as request_id','requests.status as order_status','users.name as user_name','delivery_partners.name as delivery_boy_name','requests.*','users.*')->where('requests.restaurant_id',$res_id)->where('requests.status',7)
                                     ->orderBy('request_id','desc')
                                     ->get();

        $data1 = DB::table('requests')->join('request_detail','request_detail.request_id','=','requests.id')->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
                                     ->join('food_list','food_list.id','=','request_detail.food_id')
                                     ->select('food_list.name as food_name','delivery_partners.name as delivery_boy_name','request_detail.*','food_list.*','requests.id')->where('requests.restaurant_id',$res_id)->where('requests.status',7)
                                     ->get();

        
        return view('/delivery_boy_reports_view',['admin_view'=>$admin_view,'restaurant_total_earnings'=>$restaurant_total_earnings,'restaurant_pending_payouts'=>$restaurant_pending_payouts,'restaurant_admin_earnings'=>$restaurant_admin_earnings,'data'=>$data,'data1'=>$data1]);
    }

    public function delivery_boy_report(Request $request)
    {

         $delivery_boy_details =DB::table('delivery_partners')
                               ->leftjoin('delivery_partner_details','delivery_partner_details.delivery_partners_id','=','delivery_partners.id')
                               ->leftjoin('add_city','add_city.id','=','delivery_partner_details.city')
                              
                                ->leftjoin('vehicle','vehicle.id','=','delivery_partner_details.vehicle_name')
                               ->select('delivery_partners.*','vehicle.*','add_city.city as city','vehicle.vehicle_name as vehicle_name','delivery_partner_details.*')
                              ->get();

                              //
                              //print_r($delivery_boy_details);exit();
        foreach ($delivery_boy_details as $key => $value) {

           $value->total_order = DB::table('requests')->where('user_id',$value->id)->select('id')->count();
        

        //print_r($value->delivery_boy);exit();
            
      }

        // $delivery_boy_details = DB::table('delivery_partners')->select('name as delivery_boy_name','id')->get();
       

      //  foreach ($delivery_boy_details as $key => $value) {
      //   $value->delivery_boy_commision = DB::table('requests')->where('delivery_boy_id',$value->id)->sum('delivery_boy_commision');
      //   $value->admin_commision = DB::table('requests')->where('delivery_boy_id',$value->id)->sum('admin_commision');

      //   //print_r($value->delivery_boy);exit();
            
      // }

      

      

       $month=date('m');
         $prev_month=date('m',strtotime("-1 month"));

        $current_month=DB::table('requests')
        
        ->whereMonth('requests.created_at', '=', $month)
        ->select('delivery_boy_commision')->sum('delivery_boy_commision');
         //print_r($current_month);exit();
         $last_month=DB::table('requests')
        
         ->whereMonth('requests.created_at', '=', $prev_month)
         ->select('delivery_boy_commision')->sum('delivery_boy_commision');

         $current_week=DB::table('requests')
         
         ->whereBetween('requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
         ->select('delivery_boy_commision')->sum('delivery_boy_commision');

         $previous_week = strtotime("-1 week +1 day");

        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);
        $last_week_start=date('Y-m-d',$start_week)." 00:00:00";
        $last_week_end=date('Y-m-d',$end_week)." 23:59:59";

        $last_week=DB::table('requests')
       
        ->whereBetween('requests.created_at', [$last_week_start, $last_week_end])
        ->select('delivery_boy_commision')->sum('delivery_boy_commision');

        $year=date('Y');
        $prev_year1=date('Y',strtotime("-1 month"));
        $prev_year2=date('Y',strtotime("-1 year"));

        $current_year=DB::table('requests')
        
        ->whereYear('requests.created_at', '=', $year)
        ->select('delivery_boy_commision')->sum('delivery_boy_commision');

        $last_year=DB::table('requests')
        
        ->whereYear('requests.created_at', '=', $prev_year2)
        ->select('delivery_boy_commision')->sum('delivery_boy_commision');
        
        
        $total_delivery_boy_comission = DB::table('requests')->sum('delivery_boy_commision');
        $total_order = DB::table('requests')->select('id')->count();
        return view('/delivery_boy_reports',['delivery_boy_details'=>$delivery_boy_details,'current_month'=>$current_month,'last_month'=>$last_month,'current_week'=>$current_week,'last_week'=>$last_week,'current_year'=>$current_year,'last_year'=>$last_year,'total_delivery_boy_comission'=>$total_delivery_boy_comission,'total_order'=>$total_order]);
    }

     public function deliveryboy_report_pagination(Request $request)
    {        
        //   $restaurant_details = $this->restaurants
        //                        ->join('add_city','add_city.id','=','restaurants.city')
        //                        ->join('add_area','add_area.id','=','restaurants.area')
        //                        ->select('restaurants.*','add_city.city as city','add_area.area as area')
        //                       ->get();

        $query = $this->deliverypartners->query();
        $limit = $request->iDisplayLength;
        $offset = $request->iDisplayStart;
        //print_r($offset);exit();
        //check limit and offset
        $query = $query->when(($limit!='-1' && isset($offset)), 
                            function($q) use($limit, $offset){
                                return $q->offset($offset)->limit($limit);
                            });

        
        $deliveryboy_report_details = $query->get();

        $total_deliveryboys =$this->deliverypartners->get();

        $column=array();
        $data=array();
        foreach ($deliveryboy_report_details as $key => $value) {

            $res_id = $value->id;
            $total_orders = $this->foodrequest->where('delivery_boy_id', $res_id)->count();
            $ratings = $this->order_ratings->with('Foodrequest')
                                ->wherehas('Foodrequest',function($q) use($res_id){
                                    $q->where('delivery_boy_id', $res_id);
                                    })
                                ->avg('restaurant_rating');
            $payout_done = $this->driver_payout_history->where('delivery_boy_id', $value->id)->sum('payout_amount');

            $col['id']=$value->id;
            $col['name']=$value->name;
            $col['email']=$value->email;
            $col['phone']=$value->phone;
            //if(isset($value->Deliverypartner_detail->Citylist)) print_r($value->Deliverypartner_detail->Citylist);
            $col['city']=isset($value->Deliverypartner_detail->Citylist)?$value->Deliverypartner_detail->Citylist->city:"";
            $col['vehicle_name']=isset($value->Deliverypartner_detail->Vehicle)?$value->Deliverypartner_detail->Vehicle->vehicle_name:"";
            $col['address_line_1'] = isset($value->Deliverypartner_detail->address_line_1)?$value->Deliverypartner_detail->address_line_1:"";
            $col['ratings'] = $ratings;
           $col['total_orders']=$total_orders;
           $col['total_earnings'] = DEFAULT_CURRENCY_SYMBOL.round($value->total_earnings,2);
           $col['pending_payout']=DEFAULT_CURRENCY_SYMBOL.$value->pending_payout;
           $col['payout_done']=DEFAULT_CURRENCY_SYMBOL.$value->payout_done;

            array_push($column, $col);
        }
        $deliveryboy_report_details['sEcho']=$request->sEcho;
        $deliveryboy_report_details['aaData']=$column;
        $deliveryboy_report_details['iTotalRecords']=count($total_deliveryboys);
        $deliveryboy_report_details['iTotalDisplayRecords']=count($total_deliveryboys);

        return json_encode($deliveryboy_report_details);

    }

     public function delivery_boy_report_filter(Request $request)
    {
        $start = $request->start .' 00:00:00';
         $end =   $request->end  .' 23:59:59';

         $delivery_boy_details =DB::table('delivery_partners')
                               ->join('delivery_partner_details','delivery_partner_details.delivery_partners_id','=','delivery_partners.id')
                               ->join('add_city','add_city.id','=','delivery_partner_details.city')
                              
                                ->join('vehicle','vehicle.id','=','delivery_partner_details.vehicle_name')
                               ->select('delivery_partners.*','vehicle.*','add_city.city as city','vehicle.vehicle_name as vehicle_name','delivery_partner_details.*')
                               ->whereBetween('delivery_partners.created_at',[$start,$end])
                              ->get();

                              //
                              //print_r($delivery_boy_details);exit();
       

        // $delivery_boy_details = DB::table('delivery_partners')->select('name as delivery_boy_name','id')->get();
       

      //  foreach ($delivery_boy_details as $key => $value) {
      //   $value->delivery_boy_commision = DB::table('requests')->where('delivery_boy_id',$value->id)->sum('delivery_boy_commision');
      //   $value->admin_commision = DB::table('requests')->where('delivery_boy_id',$value->id)->sum('admin_commision');

      //   //print_r($value->delivery_boy);exit();
            
      // }

      

      

       $month=date('m');
         $prev_month=date('m',strtotime("-1 month"));

        $current_month=DB::table('requests')
        
        ->whereMonth('requests.created_at', '=', $month)
        ->select('delivery_boy_commision')->sum('delivery_boy_commision');
         //print_r($current_month);exit();
         $last_month=DB::table('requests')
        
         ->whereMonth('requests.created_at', '=', $prev_month)
         ->select('delivery_boy_commision')->sum('delivery_boy_commision');

         $current_week=DB::table('requests')
         
         ->whereBetween('requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
         ->select('delivery_boy_commision')->sum('delivery_boy_commision');

         $previous_week = strtotime("-1 week +1 day");

        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);
        $last_week_start=date('Y-m-d',$start_week)." 00:00:00";
        $last_week_end=date('Y-m-d',$end_week)." 23:59:59";

        $last_week=DB::table('requests')
       
        ->whereBetween('requests.created_at', [$last_week_start, $last_week_end])
        ->select('delivery_boy_commision')->sum('delivery_boy_commision');

        $year=date('Y');
        $prev_year1=date('Y',strtotime("-1 month"));
        $prev_year2=date('Y',strtotime("-1 year"));

        $current_year=DB::table('requests')
        
        ->whereYear('requests.created_at', '=', $year)
        ->select('delivery_boy_commision')->sum('delivery_boy_commision');

        $last_year=DB::table('requests')
        
        ->whereYear('requests.created_at', '=', $prev_year2)
        ->select('delivery_boy_commision')->sum('delivery_boy_commision');

        return view('/delivery_boy_reports',['delivery_boy_details'=>$delivery_boy_details,'current_month'=>$current_month,'last_month'=>$last_month,'current_week'=>$current_week,'last_week'=>$last_week,'current_year'=>$current_year,'last_year'=>$last_year]);
    }

    public function restaurant_report_pagination(Request $request)
    {        
        //   $restaurant_details = $this->restaurants
        //                        ->join('add_city','add_city.id','=','restaurants.city')
        //                        ->join('add_area','add_area.id','=','restaurants.area')
        //                        ->select('restaurants.*','add_city.city as city','add_area.area as area')
        //                       ->get();
        //dd($request->all());

        $query = $this->restaurants->with(['city_list','Area']);
        $limit = $request->iDisplayLength;
        $offset = $request->iDisplayStart;
        //check limit and offset
        $query = $query->when(($limit!='-1' && isset($offset)), 
                            function($q) use($limit, $offset){
                                return $q->offset($offset)->limit($limit);
                            });

        
        $restaurant_details = $query->get();

        $total_restaurant = $this->restaurants->get();

         foreach ($restaurant_details as $key => $value) {
        $value->restaurant_commision = DB::table('requests')->where('restaurant_id',$value->id)->sum('restaurant_commision');
        $value->admin_commision = DB::table('requests')->where('restaurant_id',$value->id)->sum('admin_commision');
    }

        $column=array();
        $data=array();
        foreach ($restaurant_details as $key => $value) {
            $res_id = $value->id;
            $total_orders = $this->foodrequest->where('restaurant_id', $res_id)->count();
            $ratings = $this->order_ratings->with('Foodrequest')
                                ->wherehas('Foodrequest',function($q) use($res_id){
                                    $q->where('restaurant_id', $res_id);
                                    })
                                ->avg('restaurant_rating');
            $payout_done = $this->restaurant_payout_history->where('restaurant_id', $value->id)->sum('payout_amount');
            $col['id']=$value->id;
            $col['restaurant_name']=$value->restaurant_name;
            $col['email']=$value->email;
            $col['phone']=$value->phone;
            $col['rating']=$ratings;
            $col['address']=$value->address;
            $col['city'] = isset($value->city_list)?$value->city_list->city:"";
            $col['area'] = isset($value->Area)?$value->Area->area:"";
            $col['total_orders']=$total_orders;
            $col['restaurant_commision']= DEFAULT_CURRENCY_SYMBOL.round($value->restaurant_commision,2);
            $col['pending_payout']=DEFAULT_CURRENCY_SYMBOL.$value->pending_payout;
            $col['payout_done']=DEFAULT_CURRENCY_SYMBOL.$payout_done;

            array_push($column, $col);
        }
        $restaurant_details['sEcho']=$request->sEcho;
        $restaurant_details['aaData']=$column;
        $restaurant_details['iTotalRecords']=count($total_restaurant);
        $restaurant_details['iTotalDisplayRecords']=count($total_restaurant);

        return json_encode($restaurant_details);

    }



    public function city_management(Request $request)
    {
        $country = $this->country->get();
      
      return view('/add_city',compact('country'));
    }

   

    public function add_city(Request $request)
    {
        $validator = Validator::make($request->all(), [
                
                'city' => 'required',
                'admin_commision' => 'required',
                'country' => 'required',
                'state' => 'required',
                'status' => 'required',
                'geofence_latlng' => 'required'
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());
            //print_r($validator->messages()); exit;
            //return back()->with('error', $error_messages);
            return back()->withErrors($validator)->withInput();

        }else
        {
           
            /*if(!empty($request->geofence_latlng))
            {
                $source_polygon = explode(',',$request->geofence_latlng);
                $j = $k =0;
                for ($i=0; $i <= count($source_polygon)-1 && $k <= count($source_polygon)-1; $i++) {
                        $source_coordinates[$j][0] = $source_polygon[$k++];
                        $source_coordinates[$j][0] = (double)trim($source_coordinates[$j][0],'[ ');
                        $source_coordinates[$j][1] = $source_polygon[$k];
                        $source_coordinates[$j][1] = (double)trim($source_coordinates[$j][1],' ]');

                        $temp = $source_coordinates[$j][0];
                        $source_coordinates[$j][0] = $source_coordinates[$j][1];
                        $source_coordinates[$j][1] = $temp;
                        $k++;$j++;
                }
            } else {
                $source_coordinates = array();
            }*/
            //echo "<pre>";print_r( $source_coordinates);
            // $first_cordinate = current($source_coordinates);
            // $a = end($source_coordinates);
            // $key = key($source_coordinates); 
            // $source_coordinates[$key+1] = $first_cordinate;
            //echo "<pre>";print_r( $source_coordinates);exit;
        $check = DB::table('add_city')->where('city','=',$request->city)->first();
        if(isset($check)==0)
        {
            $city = $request->city;
            $country_id = $request->country;
            $state_id = $request->state;
            $admin_commision = $request->admin_commision;
            $default_delivery_amount = $request->default_delivery_amount;
            $target_amount = $request->target_amount;
            $driver_base_price = $request->driver_base_price;
            $min_dist_base_price = $request->min_dist_base_price;
            $extra_fee_amount = $request->extra_fee_amount;
            $extra_fee_amount_each = $request->extra_fee_amount_each;
            $night_fare_amount = $request->night_fare_amount;
            $night_driver_share = $request->night_driver_share;
            $surge_fare_amount = $request->surge_fare_amount;
            $surge_driver_share = $request->surge_driver_share;
            $status = $request->status;
            $add_city = $this->addcity;

            $data = array();

            // $data[] = array(
                
            //  'city'=>$city,
            //  'admin_commision'=>$admin_commision,
            //  'default_delivery_amount'=>$default_delivery_amount,
            //  'target_amount'=>$target_amount,
            //  'driver_base_price'=>$driver_base_price,
            //  'min_dist_base_price'=>$min_dist_base_price,
            //  'extra_fee_amount'=>$extra_fee_amount,
            //  'extra_fee_amount_each'=>$extra_fee_amount_each,
            //  'night_fare_amount'=>$night_fare_amount,
            //  'night_driver_share'=>$night_driver_share,
            //  'surge_fare_amount'=>$surge_fare_amount,
            //  'surge_driver_share'=>$surge_driver_share,
            //  'status'=>$status,

            // );
            $add_city->city = $city;
            $add_city->country_id = $country_id;
            $add_city->state_id = $state_id;
            $add_city->admin_commision = $admin_commision;
            $add_city->default_delivery_amount = $default_delivery_amount;
            $add_city->target_amount = $target_amount;
            $add_city->driver_base_price = $driver_base_price;
            $add_city->min_dist_base_price = $min_dist_base_price;
            $add_city->extra_fee_amount = $extra_fee_amount;
            $add_city->extra_fee_amount_each = $extra_fee_amount_each;
            $add_city->night_fare_amount = $night_fare_amount;
            $add_city->night_driver_share = $night_driver_share;
            $add_city->surge_fare_amount = $surge_fare_amount;
            $add_city->surge_driver_share = $surge_driver_share;
            $add_city->status = $status;
            $city_data = $add_city->save();
            
            //add data in city_geofencing table
            $geofencing = new City_geofencing();
            $geofencing->city_id = $add_city->id;
            $geofencing->latitude = $request->latitude;
            $geofencing->longitude = $request->longitude;
            $geofencing->polygons = $request->geofence_latlng;
            //$geofencing->save();
            $geofencing->save();
            //$data = $city_id->city_geofencing()->create($geofencing);
        }
         else
        {

            return back()->with('error','This City was Already Registered!');
          }
        }

        return redirect('/admin/city_list')->with('success','City  Added Successfully');
} 

    public function city_list()
    {
      $city_list=DB::table('add_city')->get();
      $total_city = DB::table('add_city')->select('id')->count();
      $active_total_city = DB::table('add_city')->where('status',1)->select('id')->count();
      $inactive_total_city = DB::table('add_city')->where('status',0)->select('id')->count();
      //$city_list = $this->addcity->with('Restaurants')->get();
      //dd($city_list);
      //echo "<pre>"; print_r($city_list);exit();
      
      return view('/city_list',['city_list'=>$city_list,'total_city'=>$total_city,'active_total_city'=>$active_total_city,'inactive_total_city'=>$inactive_total_city]);
    }


    /**
    * add area view page
    *
    * @param int $id
    *
    * @return view page with array $area, int $id
    */
    public function area_setting($id)
    {
        $area=DB::table('add_city')->where('id',$id)->first();
        //print_r($area);exit();
        return view('/add_areas',['area'=>$area,'city_id'=>$id]);
    }


    /**
    * edit area view page
    *
    * @param int $id
    *
    * @return view page with array $area
    */
    public function edit_area($id)
    {
        $area = $this->addarea->find($id);
        //print_r($area);exit();
        return view('/edit_areas',['area'=>$area]);
    }


    /**
    * store area and return to view page
    *
    * @param object $request
    *
    * @return view page
    */
    public function add_area(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'area' => 'required',
                'status' => 'required',
                'geofence_latlng' => 'required'
            ]);

        if($validator->fails()) 
        {

            $error_messages = implode(',',$validator->messages()->all());
            return back()->with('error', $error_messages)->withInput();
        }else
        {
            $check = $this->addarea->where('area','=',$request->area)->first();
        if(isset($check)==0)
        {
            $add_city_id = $request->id;
            $area = $request->area;
            $status = $request->status;
            $add_area = $this->addarea;
            $data = array();

            $data[] = array(
                
                'area'=>$area,
                'status'=>$status,
                'add_city_id'=>$add_city_id,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'polygons' => $request->geofence_latlng,

            );
            $add_area->insert($data);
             }
         else
        {

            return back()->with('error','This Area was Already Registered!');
          }
            
        }
        return redirect('admin/view_areas/'.$request->id)->with('success',trans('constants.add_success_msg',['param'=>'Area']));
    } 
    
    /**
    * get area list and return to view page
    *
    * @param int $id
    *
    * @return view page with array $area_list, int $id
    */
    public function area_list($id)
    {
        $area_list=DB::table('add_area')->where('add_city_id',$id)->get();
        return view('/view_areas',['area_list'=>$area_list,'city_id'=>$id]);
    }


    /**
    * update area list
    *
    * @param object $request
    *
    * @return view page
    */
    public function update_area_list(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'area' => 'required',
                'status' => 'required',
                'geofence_latlng' => 'required'
            ]);

        if($validator->fails()) 
        {

            $error_messages = implode(',',$validator->messages()->all());
            return back()->with('error', $error_messages)->withInput();
        }else
        {
            $area =  $this->addarea->find($request->id);
            $area->area = $request->area;
            $area->status = $request->status;
            $area->latitude = $request->latitude;
            $area->longitude = $request->longitude;
            $area->polygons = $request->geofence_latlng;
            $area->save();
            return redirect('/admin/view_areas/'.$area->add_city_id)->with('success',trans('constants.update_success_msg',['param'=>'Area']));  
        }
    }


    public function delete_area_list($id)
       {
        $delete =  $this->addarea->where('id',$id)->delete();

        return back()->with('success','Area Deleted Successfully');  
       }

   public function document_management() 
   {

    return view('add_document');
   }   
   

   public function document_add(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'document_for' => 'required',
                'document_name' => 'required',
                'expiry_date_needed' => 'required',
                'status' => 'required',
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
            $document_for = $request->document_for;
            $document_name = $request->document_name;
            $expiry_date_needed = $request->expiry_date_needed;
            
            $status = $request->status;
            $add_document = $this->document;

            $data = array();

            $data[] = array(
                'document_for'=>$document_for,
                'document_name'=>$document_name,
                'expiry_date_needed'=>$expiry_date_needed,
                'status'=>$status,
                

            );

            

        
                $add_document->insert($data);
            
            

        }

        return back()->with('success','Document Added Successfully');
} 

    public function document_list()
    {
        $document_list = $this->document->get();
        $total_document = $this->document->select('id')->count();
        $active_total_document = $this->document->where('status',1)->select('id')->count();
        $inactive_total_document = $this->document->where('status',0)->select('id')->count();
        return view('document_list',['document_list'=>$document_list,'total_document'=>$total_document,'active_total_document'=>$active_total_document,'inactive_total_document'=>$inactive_total_document]);
    }

     public function vehicle_management() 
   {

    return view('add_vehicle');
   }   
  
  public function vehicle_add(Request $request)
    {


        $rules = array();

        if(!$request->id){
            $rules['insurance_image'] = 'required|max:2048';
            $rules['rc_image'] = 'required|max:2048';
        }
            $rules['vehicle_name'] = 'required';
            $rules['vehicle_no'] = 'required';
            $rules['insurance_no'] = 'required';
            $rules['insurance_expiry_date'] = 'required';
            $rules['rc_no'] = 'required';
            $rules['rc_expiry_date'] = 'required';
            $rules['status'] = 'required';

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
            $vehicle_name = $request->vehicle_name;
            $vehicle_no = $request->vehicle_no;
            $insurance_no = $request->insurance_no;
            $insurance_expiry_date = date("Y-m-d",strtotime($request->insurance_expiry_date));
            $rc_no = $request->rc_no;
            $rc_expiry_date = date("Y-m-d",strtotime($request->rc_expiry_date));
            $path = 'public/vehicles/';
           

            $status = $request->status;
            if($request->id != ""){             
                $add_vehicle = $this->vehicle->find($request->id);           
            }else{ 
                $add_vehicle = new $this->vehicle; 
            }
             $add_vehicle->vehicle_name = $vehicle_name;
             $add_vehicle->vehicle_no = $vehicle_no;
             $add_vehicle->insurance_no = $insurance_no;
             $add_vehicle->insurance_expiry_date = $insurance_expiry_date;
             $add_vehicle->rc_no = $rc_no;
             $add_vehicle->rc_expiry_date = $rc_expiry_date;
             $add_vehicle->status = $status;
            if($request->insurance_image !="" || $request->insurance_image != null){
                $add_vehicle->insurance_image = $this->custom->common_upload_images($request,'insurance_image',$path);
            }
            if($request->rc_image !="" || $request->rc_image != null){
                $add_vehicle->rc_image = $this->custom->common_upload_images($request,'rc_image',$path);
            }
            if($request->vehicle_image !="" || $request->vehicle_image != null){
                $add_vehicle->vehicle_image = $this->custom->common_upload_images($request,'vehicle_image',$path);
            }
               $add_vehicle->save();

        }

        return redirect('admin/vehicle_list')->with('success','Vehicle Added Successfully');
} 

    public function editvehicle($id)
    {
        $data = $this->vehicle->find($id);
        return view('add_vehicle',compact('data'));
    }
    
    public function vehicle_list()
    {
        $vehicle_list = $this->vehicle->get();
        $total_vehicle = $this->vehicle->select('id')->count();
        $active_total_vehicle = $this->vehicle->where('status',1)->select('id')->count();
        $inactive_total_vehicle = $this->vehicle->where('status',0)->select('id')->count();
        return view('vehicle_list',['vehicle_list'=>$vehicle_list,'total_vehicle'=>$total_vehicle,'active_total_vehicle'=>$active_total_vehicle,'inactive_total_vehicle'=>$inactive_total_vehicle]);
    }

    public function cancellation_reason()
    {

        return view('cancellation_reason_list');
    }

    public function add_reason(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'reason' => 'required',
                'cancellation_for' => 'required',
                'status' => 'required',
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
            $reason = $request->reason;
            $cancellation_for = $request->cancellation_for;
            $status = $request->status;
            $add_reason = $this->cancellation_reason;

            $data = array();

            $data[] = array(
                'reason'=>$reason,
                'cancellation_for'=>$cancellation_for,
                'status'=>$status,
                

            );

            

        
                $add_reason->insert($data);
            
            

        }

        return back()->with('success','Cancellation Reason Added Successfully');
} 

   public function reason_list()
    {
        $reason_list = $this->cancellation_reason->get();
        $total_reason = $this->cancellation_reason->select('id')->count();
        $active_total_reason = $this->cancellation_reason->where('status',1)->select('id')->count();
        $inactive_total_reason = $this->cancellation_reason->where('status',0)->select('id')->count();

        return view('reason_list',['reason_list'=>$reason_list,'total_reason'=>$total_reason,'active_total_reason'=>$active_total_reason,'inactive_total_reason'=>$inactive_total_reason]);
    }

    public function driver()
    {
        $country=$this->country->get();
        $vehicle=$this->vehicle->get();

        return view('add_new_driver',['country'=>$country,'vehicle'=>$vehicle]);
    }

     public function generate_random_string()
    {
        return rand(11111111,99999999);
    }

    public function base_image_upload_license($request,$key)    
    {        
        $imageName = $request->file($key)->getClientOriginalName();       
         $ext = $request->file($key)->getClientOriginalExtension();
         $imageName = self::generate_random_string().'.'.$ext;        
         $request->file($key)->move('public/uploads/License/',$imageName);       
         return $imageName;
    }



    public function base_image_upload_profile($request,$key)    
    {        
        $imageName = $request->file($key)->getClientOriginalName();       
         $ext = $request->file($key)->getClientOriginalExtension();
         $imageName = self::generate_random_string().'.'.$ext;        
         $request->file($key)->move('public/uploads/Profile/',$imageName);       
         return $imageName;
    }

    public function add_driver(Request $request)
    {
        if(!$request->id){
            $rules['profile_pic'] = 'required|max:2048';
            $rules['license'] = 'required|max:2048';
            $rules['password'] = 'required';
            $rules['insurance_image'] = 'required';
            $rules['rc_image'] = 'required';
        }
            $rules['city'] = 'required';
            $rules['driver_name'] = 'required';
            $rules['vehicle_name'] = 'required';
            $rules['phone_no'] = 'required';
            $rules['address_line_1'] = 'required';
            $rules['state_province'] = 'required';
            $rules['status'] = 'required';
            $rules['zip_code'] = 'required';
            $rules['country'] = 'required';
            $rules['license_expiry'] = 'required';
            $rules['account_name'] = 'required';
            $rules['account_address'] = 'required';
            $rules['account_no'] = 'required';
            $rules['bank_name'] = 'required';
            $rules['branch_name'] = 'required';
            $rules['branch_address'] = 'required';
            $rules['rc_expiry_date'] = 'required';
            $rules['insurance_expiry_date'] = 'required';
            $rules['rc_no'] = 'required';
            $rules['insurance_no'] = 'required';
            $rules['vehicle_no'] = 'required';
            $rules['vehicle_name'] = 'required';

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
            
            if($request->id){
                $insert1 = Deliverypartners::find($request->id);
                $msg = "update_success_msg";
            }else{
                $insert1 = new Deliverypartners();
                $insert1->password=$this->encrypt_password($request->password);
                $msg = "add_success_msg";
                
            }
            
            if($request->license){
                $license = self::base_image_upload_license($request,'license');
                $insert1->license=$license;
            }
            if($request->license){
                $profile_picture = self::base_image_upload_profile($request,'profile_pic');
                $insert1->profile_pic=$profile_picture;
            }
            
            $insert1->partner_id=$this->generate_partner_id();
            $insert1->name=$request->driver_name;
            $insert1->email=$request->email;
            $insert1->phone=$request->phone_no;
            $insert1->expiry_date=date("Y-m-d",strtotime($request->license_expiry));
            $insert1->status=$request->status;
            $insert1->save();

            $partner_id = $insert1->id;
            
            if($request->id){
                $insert = $this->delivery_partner_details->where('delivery_partners_id',$request->id)->first();
                if(empty($insert)) $insert = $this->delivery_partner_details;
            }else{
                $insert = $this->delivery_partner_details;
            }

            $insert->delivery_partners_id=$partner_id;
            $insert->city=$request->city;
            $insert->vehicle_name=$request->vehicle_name;
            $insert->address_line_1=$request->address_line_1;
            $insert->address_line_2=$request->address_line_2;
            $insert->address_city=$request->address_city;
            $insert->state_province=$request->state_province;
            $insert->country=$request->country;
            $insert->zip_code=$request->zip_code;
            $insert->about=$request->about;
            $insert->account_name=$request->account_name;
            $insert->account_address=$request->account_address;
            $insert->account_no=$request->account_no;
            $insert->bank_name=$request->bank_name;
            $insert->branch_name= $request->branch_name;
            $insert->branch_address=$request->branch_address;
            $insert->swift_code=$request->swift_code;
            $insert->routing_no=$request->routing_no;
            $insert->save();


            if($request->id){
                $vehicle = $this->vehicle->where('delivery_partners_id',$request->id)->first();
                if(empty($vehicle)) $vehicle = $this->vehicle;
            }else{
                $vehicle = $this->vehicle;
            }
            if($request->insurance_image){
                $insurance_image = self::base_image_upload_profile($request,'insurance_image');
                $vehicle->insurance_image=$insurance_image;
            }
            if($request->rc_image){
                $rc_image = self::base_image_upload_profile($request,'rc_image');
                $vehicle->rc_image=$rc_image;
            }
            $vehicle->delivery_partners_id=$partner_id;
            $vehicle->vehicle_name=$request->vehicle_name;
            $vehicle->vehicle_no=$request->vehicle_no;
            $vehicle->insurance_no=$request->insurance_no;
            $vehicle->rc_no=$request->rc_no;
            $vehicle->insurance_expiry_date=date("Y-m-d",strtotime($request->insurance_expiry_date));
            $vehicle->rc_expiry_date=date("Y-m-d",strtotime($request->rc_expiry_date));
            $vehicle->save();
        }


        return redirect('admin/driver_list')->with('success',trans('constants.'.$msg,['param'=>'Driver']));
} 

    public function edit_delivery_boy_details($id,Request $request)
    {
        $deliverypartners = $this->deliverypartners;

        $delivery_partner_detail = $deliverypartners->find($id);
        $profile_pic = BASE_URL.$delivery_partner_detail->profile_pic;
        if($delivery_partner_detail->expiry_date!='') $delivery_partner_detail->expiry_date = date("d F, Y",strtotime($delivery_partner_detail->expiry_date));
        if(isset($delivery_partner_detail->Vehicle->insurance_expiry_date)) $delivery_partner_detail->Vehicle->insurance_expiry_date = date("d F, Y",strtotime($delivery_partner_detail->Vehicle->insurance_expiry_date));
        if(isset($delivery_partner_detail->Vehicle->rc_expiry_date)) $delivery_partner_detail->Vehicle->rc_expiry_date = date("d F, Y",strtotime($delivery_partner_detail->Vehicle->rc_expiry_date));
        
        if(isset($delivery_partner_detail->Deliverypartner_detail->city))
            $city=$this->addcity->find($delivery_partner_detail->Deliverypartner_detail->city);
        else
            $city=array();

        if(isset($delivery_partner_detail->Deliverypartner_detail->state_province))
            $state=$this->state->find($delivery_partner_detail->Deliverypartner_detail->state_province);
        else
            $state=array();

        $vehicle=$this->vehicle->get();
        $country=$this->country->get();

        return view('add_new_driver',['insert1'=>$delivery_partner_detail,'country'=>$country,'state'=>$state,'city'=>$city,'vehicle'=>$vehicle])->with('delivery_partner_commision',$delivery_partner_detail->partner_commision)->with('profile_icon',$profile_pic);

    }

    public function delete_delivery_boy($id)
       {
        $delete =  $this->deliverypartners->where('id',$id)->delete();

        $delete1 = DB::table('delivery_partner_details')->where('delivery_partners_id',$id)->delete();

        return back()->with('success','Delivery Partner Deleted Successfully');  
       }


   

    public function driver_list()
    {

        $data=$this->deliverypartners->with('Foodrequest')->get();
//dd($data[0]->Deliverypartner_detail->Citylist->city);
        $all_drivers=$this->deliverypartners->count('id');

        $active_drivers=$this->deliverypartners->where('status',1)->count();

        $in_active_drivers=$this->deliverypartners->where('status',0)->count();
      
        return view('driver_list',['data'=>$data,'all_drivers'=>$all_drivers,'active_drivers'=>$active_drivers,'in_active_drivers'=>$in_active_drivers]);
    }

     public function view_deliveryboy_order_details($id)
    {
      $delivery_boy_details=DB::table('requests')
      ->where('delivery_boy_id',$id)
      ->join('users','users.id','=','requests.user_id')
      ->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
      ->join('restaurants','restaurants.id','=','requests.restaurant_id')
      ->select('users.name as customer_name','users.phone as phone','delivery_partners.name as driver_name','restaurants.restaurant_name as restaurant_name','users.*','requests.*','delivery_partners.*','restaurants.*','requests.status as status')
      ->get();
      
      return view('/view_delivery_boy_order_details',['delivery_boy_details'=>$delivery_boy_details]);
    }

    /**
     * function to get city data 
     * @param int $id
     * @return array to blade file
     */
    public function edit_city($id)
    {
      
        $country = $this->country->get();
        //get city data based on id
        $city_data = $this->addcity->where('id',$id)->with(['Country','State'])->first();
        //dd($data);

        return view('/edit_city',compact('city_data','country'));
    }

    public function update_city(Request $request)
    {
        $validator = Validator::make($request->all(), [
                
                'city' => 'required',
                'country' => 'required',
                'state' => 'required',
                'admin_commision' => 'required',
                'status' => 'required',
                'geofence_latlng' => 'required'
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());
            //print_r($validator->messages()); exit;
            //return back()->with('error', $error_messages);
            return back()->withErrors($validator)->withInput();

        }else
        {
            /*if(!empty($request->geofence_latlng))
            {
                $source_polygon = explode(',',$request->geofence_latlng);
                $j = $k =0;
                for ($i=0; $i <= count($source_polygon)-1 && $k <= count($source_polygon)-1; $i++) {
                        $source_coordinates[$j][0] = $source_polygon[$k++];
                        $source_coordinates[$j][0] = (double)trim($source_coordinates[$j][0],'[ ');
                        $source_coordinates[$j][1] = $source_polygon[$k];
                        $source_coordinates[$j][1] = (double)trim($source_coordinates[$j][1],' ]');

                        $temp = $source_coordinates[$j][0];
                        $source_coordinates[$j][0] = $source_coordinates[$j][1];
                        $source_coordinates[$j][1] = $temp;
                        $k++;$j++;
                }
            } else {
                $source_coordinates = array();
            }*/
            //echo "<pre>";print_r( $source_coordinates);
            // $first_cordinate = current($source_coordinates);
            // $a = end($source_coordinates);
            // $key = key($source_coordinates); 
            // $source_coordinates[$key+1] = $first_cordinate;
            //echo "<pre>";print_r( $source_coordinates);exit;
            
            $city = $request->city;
            $country_id = $request->country;
            $state_id = $request->state;
            $admin_commision = $request->admin_commision;
            $default_delivery_amount = $request->default_delivery_amount;
            $target_amount = $request->target_amount;
            $driver_base_price = $request->driver_base_price;
            $min_dist_base_price = $request->min_dist_base_price;
            $extra_fee_amount = $request->extra_fee_amount;
            $extra_fee_amount_each = $request->extra_fee_amount_each;
            $night_fare_amount = $request->night_fare_amount;
            $night_driver_share = $request->night_driver_share;
            $surge_fare_amount = $request->surge_fare_amount;
            $surge_driver_share = $request->surge_driver_share;
            $status = $request->status;
            $add_city = $this->addcity;

            $data = array();

            // $data[] = array(
                
            //  'city'=>$city,
            //  'admin_commision'=>$admin_commision,
            //  'default_delivery_amount'=>$default_delivery_amount,
            //  'target_amount'=>$target_amount,
            //  'driver_base_price'=>$driver_base_price,
            //  'min_dist_base_price'=>$min_dist_base_price,
            //  'extra_fee_amount'=>$extra_fee_amount,
            //  'extra_fee_amount_each'=>$extra_fee_amount_each,
            //  'night_fare_amount'=>$night_fare_amount,
            //  'night_driver_share'=>$night_driver_share,
            //  'surge_fare_amount'=>$surge_fare_amount,
            //  'surge_driver_share'=>$surge_driver_share,
            //  'status'=>$status,

            // );
            $citydata = $add_city->find($request->id);
            $citydata->city = $city;
            $add_city->country_id = $country_id;
            $add_city->state_id = $state_id;
            $citydata->admin_commision = $admin_commision;
            $citydata->default_delivery_amount = $default_delivery_amount;
            $citydata->target_amount = $target_amount;
            $citydata->driver_base_price = $driver_base_price;
            $citydata->min_dist_base_price = $min_dist_base_price;
            $citydata->extra_fee_amount = $extra_fee_amount;
            $citydata->extra_fee_amount_each = $extra_fee_amount_each;
            $citydata->night_fare_amount = $night_fare_amount;
            $citydata->night_driver_share = $night_driver_share;
            $citydata->surge_fare_amount = $surge_fare_amount;
            $citydata->surge_driver_share = $surge_driver_share;
            $citydata->status = $status;
            $citydata->save();
            
            //add data in city_geofencing table
            $geofencing = new City_geofencing();
            $geofencingdata = $geofencing->where('city_id',$citydata->id)->first();
            if(empty($geofencingdata)) $geofencingdata = $geofencing;
            $geofencingdata->city_id = $citydata->id;
            $geofencingdata->latitude = !empty($request->latitude)?$request->latitude:env('DEFAULT_LAT');
            $geofencingdata->longitude = !empty($request->longitude)?$request->longitude:env('DEFAULT_LNG');
            $geofencingdata->polygons = $request->geofence_latlng;
            //$geofencing->save();
            $geofencingdata->save();
            //$data = $city_id->city_geofencing()->create($geofencing);
        }

        return redirect('/admin/city_list')->with('success','City  Added Successfully');
    } 

    /**
     * get add_ons, menu based on restaurant
     * 
     * @param int $id
     * 
     * @return json $data
     */
    public function getrestaurant_based_detail($id)
    {
        $data = $this->restaurants->with(['Add_ons','Menu'])->find($id);
        //dd($data);
        return $data;
    }


    /**
     * get area based on city
     * 
     * @param int $id
     * 
     * @return json $data
     */
    public function getcity_area($id)
    {
        $data = $this->addcity->with(['Area'])->find($id);
        //dd($data);
        return $data;
    }


    public function check_restaurant_address(Request $request)
    {
        $area_id = $request->area_id;
        $source_lat = $request->lat;
        $source_lng = $request->lng;

        $data = $this->addarea->where('id',$area_id)
                    ->selectRaw("(6371 * acos(cos(radians(" . $source_lat . "))* cos(radians(`latitude`)) 
                            * cos(radians(`longitude`) - radians(" . $source_lng . ")) + sin(radians(" . $source_lat . ")) 
                            * sin(radians(`latitude`)))) as distance")
                    ->having('distance','<=',DEFAULT_RADIUS)
                    ->orderBy('distance')
                    ->first();

        return $data;
    }
    
    public function setting_edit_restaurant($id,Request $request)
    {

        $city = $this->addcity->get();
        $area = $this->addarea->get();
        $cuisines = $this->cuisines->get();
        $title = "EDIT RESTAURANT";
        $restaurants = $this->restaurants;
        $restaurant_detail = $restaurants->where('id',$id)->with(['Cuisines','Document','RestaurantBankDetails'])->first();
        $document = $this->document->where('document_for',2)->where('status',1)->get();
        //dd($restaurant_detail->document);
        $cuisine_ids = array();
        foreach($restaurant_detail->Cuisines as $val){
            $cuisine_ids[] = $val->id;
        }
       // dd($restaurant_detail);
        return view('setting_add_restaurant',['cuisine_ids'=>$cuisine_ids,'data'=>$restaurant_detail,'city'=>$city,'area'=>$area,'title'=>$title,'cuisines'=>$cuisines,'document'=>$document]);
    }
    
    public function setting_edit_account($id,Request $request)
    {

        $city = $this->addcity->get();
        $area = $this->addarea->get();
        $cuisines = $this->cuisines->get();
        $title = "EDIT RESTAURANT";
        $restaurants = $this->restaurants;
        $restaurant_detail = $restaurants->where('id',$id)->with(['Cuisines','Document','RestaurantBankDetails'])->first();
        $document = $this->document->where('document_for',2)->where('status',1)->get();
        //dd($restaurant_detail->document);
        $cuisine_ids = array();
        foreach($restaurant_detail->Cuisines as $val){
            $cuisine_ids[] = $val->id;
        }
       // dd($restaurant_detail);
        return view('setting_add_account',['cuisine_ids'=>$cuisine_ids,'data'=>$restaurant_detail,'city'=>$city,'area'=>$area,'title'=>$title,'cuisines'=>$cuisines,'document'=>$document]);
    }
    public function setting_edit_paypal($id,Request $request)
    {

        $city = $this->addcity->get();
        $area = $this->addarea->get();
        $cuisines = $this->cuisines->get();
        $title = "EDIT Paypal Detail";
        $restaurants = $this->restaurants;
        $restaurant_detail = $restaurants->where('id',$id)->with(['Cuisines','Document','RestaurantBankDetails'])->first();
        $document = $this->document->where('document_for',2)->where('status',1)->get();
        //dd($restaurant_detail->document);
        $cuisine_ids = array();
        foreach($restaurant_detail->Cuisines as $val){
            $cuisine_ids[] = $val->id;
        }
       // dd($restaurant_detail);
        return view('setting_add_paypal',['cuisine_ids'=>$cuisine_ids,'data'=>$restaurant_detail,'city'=>$city,'area'=>$area,'title'=>$title,'cuisines'=>$cuisines,'document'=>$document]);
    }
    public function setting_add_to_accounts(Request $request)
    {
     // dd($request->all());
        $rules = array(
            'account_name' => 'required',
            'account_address' => 'required',
            'account_no' => 'required',
            'bank_name' => 'required',
            'branch_name' => 'required',
            'branch_address' => 'required',
        );
      
                
                    $restaurant_bank_details = $this->restaurant_bank_details->where('restaurant_id',$request->id)->first();
                   
                    if(!empty($restaurant_bank_details)){
                    $restaurant_bank_details->account_name = $request->account_name;
                    $restaurant_bank_details->account_address = $request->account_address;
                    $restaurant_bank_details->account_no = $request->account_no;
                    $restaurant_bank_details->bank_name = $request->bank_name;
                    $restaurant_bank_details->branch_name = $request->branch_name;
                    $restaurant_bank_details->branch_address = $request->branch_address;
                    $restaurant_bank_details->swift_code = $request->swift_code;
                    $restaurant_bank_details->routing_no = $request->routing_no;
                    $restaurant_bank_details->save();
                    $msg = "update_success_msg";
                    }else{
                    $this->restaurant_bank_details->restaurant_id = $request->id;
                    $this->restaurant_bank_details->account_name = $request->account_name;
                    $this->restaurant_bank_details->account_address = $request->account_address;
                    $this->restaurant_bank_details->account_no = $request->account_no;
                    $this->restaurant_bank_details->bank_name = $request->bank_name;
                    $this->restaurant_bank_details->branch_name = $request->branch_name;
                    $this->restaurant_bank_details->branch_address = $request->branch_address;
                    $this->restaurant_bank_details->swift_code = $request->swift_code;
                    $this->restaurant_bank_details->routing_no = $request->routing_no;
                    $this->restaurant_bank_details->save();
                    $msg = "add_success_msg";
                }
        return redirect('admin/setting/edit_account/'.session()->get('userid'))->with('success',trans('constants.'.$msg,['param'=>'Restaurant']));
    }
    
    public function setting_add_to_paypal(Request $request)
    {
     // dd($request->all());
        $rules = array(
            'paypal_id' => 'required',
           
        );
      
                
                    $restaurant_bank_details = $this->restaurant_bank_details->where('restaurant_id',$request->id)->first();
                   
                    if(!empty($restaurant_bank_details)){
                    $restaurant_bank_details->paypal_id = $request->paypal_id;
                    
                    $restaurant_bank_details->save();
                    $msg = "update_success_msg";
                    }else{
                    $this->restaurant_bank_details->restaurant_id = $request->id;
                    $this->restaurant_bank_details->paypal_id = $request->paypal_id;
                    
                    $this->restaurant_bank_details->save();
                    $msg = "add_success_msg";
                }
        return redirect('admin/setting/edit_paypal/'.session()->get('userid'))->with('success',trans('constants.'.$msg,['param'=>'Restaurant']));
    }
    public function setting_add_to_restaurants(Request $request)
    {
     //dd($request->all());
        $rules = array(
            'name' => 'required|max:50',
            //'password' => 'required',
            'city' => 'required',
           // 'area' => 'required',
            //'status' => 'required',
            'opening_time' => 'required',
            'closing_time' => 'required',
            'weekend_opening_time' => 'required',
            'weekend_closing_time' => 'required',
            'estimated_delivery_time' => 'required',
            'address' => 'required',
            'packaging_charge' => 'required',
            //'offer_percentage' => 'required',
            'delivery_type' => 'required|array',
            'cuisines' => 'required|array',
            /*'account_name' => 'required',
            'account_address' => 'required',
            'account_no' => 'required',
            'bank_name' => 'required',
            'branch_name' => 'required',
            'branch_address' => 'required',*/
        );
        if($request->id!='')
        {
            $rules['email'] = 'required|unique:restaurants,email,'.$request->id;
            $rules['phone'] = 'required|numeric|unique:restaurants,phone,'.$request->id;
        }else
        {
            $rules['image'] = 'required|max:2048|mimes:jpeg,jpg,bmp,png';
            $rules['email'] = 'required|unique:restaurants,email';
            $rules['phone'] = 'required|numeric|unique:restaurants,phone';
            $rules['status'] = 'required';
        }
        // foreach($request->document as $key=>$value){
        //     $rules['document.document.*'] = 'max:2048';
        // }
       // dd($rules);
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
            
            //dd($request->document);
                $restaurants = $this->restaurants;
                $custom = $this->custom;
                $name = $request->name;
                $email = $request->email;
                $phone = $request->phone;
                $city = $request->city;
                $area = $request->area;
                $discount_type = $request->discount_type;
                $target_amount = $request->target_amount;
                $offer_amount = $request->offer_amount;
                $admin_commision =  $request->admin_commision;
                 if(isset($request->status))
                {
                 $status = $request->status;
                }
                else
                {
                $status=0;
                }
                $opening_time = date("H:i:s",strtotime($request->opening_time));
                $closing_time = date("H:i:s",strtotime($request->closing_time));
                $weekend_opening_time = date("H:i:s",strtotime($request->weekend_opening_time));
                $weekend_closing_time = date("H:i:s",strtotime($request->weekend_closing_time));
                if($request->image)
                {
                    $image = $custom->restaurant_upload_image($request,'image');
                }else
                {
                    $image=PROFILE_ICON;
                }
                $packaging_charge = $request->packaging_charge;
                //$offer_percentage = $request->offer_percentage;
                if($request->shop_description)
                {
                $shop_description = $request->shop_description;
                }else
                {
                    $shop_description = "";
                }
                $estimated_delivery_time = $request->estimated_delivery_time;
                $address = $request->address;

                if($request->id)
                {
                    
                    $restaurants_det = $restaurants->find($request->id);
                    if(!$request->image)
                    {
                        $image = $restaurants_det->image;
                    }
                                                                
                    $restaurants_det->restaurant_name = $name;
                    $restaurants_det->image = $image;
                    $restaurants_det->email = $email;
                    $restaurants_det->org_password = $request->password;
                    $restaurants_det->password = Hash::make($request->password);
                    $restaurants_det->phone = $phone;
                    $restaurants_det->city = $city;
                    $restaurants_det->area = $area;
                    $restaurants_det->discount_type = $discount_type;
                    $restaurants_det->target_amount = $target_amount;
                    $restaurants_det->offer_amount = $offer_amount;
                    $restaurants_det->admin_commision = $admin_commision;
                    $restaurants_det->restaurant_delivery_charge = $request->restaurant_delivery_charge;
                    $restaurants_det->driver_commision = $request->driver_commision;
                    //$restaurants_det->discount = $offer_percentage;
                    $restaurants_det->shop_description = $shop_description;
                    $restaurants_det->is_open = 0;
                    $restaurants_det->estimated_delivery_time = $estimated_delivery_time;
                    $restaurants_det->packaging_charge = $packaging_charge;
                    $restaurants_det->address = $address;
                    $restaurants_det->opening_time = $opening_time;
                    $restaurants_det->closing_time = $closing_time;
                    $restaurants_det->weekend_opening_time = $weekend_opening_time;
                    $restaurants_det->weekend_closing_time = $weekend_closing_time;
                    $restaurants_det->status = $status;
                    $restaurants_det->delivery_type = json_encode($request->delivery_type);
                    $restaurants_det->save();
                                
                    $cuisines = $this->cuisines->find($request->cuisines);
                    //update many to many relationship data
                    $restaurants_det->Cuisines()->sync($cuisines);

                    //data insert into document many to many
                    $sync_data=array();
                    if(!empty($request->document)){
                        foreach($request->document as $key=>$value){
                            if($_FILES['document']['name'][$key]['document']!='')
                            {
                                $expiry_date='';
                                if(isset($value['date']) && $value['date']!=null) $expiry_date=date("Y-m-d",strtotime($value['date']));

                                $filename = strtotime(date("Y-m-d")).basename($_FILES['document']['name'][$key]['document']);
                                move_uploaded_file($_FILES["document"]["tmp_name"][$key]['document'], 'public/uploads/Restaurant Document/'.$filename);                                
                                
                                $sync_data[$key] = ['document' => $filename,'expiry_date'=>$expiry_date];

                                // if($expiry_date!='' && $_FILES['document']['name'][$key]['document']!='')
                                //     $sync_data[$key] = ['document' => $filename,'expiry_date'=>$expiry_date];
                                // elseif($expiry_date==''  && $_FILES['document']['name'][$key]['document']!='')
                                //     $sync_data[$key] = ['document' => $filename, ,'expiry_date'=>$expiry_date];
                                // elseif($expiry_date!=''  && $_FILES['document']['name'][$key]['document']=='')
                                //     $sync_data[$key] = ['expiry_date'=>$expiry_date];
                            }
                        }
                        $restaurants_det->Document()->sync($sync_data);
                    }
                    $restaurant_bank_details = $this->restaurant_bank_details->where('restaurant_id',$request->id)->first();
                    if(empty($restaurant_bank_details)) $restaurant_bank_details = $this->restaurant_bank_details;
                    $restaurant_bank_details->account_name = $request->account_name;
                    $restaurant_bank_details->account_address = $request->account_address;
                    $restaurant_bank_details->account_no = $request->account_no;
                    $restaurant_bank_details->bank_name = $request->bank_name;
                    $restaurant_bank_details->branch_name = $request->branch_name;
                    $restaurant_bank_details->branch_address = $request->branch_address;
                    $restaurant_bank_details->swift_code = $request->swift_code;
                    $restaurant_bank_details->routing_no = $request->routing_no;
                    $restaurants_det->RestaurantBankDetails()->save($restaurant_bank_details);
                    $msg = "update_success_msg";

                }else
                {
                    $check_email_phone = $restaurants->where('email',$request->email)->orwhere('phone',$request->phone)->first();
                    if($check_email_phone){
                        return back()->with('error', 'Email/Phone already exists');
                    }
                    $restaurants->restaurant_name = $name;
                    $restaurants->image = $image;
                    $restaurants->email = $email;
                    $restaurants->org_password = $request->password;
                    $restaurants->password = Hash::make($request->password);
                    $restaurants->phone = $phone;
                    $restaurants->city = $city;
                    $restaurants->area = $area;
                    $restaurants->discount_type = $discount_type;
                    $restaurants->target_amount = $target_amount;
                    $restaurants->offer_amount = $offer_amount;
                    $restaurants->admin_commision = $admin_commision;
                    $restaurants->restaurant_delivery_charge = $request->restaurant_delivery_charge;
                    $restaurants->driver_commision = $request->driver_commision;
                    //$restaurants->discount = $offer_percentage;
                    $restaurants->shop_description = $shop_description;
                    $restaurants->is_open = 0;
                    $restaurants->estimated_delivery_time = $estimated_delivery_time;
                    $restaurants->packaging_charge = $packaging_charge;
                    $restaurants->address = $address;
                    $restaurants->lat = $request->latitude;
                    $restaurants->lng = $request->longitude;
                    $restaurants->opening_time = $opening_time;
                    $restaurants->closing_time = $closing_time;
                    $restaurants->weekend_opening_time = $weekend_opening_time;
                    $restaurants->weekend_closing_time = $weekend_closing_time;
                    $restaurants->status = $status;
                    $restaurants->delivery_type = json_encode($request->delivery_type);
                    $restaurants->save();
                    
                    $cuisines = $this->cuisines->find($request->cuisines);
                    $restaurants->Cuisines()->attach($cuisines);

                    //$food_quantity = $this->document->find($request->food_quantity);
                    $sync_data=array();
                    if(!empty($request->document)){
                        foreach($request->document as $key=>$value){
                            if($_FILES['document']['name'][$key]['document']!='')
                            {
                                $expiry_date='';
                                if(isset($value['date'])) $expiry_date=date("Y-m-d",strtotime($value['date']));

                                $filename = strtotime(date("Y-m-d")).basename($_FILES['document']['name'][$key]['document']);
                                move_uploaded_file($_FILES["document"]["tmp_name"][$key]['document'], 'public/uploads/Restaurant Document/'.$filename);
                                // $filename = $this->base_image_upload($request,'document','Restaurant Documents');
                                
                                $sync_data[$key] = ['document' => $filename,'expiry_date'=>$expiry_date];

                                // if($expiry_date=='')
                                //     $sync_data[$key] = ['document' => $filename];
                                // else
                                //     $sync_data[$key] = ['document' => $filename,'expiry_date'=>$expiry_date];
                            }
                        }
                        //dd($sync_data);
                        $restaurants->Document()->attach($sync_data);
                    }

                    $this->restaurant_bank_details->account_name = $request->account_name;
                    $this->restaurant_bank_details->account_address = $request->account_address;
                    $this->restaurant_bank_details->account_no = $request->account_no;
                    $this->restaurant_bank_details->bank_name = $request->bank_name;
                    $this->restaurant_bank_details->branch_name = $request->branch_name;
                    $this->restaurant_bank_details->branch_address = $request->branch_address;
                    $this->restaurant_bank_details->swift_code = $request->swift_code;
                    $this->restaurant_bank_details->routing_no = $request->routing_no;
                    $restaurants->RestaurantBankDetails()->save($this->restaurant_bank_details);

                    $msg = "add_success_msg";
                }
                
        }

        return redirect('admin/setting/edit_store/'.$restaurants_det->id)->with('success',trans('constants.'.$msg,['param'=>'Restaurant']));

    }

} 