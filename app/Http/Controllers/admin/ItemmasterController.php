<?php

namespace App\Http\Controllers\admin;
                                    
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController;
use DB;
use Illuminate\Validation\Rule;
use PhpParser\Node\Expr\List_;
use App\Model\Add_ons;
use App\Model\FoodQuantity;


class ItemmasterController extends BaseController
{


	/**
	 * get add category page
	 * 
	 * @return view page
	 * 
	 */
	public function index()
	{
		return view('add_category');
	}


	public function get_category_list(Request $request)
	{
	
		$data = DB::table('category')->get();
		$active_total_category = DB::table('category')->where('status',1)->select('id')->count();
        $inactive_total_category = DB::table('category')->where('status',0)->select('id')->count();

		// print_r($data); exit;
		
		return view('category_list',['data'=>$data,'active_total_category'=>$active_total_category,'inactive_total_category'=>$inactive_total_category]);
	}

	public function add_to_category(Request $request)
	{

		$validator = Validator::make($request->all(), [
                'category_name' => 'required',
                'status' => 'required'
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
			$category_name = $request->category_name;
			$status = $request->status;
			$category_list = $this->category;
			if($request->id)
			{
					$category_list->where('id',$request->id)->update([
						'category_name'=>$category_name,
						'status'=>$status
					]);
			}else
			{
					$data = array();

					$data[] = array(
					'category_name'=>$category_name,
					'status'=>$status
					);

					$category_list->insert($data);
			}

		}

		return redirect('/admin/category_list')->with('success','Category added Successfully');
	}

	public function edit_category($category_id)
	{
		$category_list = $this->category;

		$data = $category_list->where('id',$category_id)->first();

		return view('add_category',['data'=>$data]);
	}

	public function delete_category($category_id)
	{
		$this->category->where('id',$category_id)->delete();

		return redirect('/admin/category_list')->with('success','Category Deleted Successfully');
	}

	public function get_cuisines_list(Request $request)
	{
		$data = DB::table('cuisines')->get();
		$active_total_cuisines = DB::table('cuisines')->where('status',1)->select('id')->count();
        $inactive_total_cuisines = DB::table('cuisines')->where('status',0)->select('id')->count();

		return view('cuisines_list',['data'=>$data,'active_total_cuisines'=>$active_total_cuisines,'inactive_total_cuisines'=>$inactive_total_cuisines]);
	}

	

		public function add_cuisines(Request $request)
	{

		return view('add_cuisines');

	}

		public function add_to_cuisines(Request $request)
	{

		$validator = Validator::make($request->all(), [
            'cuisine_name' => 'required|max:30',
        ]);

		if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
			$cuisine_name = $request->cuisine_name;
			
			if($request->id)
			{
				$this->cuisines->where('id',$request->id)->update(['name'=>$cuisine_name]);
			}else
			{
					$data = array();

					$data[] = array(
					'name'=>$cuisine_name
					);

					$this->cuisines->insert($data);
			}


			return redirect('/admin/cuisines_list')->with('success','Cusine Added');
		}
	}

	public function delete_cuisine(Request $request)
	{
			$validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
        	$id = $request->id;
        	$cuisines = $this->cuisines;

        	$cuisines->where('id',$id)->delete();

        	return redirect('/admin/cuisines_list')->with('success','Cuisine Removed');
        }
	}


	public function delete_addon(Request $request)
	{
			$validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
        	$id = $request->id;
        	$addon = $this->add_ons;

        	$addon->where('id',$id)->delete();

        	return redirect('/admin/addons_list')->with('success','Addon Removed');
        }
	}



	/**
	 * Store or update add-ons data in db
	 * 
	 * @param array $request
	 * 
	 * @return view page
	 */
	public function store_addons(Request $request)
	{
		if($request->session()->get('role')==1){
			$restaurant_id = $request->restaurant_name;
		}else{
			$restaurant_id = $request->session()->get('userid');
		}
		$name = $request->name;
		$id = $request->id;
		if($request->id){
			$validator = Validator::make($request->all(), [
                'name' => [
						'required',
						Rule::unique('add_ons')->where(function ($query) use($id,$name,$restaurant_id) {
							return $query->where('restaurant_id', $restaurant_id)
							->where('name', $name)
							->where('id','!=',$id);
						}),
					],
                'price' => 'required'
			]);
		}else{
			$validator = Validator::make($request->all(), [
                'name' => [
					'required',
					Rule::unique('add_ons')->where(function ($query) use($name,$restaurant_id) {
						return $query->where('restaurant_id', $restaurant_id)
						->where('name', $name);
					}),
				],
                'price' => 'required'
			]);
		}

		if($validator->fails()) 
		{
            $error_messages = implode(',',$validator->messages()->all());
            return back()->with('error', $error_messages);

        }else
        {
			if($request->id)
			{
					$addons = $this->add_ons->find($request->id);
					$addons->restaurant_id = $restaurant_id;
					$addons->name = $request->name;
					$addons->price = $request->price;
					$addons->save();
					$trans_msg = "update_success_msg";
			}else
			{
				$this->add_ons->restaurant_id = $restaurant_id;
				$this->add_ons->name = $request->name;
				$this->add_ons->price = $request->price;
				$this->add_ons->save();
				$trans_msg = "add_success_msg";
			}

		}
		return redirect('/admin/addons_list')->with('success',trans('constants.'.$trans_msg,[ 'param' => 'Add-ons']));
	}


	/**
	 * add view of add_ons
	 * 
	 * @param object $request
	 * 
	 * @return Add_ons view page
	 */
	public function add_addons(Request $request)
	{
		$restaurant = $this->restaurants->get();
		// print_r($data); exit;
		return view('add_addons',['restaurant'=>$restaurant]);
	}


	/**
	 * List view of add_ons
	 * 
	 * @return Add_ons list view page
	 */
	public function list_addons(Request $request)
	{
		if($request->session()->get('role')==1){
			$data = $this->add_ons->with('Restaurant')->get();
		}else{
			$restaurant_id = $request->session()->get('userid');
			$data = $this->add_ons->with('Restaurant')->where('restaurant_id',$restaurant_id)->get();
		}
		$total_addon = $this->add_ons->select('id')->count();
        $active_addon = $this->add_ons->where('status',1)->select('id')->count();
        $inactive_addon = $this->add_ons->where('status',0)->select('id')->count();
		 //dd($data); 
		return view('addons_list',['addons_list'=>$data,'total_addon'=>$total_addon,'active_addon'=>$active_addon,'inactive_addon'=>$inactive_addon]);
	}


	/**
	 * edit view of add_ons
	 * 
	 * @param int $id
	 * 
	 * @return add_ons edit view page
	 */
	public function edit_addons($id)
	{
		$data = $this->add_ons->with('Restaurant')->find($id);
		$restaurant = $this->restaurants->get();
		// print_r($data); exit;
		return view('add_addons',['data'=>$data,'restaurant'=>$restaurant]);
	}
    
    public function addon_status_enable(Request $request)
    {
   
    $approve=$this->add_ons->where('id',$request->id)->update(['status'=>1]);

    return back()->with('success','Addons Enabled');

    }

        public function addon_status_disable(Request $request)
    {
   
    $approve=$this->add_ons->where('id',$request->id)->update(['status'=>0]);

    return back()->with('success','Addons Disabled');

    }

	/**
	 * List view of food_quantity
	 * 
	 * @return FoodQuantity List view page
	 */
	public function list_food_quantity()
	{
		$data = $this->food_quantity->get();
		$active_total_food = $this->food_quantity->where('status',1)->select('id')->count();
        $inactive_total_food = $this->food_quantity->where('status',0)->select('id')->count();
		 //dd($data); 
		return view('food_quantity_list',['food_quantity_list'=>$data,'active_total_food'=>$active_total_food,'inactive_total_food'=>$inactive_total_food]);
	}

	public function delete_food_quantity($id)
	{
		$this->food_quantity->where('id',$id)->delete();

		return redirect('/admin/food-quantity-list')->with('success','Food Quantity Deleted Successfully');
	}


	/**
	 * edit view of food_quantity
	 * 
	 * @param int $id
	 * 
	 * @return FoodQuantity edit view page
	 */
	public function edit_food_quantity($id)
	{
		$data = $this->food_quantity->find($id);
		// print_r($data); exit;
		return view('add_food_quantity',['data'=>$data]);
	}


	/**
	 * add view of food_quantity
	 * 
	 * @param object $request
	 * 
	 * @return FoodQuantity add view page
	 */
	public function add_food_quantity(Request $request)
	{
		return view('add_food_quantity');
	}


	/**
	 * Store or update add-ons data in db
	 * 
	 * @param array $request
	 * 
	 * @return view page
	 */
	public function store_food_quantity(Request $request)
	{
		if($request->id){
			$validator = Validator::make($request->all(), [
                'name' => 'required|unique:food_quantity,name,'.$request->id,
			]);
		}else{
			$validator = Validator::make($request->all(), [
                'name' => 'required|unique:food_quantity,name',
			]);
		}

		if($validator->fails()) 
		{
            $error_messages = implode(',',$validator->messages()->all());
            return back()->with('error', $error_messages);

        }else
        {
			if($request->id)
			{
					$food_quantity = $this->food_quantity->find($request->id);
					$food_quantity->name = $request->name;
					$food_quantity->save();
					$trans_msg = "update_success_msg";
			}else
			{
				$this->food_quantity->name = $request->name;
				$this->food_quantity->save();
				$trans_msg = "add_success_msg";
			}

		}
		return redirect('/admin/food-quantity-list')->with('success',trans('constants.'.$trans_msg,[ 'param' => 'Food Quantity']));
	}
}