<?php

namespace App\Http\Controllers\admin;
                                    
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController;
use DB;

class RestaurantmasterController extends BaseController
{
	public function restaurant_cuisines(Request $request)
	{
		$restaurant_id = $request->session()->get('userid');

		$restaurant_cuisines = DB::table('restaurant_cuisines')->where('restaurant_id',$restaurant_id)
															   ->join('cuisines','cuisines.id','=','restaurant_cuisines.cuisine_id')
															   ->select('restaurant_cuisines.id as cid','restaurant_cuisines.status','cuisines.name as cuisine_name','cuisines.cuisine_image')
															   ->get();


		$cuisines = $this->cuisines->get();
        $total_cuisines = $this->restaurantcuisines->select('id')->where('restaurant_id',$restaurant_id)->count();
        $active_total_cuisines = $this->restaurantcuisines->where(['status'=>1,'restaurant_id'=>$restaurant_id])->select('id')->count();
        $inactive_total_cuisines = $this->restaurantcuisines->where(['status'=>0,'restaurant_id'=>$restaurant_id])->select('id')->count();
		return view('restaurant_cuisines',['restaurant_cuisines'=>$restaurant_cuisines,'cuisines'=>$cuisines,'total_cuisines'=>$total_cuisines,'active_total_cuisines'=>$active_total_cuisines,'inactive_total_cuisines'=>$inactive_total_cuisines]);

	}
    
    public function add_restaurant_cuisines(){
        $cuisines = $this->cuisines->get(); 
        return view('add_restaurant_cuisines',compact('cuisines'));
    }

	public function add_to_restaurant_cuisines(Request $request)
	{
		$validator = Validator::make($request->all(), [
                'cuisine_id' => 'required',
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
        	$restaurant_id = $request->session()->get('userid');
        	$cuisine_id = $request->cuisine_id;

        	$restaurant_cuisines = $this->restaurantcuisines;

        	$data = array();

        	$data[] = array(
        		'restaurant_id'=>$restaurant_id,
        		'cuisine_id'=>$cuisine_id
        	);

        	$check = $restaurant_cuisines->where('restaurant_id',$restaurant_id)->where('cuisine_id',$cuisine_id)->count();

        	if($check==0)
        	{

        		$restaurant_cuisines->insert($data);
        	}else
        	{
        		 return redirect('/admin/restaurant_cuisines')->with('error','Cuisine already exist');
        	}

        }

        return redirect('/admin/restaurant_cuisines')->with('success','Cuisine added Successfully');
	}


 public function restaurant_status_enable(Request $request)
    {
   
    $approve=$this->restaurantcuisines->where('id',$request->id)->update(['status'=>1]);

    return back()->with('success','Restaurant Enabled');

    }

        public function restaurant_status_disable(Request $request)
    {
   
    $approve=$this->restaurantcuisines->where('id',$request->id)->update(['status'=>0]);

    return back()->with('success','Restaurant Disabled');

    }

	public function delete_restaurant_cuisine(Request $request)
	{
		$validator = Validator::make($request->all(), [
                'cuisine_id' => 'required',
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('error', $error_messages);

        }else
        {
        	$cuisine_id = $request->cuisine_id;

        	$this->restaurantcuisines->where('id',$cuisine_id)->delete();
        }

        return redirect('/admin/restaurant_cuisines')->with('success','Cuisine Deleted Successfully');
	} 
}