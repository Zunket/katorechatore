<?php

namespace App\Http\Controllers\admin;
                                    
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController;
use Carbon\Carbon;
use App\Model\Requests;
use DB;

class OrderController extends BaseController
{
	public function neworder_list(Request $request,$status=null)
	{
		$mytime = Carbon::now()->format('Y-m-d');
        
		$restaurant_id = $request->session()->get('userid');

		$role = $request->session()->get('role');
        if($status == 7){

        	$data = DB::table('requests')->join('users','users.id','=','requests.user_id')->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
        	->join('restaurants','restaurants.id','=','requests.restaurant_id')
									 ->select('requests.id as request_id','requests.status as order_status','users.name as user_name','restaurants.rating','delivery_partners.name as delivery_boy_name','requests.*','users.*')->where('requests.status',7)
									 ->orderBy('request_id','desc')
									 ->get();
									 // print_r($data); exit;
			$data1 = DB::table('requests')->join('request_detail','request_detail.request_id','=','requests.id')->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
									 ->join('food_list','food_list.id','=','request_detail.food_id')
									 ->select('food_list.name as food_name','delivery_partners.name as delivery_boy_name','request_detail.*','food_list.*','requests.id')->where('requests.status',7)
									 ->get();
        }elseif($status == 10){

        	$data = DB::table('requests')->join('users','users.id','=','requests.user_id')->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
        	->join('restaurants','restaurants.id','=','requests.restaurant_id')
									 ->select('requests.id as request_id','requests.status as order_status','users.name as user_name','restaurants.rating','delivery_partners.name as delivery_boy_name','requests.*','users.*')->whereIn('requests.status',[10,9])
									 ->orderBy('request_id','desc')
									 ->get();
									 // print_r($data); exit;
			$data1 = DB::table('requests')->join('request_detail','request_detail.request_id','=','requests.id')->join('delivery_partners','delivery_partners.id','=','requests.delivery_boy_id')
									 ->join('food_list','food_list.id','=','request_detail.food_id')
									 ->select('food_list.name as food_name','delivery_partners.name as delivery_boy_name','request_detail.*','food_list.*','requests.id')->whereIn('requests.status',[10,9])
									 ->get();
        }else{

        	if($role==2)

			{

				$data = DB::table('requests')->where('requests.restaurant_id',$restaurant_id)
				->join('restaurants','restaurants.id','=','requests.restaurant_id')
										 ->join('users','users.id','=','requests.user_id')
										 ->select('requests.id as request_id','requests.status as order_status','users.name as user_name','restaurants.rating','requests.*','users.*')->where('requests.status','!=',7)
										 ->orderBy('request_id','desc')
										 ->get();
										 // print_r($data); exit;
				$data1 = DB::table('requests')->where('requests.restaurant_id',$restaurant_id)
										 ->join('request_detail','request_detail.request_id','=','requests.id')
										 ->join('food_list','food_list.id','=','request_detail.food_id')
										 ->select('food_list.name as food_name','request_detail.*','food_list.*','requests.id')
										 ->get();
			}else
			{



				$data = DB::table('requests')->join('users','users.id','=','requests.user_id')
				->join('restaurants','restaurants.id','=','requests.restaurant_id')
										 ->select('requests.id as request_id','requests.status as order_status','users.name as user_name','restaurants.rating','requests.*','users.*')->where('requests.status','!=',7)
										 ->orderBy('request_id','desc')
										 ->get();

										 // print_r($data); exit;
				$data1 = DB::table('requests')->join('request_detail','request_detail.request_id','=','requests.id')
										 ->join('food_list','food_list.id','=','request_detail.food_id')
										 ->select('food_list.name as food_name','request_detail.*','food_list.*','requests.id')
										 ->get();
			}
        }
		
		$total_order = DB::table('requests')->select('id')->count();
        $active_total_order = DB::table('requests')->where('status',7)->select('id')->count();
        $inactive_total_order = DB::table('requests')->whereIn('status',[10,9])->select('id')->count();

		return view('neworder_list',['data'=>$data,'data1'=>$data1,'total_order'=>$total_order,'active_total_order'=>$active_total_order,'inactive_total_order'=>$inactive_total_order]);
	}

	public function deliveries(Request $request)
	{
		
		$mytime = Carbon::now()->format('Y-m-d');
        
		$restaurant_id = $request->session()->get('userid');

		$role = $request->session()->get('role');
        
        $builder = new Requests;

        if($request->user_id){

        	$builder = $builder->where('requests.user_id',$request->user_id);
        }
        if($request->from_date && $request->to_date){
        
        $from = date('Y-m-d', strtotime($request->from_date));
        $to = date('Y-m-d', strtotime($request->to_date));

        $builder = $builder->whereBetween(DB::raw('DATE(requests.created_at)'), array($from, $to));
        } 

    	$data = $builder->join('users','users.id','=','requests.user_id')
				->join('restaurants','restaurants.id','=','requests.restaurant_id')
										 ->select('requests.id as request_id','requests.status as order_status','users.name as user_name','restaurants.rating','requests.*','users.*')
										 ->orderBy('request_id','desc')
										 ->get();

										 // print_r($data); exit;
				$data1 = DB::table('requests')->join('request_detail','request_detail.request_id','=','requests.id')
										 ->join('food_list','food_list.id','=','request_detail.food_id')
										 ->select('food_list.name as food_name','request_detail.*','food_list.*','requests.id')
										 ->get();
        
		
		$total_earnings = DB::table('requests')->sum('bill_amount');
        $total_restaurant_comission = DB::table('requests')->sum('restaurant_commision');
        $total_delivery_boy_comission = DB::table('requests')->sum('delivery_boy_commision');
        $total_admin_comission = DB::table('requests')->sum('admin_commision');
		return view('deliveries',['data'=>$data,'data1'=>$data1,'total_earnings'=>$total_earnings,'total_restaurant_comission'=>$total_restaurant_comission,'total_delivery_boy_comission'=>$total_delivery_boy_comission,'total_admin_comission'=>$total_admin_comission]);
	}

	public function accept_request($request_id,Request $request)
	{
		$restaurant_id = $request->session()->get('userid');

		$foodrequest = $this->foodrequest;
		$trackorderstatus = $this->trackorderstatus;

		$foodrequest->where('id',$request_id)->update(['status'=>1]);

		 $status_entry[] = array(
                'request_id'=>$request_id,
                'status'=>1,
                'detail'=>"Order Accepted by Restaurant"
            );

		  $trackorderstatus->insert($status_entry);
		  
		  $user_data = $this->foodrequest->where('id',$request_id)->first();

		  // to insert into firebase
			$postdata = array();
			$postdata['request_id'] = $request_id;
			//$postdata['provider_id'] = (String)$temp_driver;
			$postdata['user_id'] = (String)$user_data->user_id;
			$postdata['status'] = '1';
			$postdata = json_encode($postdata);
			$this->update_firebase($postdata, 'current_request', $request_id);

		return redirect()->back();

	}

	public function assign_request($request_id,Request $request)
	{
	    //dd('hi');
		$restaurant_id = $request->session()->get('userid');
		# code...
		$old_provider=0;

		$trackorderstatus = $this->trackorderstatus;

		$request_data = DB::table('requests')->where('id',$request_id)->first();
		

		$restuarant_detail = $this->restaurants->where('id',$restaurant_id)->first();

		$source_lat = $restuarant_detail->lat;
		$source_lng = $restuarant_detail->lng;
		

		$data = file_get_contents(env('FIREBASE_URL')."/available_providers/.json");
		//dd($data);
		$data = json_decode($data);

		//var_dump($data); exit;
		$temp_driver = 0;
		$last_distance = 0;
		if($data != NULL && $data !="")
		{
		foreach ($data as $key => $value) {
			# code...
			$driver_id = $key;
			if($old_provider==0){
				$old_provider = -1;
			}
			if($driver_id != $old_provider){
				if ($value != NULL && $value != ""){
				$driver_lat = $value->lat;
				$driver_lng = $value->lng;

				try {
					$q = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$source_lat,$source_lng&destinations=$driver_lat,$driver_lng&mode=driving&sensor=false&key=".GOOGLE_API_KEY;
					$json = file_get_contents($q); 
					$details = json_decode($json, TRUE);

				//	 dd($details); exit;
					if(isset($details['rows'][0]['elements'][0]['status']) && $details['rows'][0]['elements'][0]['status'] != 'ZERO_RESULTS'){
						$current_distance_with_unit = $details['rows'][0]['elements'][0]['distance']['text'];
						$current_distance = (float)$details['rows'][0]['elements'][0]['distance']['text'];
						$unit =str_after($current_distance_with_unit, ' ');
						if($unit == 'm')
						{
							$current_distance = $current_distance/1000;
						}
						if($current_distance<=DEFAULT_RADIUS){
							if($temp_driver == 0){
								$temp_driver = $driver_id;
								$last_distance = $current_distance;
							}else{
								if($current_distance < $last_distance){
									$temp_driver = $driver_id;
									$last_distance = $current_distance;
								}
							}
						}
					}

					} catch (Exception $e) {
						
						}
				}
			}
			
			//print_r($value->lat); exit;
			}
		}
		
		if ($temp_driver != 0 ) {
			# code...
			$ins_data = array();
			$user_data = DB::table('requests')
						->where('id',$request_id)
						->first();

			$data =DB::table('requests')->where('id',$request_id)->update(['delivery_boy_id'=>$temp_driver,'status'=>2]);
		    //dd($data);
			$check_status = $trackorderstatus->where('request_id',$request_id)->where('status',2)->count();
			if($check_status==0)
			{
			$status_entry[] = array(
                'request_id'=>$request_id,
                'status'=>2,
                'detail'=>"Food is being prepared"
            );

          $trackorderstatus->insert($status_entry);
      	}

			// to insert into firebase
			$postdata = array();
			$postdata['request_id'] = $request_id;
			$postdata['provider_id'] = (String)$temp_driver;
			$postdata['user_id'] = (string)$user_data->user_id;
			$postdata['status'] = '2';
			$postdata = json_encode($postdata);
			$this->update_firebase($postdata, 'current_request', $request_id);  

			// sending request to driver
			$postdata = array();
			$postdata['request_id'] = $request_id;
			$postdata['user_id'] = (String)$user_data->user_id;
			$postdata['status'] = '1';
			$postdata = json_encode($postdata);
			$this->update_firebase($postdata, 'new_request', $temp_driver); 

			//send push notification to user
			$provider = $this->deliverypartners->find($temp_driver);
			if(isset($provider->device_token) && $provider->device_token!='')
			{
				$title = $message = trans('constants.new_order');
				$this->user_send_push_notification($provider->device_token, $provider->device_type, $title, $message, $request_id);
			}
            

			// end of request to driver
			return redirect()->back();
		} else {
				# code...
				 $title = "No Providers available";
				// $message = array();
				// $message['title'] = "Taxi Request";
				// $message['body'] = "No Providers available";
				// $this->send_push_notification($request->header('authId'), $title, $message);

				return redirect()->back();
			}
			
			# code...	
	}

	/** 
	* to get order list based on status
	*
	* @param object $request, string $type
	*
	* @return view page with details
	*/
	public function order_list(Request $request, $type)
	{
			$restaurant_id = $request->session()->get('userid');
			$role = $request->session()->get('role');
			if($type=='new') $status = [0];
			if($type=='processing') $status = [1,2];	
			if($type=='pickup') $status = [3,4,5];
			if($type=='delivered') $status = [6,7];	
			if($type=='cancelled') $status = [9,10];
			if($role==2)
			{
				$data = $this->foodrequest->whereIn('status',$status)->where('restaurant_id',$restaurant_id)->orderBy('id','desc')->get();
			}else
			{
				$data = $this->foodrequest->whereIn('status',$status)->orderBy('id','desc')->get();
			}
			//dd($data);

			return view('orders_list',['data'=>$data,'title'=>$type]);
	}



	/**
	 * cancel the order request
	 * 
	 * @param int $request_id, object $request
	 * 
	 * @return return to blade page
	 */
	public function cancel_request($request_id,Request $request)
	{
			$role = $request->session()->get('role');

			$foodrequest = $this->foodrequest;
			$trackorderstatus = $this->trackorderstatus;
			if($role==1){
				$status = 9;
				$message = "Order Cancelled by Admin";
			}else{
				$status = 10;
				$message = "Order Cancelled by Restaurant";
			} 

			$foodrequest->where('id',$request_id)->update(['status'=>$status]);

			$status_entry[] = array(
									'request_id' => $request_id,
									'status' => $status,
									'detail' => $message
							);

			$trackorderstatus->insert($status_entry);
			return redirect('/admin/neworder_list');

	}


	public function order_dashboard(Request $request)
	{
		$today_orders = DB::table('requests')
		                ->whereDate('created_at', Carbon::today())
		                ->count();

		$today_completed_orders = DB::table('requests')
		                          ->whereDate('created_at', Carbon::today())
		                          ->where('status',7)
		                          ->count();

		$today_cancel_orders = DB::table('requests')
		                       ->whereDate('created_at', Carbon::today())
		                       ->where('status',10)
		                       ->count();

		$today_processing_orders = DB::table('requests')
		                       ->whereDate('created_at', Carbon::today())
		                       ->whereIn('status',[2,3,4,5,6])
		                       ->count();
           $restaurant_id = $request->session()->get('userid');
           $role = $request->session()->get('role');
		   $query = $this->foodrequest
                    ->orderby('id','desc')
                    ->limit(5);
           $query = $query->when(($role!=1),function($q)use($restaurant_id){
                    return $q->where('restaurant_id',$restaurant_id);
            });
                    
    $recent_orders = $query->get();

		// $recent_orders = DB::table('requests')
		//                      ->join('users','users.id','=','requests.user_id')
		//                      ->select('requests.*','users.name as name')
		//                      ->orderby('id','desc')
		//                      ->limit(5)
		//                      ->get();

		$area_wise_earnings = $this->foodrequest
		                      ->join('restaurants','restaurants.id','=','requests.restaurant_id')
		                      ->join('add_area','add_area.id','=','restaurants.area')
		                      
		                      ->groupBy('restaurants.area','requests.id','add_area.area')
		                      ->select('restaurants.area','requests.id','add_area.area as res_area')
                              ->get();

		$column=array();
        foreach ($area_wise_earnings as $key => $value) {
           
            $col['res_area']=isset($value->Restaurants->Area)?$value->Restaurants->Area->area:"";
            $col['id']=$value->id;
           
            array_push($column, $col);
        }

        $area_wise_earnings = $column;


        //print_r($area_wise_earnings);exit();

		//dd($area_wise_earnings[0]); 

		//print_r($area_wise_earnings);exit();

		return view('order_dashboard',['today_orders'=>$today_orders,'today_completed_orders'=>$today_completed_orders,'today_cancel_orders'=>$today_cancel_orders,'today_processing_orders'=>$today_processing_orders,'recent_orders'=>$recent_orders,'area_wise_earnings'=>$area_wise_earnings,]);                
	}

	/**
	 * View the order request
	 * 
	 * @param int $request_id, object $request
	 * 
	 * @return return to blade page
	 */
	public function view_order($request_id,Request $request)
	{
		$data = $this->foodrequest->where('id',$request_id)
								  ->with('Restaurants.city_list')
								  ->with('Restaurants.Area')
								  ->first();
		//dd($data);
		return view('view_order',compact('data'));
	}

}