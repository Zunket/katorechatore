<?php

namespace App\Http\Controllers\admin;
                                    
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController;
use DB;

class UserController extends BaseController
{
	public function user_list(Request $request)
	{
		$user_detail = $this->users->get();

		// print_r($user_detail); exit;

		return view('user_list',['user_detail'=>$user_detail]);
	}
	
	public function Adduser(Request $request)
	{
		return view('add_user');
	}

	public function edit_user($id)
	{
	    
		$data = $this->users->where('id',$id)->first();
		
        /*$data->image = BASE_URL.UPLOADS_PROFILE_PATH.$data->image;*/

		return view('edit_user',['data'=>$data]);
	}

	public function user_add(Request $request){

        $custom = $this->custom;
		$update =  $this->users->find($request->id);
        $update->name = $request->name;
	    $update->phone = $request->phone;
	    $update->email = $request->email;

	    if ($request->profile_image) {

            if ($update->profile_image != "") {
                $custom::delete_image($update->profile_image);
            }

            $profile_pic = $custom::upload_image($request,'profile_image');

            $update['profile_image'] = BASE_URL.UPLOADS_PROFILE_PATH.$profile_pic;
            
        }
       
	 
        $update->save();


        return redirect('/admin/user_list')->with('success','User Updated Successfully');  
	}

	public function delete_user($id)
	{
		

		$user = $this->users->where('id',$id)->delete();

		return redirect('/admin/user_list')->with('success','User Deleted Successfully');
	}
}
