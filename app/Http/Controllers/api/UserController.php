<?php

namespace App\Http\Controllers\api;
                                    
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController;
use DB;
use Carbon\Carbon;
use App\Model\Plan;
use App\Model\Subscribtion;
use App\Model\SubscribtionPayment;


class UserController extends BaseController
{

    public function plans(Request $request){
        $subscriberData = $this->planList($request);
        if(count($subscriberData)!=0)
        {
            return $response_array = array('status' => true, 'data' => $subscriberData);
        }else
        {
            return $response_array = array('status' => false, 'message' => 'No plan found');
        }
    }

    public function planList($request){
        $response = Plan::where('status',1)->get();
        $subscriberData=[];
        foreach ($response as $res) {
            $subscriber = Subscribtion::where(['user_id'=>$request->header('authId'),'plan_id'=>$res->id])->first();
            $Data['id'] = $res->id ;
            $Data['name'] = $res->name ;
            $Data['meals'] = $res->meals ;
            $Data['img'] = $res->img;
            $Data['price'] = $res->price ;
            $Data['description'] = $res->description ;
            $Data['isSubscribed'] = count($subscriber)>0?1:0;
            array_push($subscriberData,$Data);
        }
        return $subscriberData;
    }
    public function subscribe(Request $request)
    {
           $validator = Validator::make(
                $request->all(),
                array(
                    'plan_id' => 'required',
                    'payment_type' => 'required',
                    'transaction_id' => 'required'       
                ));
        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
        }else
        {
            if($request->header('authId')!="")
            {
                $user_id = $request->header('authId');
            }else
            {
                $user_id = $request->authId;
            }
            $subscriberData = $this->planList($request);
            $plan = Plan::find($request->plan_id);
            $expirydate = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$plan->validity.' days')); 
            $subscriber = new Subscribtion;
            $subscriber->user_id = $user_id;
            $subscriber->plan_id = $request->plan_id;
            $subscriber->days_left = $plan->validity;
            $subscriber->expiry_date = $expirydate ;
            $subscriber->save();

            $subscriberPayment  = new SubscribtionPayment;
            $subscriberPayment->amount = $plan->price;
            $subscriberPayment->payment_type = $request->payment_type;
            $subscriberPayment->transaction_id = $request->transaction_id;
            
            if($subscriber->payment()->save($subscriberPayment)){
                $response_array = array('status' => true,'message'=>'Subscribed successfully', 'plans' =>$subscriberData);  
                $response = response()->json($response_array, 200);
            }else{
                $response_array = array('status' => false, 'message' => 'Something went wrong');
                $response = response()->json($response_array, 500);  
            }
            return $response;
        }
    }

    public function get_default_address(Request $request)
    {
        if($request->header('authId')!="")
        {
            $user_id = $request->header('authId');
        }else
        {
            $user_id = $request->authId;
        }
        // $user_id = $request->header('authId');
        $delivery_address = $this->deliveryaddress;

        $data = $delivery_address::where('user_id',$user_id)->where('is_default',1)->first();

        if(count($data)!=0)
        {
            $response_array = array('status' => true, 'data' => $data);
        }else
        {
            $response_array = array('status' => false, 'message' => 'No address found');
        }        

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function set_delivery_address(Request $request)
    {
         if($request->header('authId')!="")
        {
            $user_id = $request->header('authId');
        }else
        {
            $user_id = $request->authId;
        }
        // $user_id = $request->header('authId');
        $delivery_address = $this->deliveryaddress;

        if($request->current_address)
        {
            $current_address = $request->current_address;
            $lat = $request->lat;
            $lng = $request->lng;

            $data1 =  $delivery_address::where('user_id',$user_id)->where('address','like','%'.$current_address.'%')->first();

            if(count($data1)!=0)
            {
                // $delivery_address::where('user_id',$user_id)->where('address','like','%'.$current_address.'%')->delete();
                $delivery_address::where('user_id',$user_id)->where('is_default',1)->update(['is_default'=>0]);
                $delivery_address::where('id',$data1->id)->update(['is_default'=>1]);
            }else
            {
                $delivery_address::where('user_id',$user_id)->where('is_default',1)->update(['is_default'=>0]);

                $data = array();

                    $data[] = array(
                        'user_id'=>$user_id,
                        'address'=>$current_address,
                        'lat'=>$lat,
                        'lng'=>$lng,
                        'is_default'=>1,
                        'type'=>1
                    );

                    $delivery_address::insert($data);
            }

            

            $current_delivery_address = $delivery_address::where('user_id',$user_id)->where('is_default',1)->first();

        }else
        {
            $current_delivery_address = $delivery_address::where('user_id',$user_id)->where('is_default',1)->first();
        }

        if(count($current_delivery_address)!=0)
        {
            $response_array = array('status' => true, 'data' => $current_delivery_address);
        }else
        {
            $response_array = array('status' => false, 'message' => 'No address found');
        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function get_delivery_address(Request $request)
    {
         if($request->header('authId')!="")
        {
            $user_id = $request->header('authId');
        }else
        {
            $user_id = $request->authId;
        }
        // $user_id = $request->header('authId');
        $delivery_address = $this->deliveryaddress;
        $data = $delivery_address::where('user_id',$user_id)->get();

        if(count($data)!=0)
        {
            return response()->json(['status'=>true,'data'=>$data]);    // type - 1 home, 2 work, 3 others
        }else
        {
            return response()->json(['status'=>false,'message'=>'No address found']);
        }


    }

    public function add_delivery_address(Request $request)
    {
           $validator = Validator::make(
                $request->all(),
                array(
                    'address' => 'required',
                    'lat' => 'required',
                    'lng' => 'required',
                    'type' => 'required',        // Type -1 Home, 2- Office, 3 -Others  
                    'landmark' => 'required',
                    'flat_no' => 'required'        
                ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
        }else
        {
            if($request->header('authId')!="")
            {
                $user_id = $request->header('authId');
            }else
            {
                $user_id = $request->authId;
            }
            // $user_id = $request->header('authId');
            $address = $request->address;
            $lat = $request->lat;
            $lng = $request->lng;
            $type = $request->type;
            $flat_no = $request->flat_no;
            $landmark = $request->landmark;
            $delivery_address = $this->deliveryaddress;

            $check_for_default_address = $delivery_address::where('user_id',$user_id)->where('is_default',1)->get();

            if(count($check_for_default_address)!=0)
            {
                $is_default = 0;
            }else
            {
                $is_default = 1;
            }

            $insert_data = array();

            $insert_data[] = array(
                'user_id'=>$user_id,
                'address'=>$address,
                'lat'=>$lat,
                'lng'=>$lng,
                'type'=>$type,
                'flat_no'=>$flat_no,
                'landmark'=>$landmark,
                'is_default'=>$is_default
            );

            $delivery_address::insert($insert_data);

            $response_array = array('status' => true, 'message' => 'Address added successfully');

        }

         $response = response()->json($response_array, 200);
        return $response;
    }

    public function get_filter_list($filter_type)
    {
        if($filter_type ==1)    // filter_type =1 - Cusines table else relevance table
        {
            $cuisines = $this->cuisines;
            $data = $cuisines::get();
        }else
        {
            $data = DB::table('relevance')->get();
        } 

        if(count($data)!=0)
        {
            return response()->json(['status'=>true,'data'=>$data]);
        }else
        {
            return response()->json(['status'=>false,'message'=>'No data found']);
        }  
    }

    public function get_banners(Request $request)
    {
        $data = DB::table('offers_banner')->where('status',1)->orderBy('position','asc')->get();


        if(count($data)!=0)
        {
             return response()->json(['status'=>true,'data'=>$data, 'base_url'=>BASE_URL.UPLOADS_PATH]);
         }else
         {
            return response()->json(['status'=>false,'message'=>'No data found']);
         }
    }

     public function get_relevance_restaurant(Request $request)
    {

        $validator = Validator::make(
                $request->all(),
                array(
                    'is_pureveg' => 'required',
                    'is_offer' => 'required',
                    'lat' => 'required',
                    'lng' => 'required'
                   
                ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
        }else
        {
             if($request->header('authId')!="")
            {
                $user_id = $request->header('authId');
            }else
            {
                $user_id = $request->authId;
            }
            $restaurants = $this->restaurants;
           
            $size_cuisines = sizeof($request->cuisines);
            // echo $size_cuisines; exit;
                                                                
            $source_lat = $request->lat;
            $source_lng = $request->lng;
            $cuisines = $request->cuisines;

            $query = $restaurants->with('Cuisines')->where('status',1)
                    ->select('restaurants.*')
                    ->selectRaw("(6371 * acos(cos(radians(" . $source_lat . "))* cos(radians(`lat`)) 
                            * cos(radians(`lng`) - radians(" . $source_lng . ")) + sin(radians(" . $source_lat . ")) 
                            * sin(radians(`lat`)))) as distance")
                    // ->having('distance','<=',DEFAULT_RADIUS)
                    ->orderBy('distance');
                
            $query = $query->when(($request->is_offer==1),
                        function($q){
                            return $q->where('offer_amount','!=',0);
                        });

            $query = $query->when(($size_cuisines!=0),
                        function($q) use($cuisines){
                            $q->wherehas('Cuisines',function($q) use($cuisines){
                                return $q->whereIn('cuisines.id', $cuisines);
                            });
                        });
            
            // $query = $query->when(($request->is_pureveg==1),
            //             function($q){
            //                 return $q->where('offer_amount','!=',0);
            //             });
                    
            $data = $query->get();

            $restaurant_list = array();
            foreach($data as $d)
            {
              
                $rcuisines = array();
                $i=0;
                foreach($d->Cuisines as $r_cuisines)
                {
                    if($i<2)
                    {
                        $rcuisines[] = array(
                            'name' => $r_cuisines->name
                        );  
                        $i =$i+1;
                    }
                }
                   
                $check_favourite = DB::table('favourite_list')->where('user_id',$user_id)->where('restaurant_id',$d->id)->get();
                if(count($check_favourite)!=0)
                {
                    $is_favourite = 1;
                }else
                {
                    $is_favourite = 0;
                }
                //calculate restaurant open time
                $is_open = 0;
                $current_time = date('H:i:s');
                $data = date("Y-m-d");
                $is_weekend = $this->isWeekend($data);
                if($is_weekend==true){ 
                     if((strtotime($d->weekend_opening_time)<=strtotime($current_time)) && (strtotime($d->weekend_closing_time)>=strtotime($current_time))){
                         $is_open = 1;
                     }
                }else{ 
                     if((strtotime($d->opening_time)<=strtotime($current_time)) && (strtotime($d->closing_time)>=strtotime($current_time))){
                         $is_open = 1;  
                     }
                }
                //check restaurant offer
                $restaurant_offer = "";
                if($d->offer_amount!=''){
                    if($d->discount_type==1){
                            $restaurant_offer = "Flat offer ".DEFAULT_CURRENCY_SYMBOL." ".$d->offer_amount;
                    }else{
                            $restaurant_offer = $d->offer_amount."% offer";
                    }
                    if($d->target_amount!=0){
                            $restaurant_offer = $restaurant_offer." on orders above ".DEFAULT_CURRENCY_SYMBOL." ".$d->target_amount;
                    }
                }
                 $count = $this->review->where('restaurant_id',$d->id)->count();
                $rating_sum = $this->review->where('restaurant_id',$d->id)->sum('rating');
                $c = $count * 5;
                if($c !=''){
                    $rating = (@$rating_sum * 5) / @$c;
                }else{
                    $rating=0;
                }
                if(sizeof($rcuisines)>0)
                {
                    $restaurant_list[] = array(
                        'id'        =>$d->id,
                        'name'      => $d->restaurant_name,
                        'image'     => BASE_URL.RESTAURANT_UPLOADS_PATH.$d->image,
                        'discount'  => $d->discount,
                        'rating'    => bcdiv($rating, 1, 1),
                        'is_open'   => $is_open,     // 1- Open , 0 - Close
                        'cuisines'  => $rcuisines,
                        'travel_time' => $d->estimated_delivery_time,
                        'price'     => $restaurant_offer,
                        'is_favourite'=>$is_favourite,
                        );
                }

            }

            if(sizeof($restaurant_list)>0)
            {

                $response_array = array('status'=>true,'restaurants'=>$restaurant_list);
            }else
            {
                $response_array = array('status'=>false,'message'=>'No Data Found');
            }
        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function get_menu(Request $request)
    {
        $validator = Validator::make(
                $request->all(),
                array(
                    'restaurant_id' => 'required'
                ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
        }else
        {
            $menu = $this->menu;
            $foodlist = $this->foodlist;
            $restaurant_id = $request->restaurant_id;
               $menu_list=array();

                   $check = $menu::where('restaurant_id',$restaurant_id)->where('status',1)->get();

                    foreach($check as $c)
                    {
                        $food_count = $foodlist::where('menu_id',$c->id)->where('status',1)->count();

                        $menu_list[] = array(
                            'menu_id'   => $c->id,
                            'menu_name' => $c->menu_name,
                            'food_count'=>$food_count,
                        );
                    }

                    $response_array = array('status'=>true,'menus'=>$menu_list);
        }

          $response = response()->json($response_array, 200);
        return $response;
    }

    public function get_nearby_restaurant(Request $request)
    {
        $validator = Validator::make(
                $request->all(),
                array(
                    'lat' => 'required',
                    'lng' => 'required',
                
                ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
        }else
        {
             if($request->header('authId')!="")
            {
                $user_id = $request->header('authId');
            }else
            {
                $user_id = $request->authId;
            }

            $source_lat = $request->lat;
            $source_lng = $request->lng;
            $type = $request->serviceType;

            $restaurants = $this->restaurants;

            $size_cuisines = isset($request->cuisines)?sizeof($request->cuisines):0;
            $cuisines = isset($request->cuisines)?$request->cuisines:'';
                                                               
            //geofencing for restaurant
            $query = $restaurants->with('Cuisines')->where('status',1)
                    ->select('restaurants.*')
                    ->selectRaw("(6371 * acos(cos(radians(" . $source_lat . "))* cos(radians(`lat`)) 
                            * cos(radians(`lng`) - radians(" . $source_lng . ")) + sin(radians(" . $source_lat . ")) 
                            * sin(radians(`lat`)))) as distance")
                    // ->having('distance','<=',DEFAULT_RADIUS)
                    ->orderBy('distance');

            $query = $query->when(($request->is_offer==1),
                        function($q){
                            return $q->where('offer_amount','!=',0);
                        });
            $query = $query->when(($type=='is_vip'),
                        function($q){
                            return $q->where('is_vip','=',1);
                        });
            $query = $query->when(($type=='is_home'),
                        function($q){
                            return $q->where('is_home_service','=',1);
                        });
            $query = $query->when(($type=='is_24x7'),
                        function($q){
                            return $q->where('is_24x7','=',1);
                        });

            $query = $query->when(($size_cuisines!=0),
                        function($q) use($cuisines){
                            $q->wherehas('Cuisines',function($q) use($cuisines){
                                return $q->whereIn('cuisines.id', $cuisines);
                            });
                        });

            $data = $query->get();

            $restaurant_list = array();
            $j=0;  
            foreach($data as $d)
            {
                // if($j<2)  // To display only two restaurants
                // {
                        $rcuisines = array();
                        $i=0;   
                        foreach($d->Cuisines as $r_cuisines)
                        {
                            if($i<2) // To display only two cuisines
                            {
                                $rcuisines[] = array(
                                    'name' => $r_cuisines->name
                                );
                                $i =$i+1;
                            }
                        }
                       
                       $check_favourite = DB::table('favourite_list')->where('user_id',$user_id)->where('restaurant_id',$d->id)->get();
                       if(count($check_favourite)!=0)
                       {
                            $is_favourite = 1;
                       }else
                       {
                            $is_favourite = 0;
                       }

                       //calculate restaurant open time
                       $is_open = 0;
                       $current_time = date('H:i:s');
                       $data = date("Y-m-d");
                       $is_weekend = $this->isWeekend($data);
                       if($is_weekend==true){ 
                            if((strtotime($d->weekend_opening_time)<=strtotime($current_time)) && (strtotime($d->weekend_closing_time)>=strtotime($current_time))){
                                $is_open = 1;
                            }
                       }else{ 
                            if((strtotime($d->opening_time)<=strtotime($current_time)) && (strtotime($d->closing_time)>=strtotime($current_time))){
                                $is_open = 1;  
                            }
                       }
                       //check restaurant offer
                        $restaurant_offer = "";
                        if($d->offer_amount!=''){
                            if($d->discount_type==1){
                                    $restaurant_offer = "Flat offer ".DEFAULT_CURRENCY_SYMBOL." ".$d->offer_amount;
                            }else{
                                    $restaurant_offer = $d->offer_amount."% offer";
                            }
                            if($d->target_amount!=0){
                                    $restaurant_offer = $restaurant_offer." on orders above ".DEFAULT_CURRENCY_SYMBOL." ".$d->target_amount;
                            }
                        }
                        $count = $this->review->where('restaurant_id',$d->id)->count();
                        $rating_sum = $this->review->where('restaurant_id',$d->id)->sum('rating');
                        $c = $count * 5;
                        if($c !=''){
                            $rating = (@$rating_sum * 5) / @$c;
                        }else{
                            $rating=0;
                        }
                       $restaurant_list[] = array(
                            'id'        =>$d->id,
                            'name'      => $d->restaurant_name,
                            'image'     => BASE_URL.RESTAURANT_UPLOADS_PATH.$d->image,
                            'discount'  => $d->discount,
                            'rating'    => bcdiv($rating, 1, 1),
                            'is_open'   => $is_open,     // 1- Open , 0 - Close
                            'cuisines'  => $rcuisines,
                            'travel_time' => $d->estimated_delivery_time,
                            'price'     => $restaurant_offer,
                            'is_favourite'=>$is_favourite,
                            'address'=>$d->address
                            );
                    $j++;
               
                // }
            }

            if(count($restaurant_list)!=0)
            {
                $response_array = array('status'=>true,'restaurants'=>$restaurant_list);
            }else
            {
                $response_array = array('status'=>false,'message'=>__('constants.no_restaurant'));
            }


        }

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function get_popular_brands(Request $request)
    {
        $popularbrands = $this->popularbrands;

        $data = $popularbrands::get();

        if(count($data)!=0)
        {
            $response_array = array('status'=>true,'data'=>$data, 'base_url'=>BASE_URL);
        }else
        {
            $response_array = array('status'=>false,'message'=>'No Data Found');
        }

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function get_favourite_list(Request $request)
    {
         if($request->header('authId')!="")
            {
                $user_id = $request->header('authId');
            }else
            {
                $user_id = $request->authId;
            }
        // $user_id = $request->header('authId');
        $favouritelist = $this->favouritelist;
        $restaurants = $this->restaurants;
        $data = $favouritelist::where('user_id',$user_id)->get();
        // print_r($data); exit;
        $restaurant_list=array();
        if(count($data)!=0)
        {
            foreach($data as $key)
            {

                $restaurant_detail = $restaurants::where('id',$key->restaurant_id)->where('status',1)->first();
if(isset($restaurant_detail->id)){
               $restaurant_cuisines = DB::table('restaurant_cuisines')->join('cuisines','cuisines.id','=','restaurant_cuisines.cuisine_id')
                                                           ->join('restaurants','restaurants.id','=','restaurant_cuisines.restaurant_id')
                                                           ->select('restaurant_cuisines.restaurant_id as restaurant_id','cuisines.name as cuisine_name','restaurants.restaurant_name as restaurant_name')
                                                           ->where('restaurants.id','=',$key->restaurant_id)
                                                           ->get();

                          $rcuisines = array();
                        $i=0;   
                        foreach($restaurant_cuisines as $r_cuisines)
                        {
                            
                            if($restaurant_detail->restaurant_name == $r_cuisines->restaurant_name && $i<2) // To display only two cuisines
                            {
                            
                                    $rcuisines[] = array(
                                       'name' => $r_cuisines->cuisine_name
                                    );

                                  $i =$i+1;
                           
                            }

                        }
                
                
                    //calculate restaurant open time
                    $is_open = 0;
                    $current_time = date('H:i:s');
                    $data = date("Y-m-d");
                    $is_weekend = $this->isWeekend($data);
                    if($is_weekend==true){ 
                         if((strtotime($restaurant_detail->weekend_opening_time)<=strtotime($current_time)) && (strtotime($restaurant_detail->weekend_closing_time)>=strtotime($current_time))){
                             $is_open = 1;
                         }
                    }else{ 
                         if((strtotime($restaurant_detail->opening_time)<=strtotime($current_time)) && (strtotime($restaurant_detail->closing_time)>=strtotime($current_time))){
                             $is_open = 1;  
                         }
                    }
                    //check restaurant offer
                    $restaurant_offer = "";
                    if($restaurant_detail->offer_amount!=''){
                        if($restaurant_detail->discount_type==1){
                                $restaurant_offer = "Flat offer ".DEFAULT_CURRENCY_SYMBOL." ".$restaurant_detail->offer_amount;
                        }else{
                                $restaurant_offer = $restaurant_detail->offer_amount."% offer";
                        }
                        if($restaurant_detail->target_amount!=0){
                                $restaurant_offer = $restaurant_offer." on orders above ".DEFAULT_CURRENCY_SYMBOL." ".$restaurant_detail->target_amount;
                        }
                    }
                    $restaurant_list[] = array(
                            'restaurant_id' => $key->restaurant_id,
                            'name'      => $restaurant_detail->restaurant_name,
                            'image'     => BASE_URL.RESTAURANT_UPLOADS_PATH.$restaurant_detail->image,
                            'discount'  => $restaurant_detail->discount,
                            'rating'    => $restaurant_detail->rating,
                            'is_open'   => $is_open,     // 1- Open , 0 - Close
                            'travel_time' => $restaurant_detail->estimated_delivery_time,
                            'price'     => $restaurant_offer,
                            'address'   => $restaurant_detail->address,
                            'is_favourite' => 1,
                            'cuisines'  => $rcuisines
                            );
                }
            }

             $response_array = array('status' => true,'favourite_list'=>$restaurant_list);

        }else
        {
            $response_array = array('status' => false,'message'=>'No favourite restaurants found');
        }
        
        $response = response()->json($response_array, 200);
        return $response;
    }

    public function update_favourite(Request $request)
    {
        if($request->header('authId')!="")
        {
            $user_id = $request->header('authId');
        }else
        {
            $user_id = $request->authId;
        }
        // $user_id = $request->header('authId');

          $validator = Validator::make(
                $request->all(),
                array(
                    'restaurant_id' => 'required'
                ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);

        }else
        {
            $restaurant_id = $request->restaurant_id;

            $favouritelist = $this->favouritelist;

            $check = $favouritelist::where('user_id',$user_id)->where('restaurant_id',$restaurant_id)->first();

            if(count($check)!=0)
            {
                $favouritelist::where('id',$check->id)->delete();

                $response_array = array('status' => true,'message'=>'Removed from Favourites');
            }else
            {
                $data = array();
                $data['user_id'] = $user_id;
                $data['restaurant_id'] = $restaurant_id;
                $favouritelist::insert($data);

                $response_array = array('status' => true,'message'=>'Added to Favourites');
            }

        }

                 $response = response()->json($response_array, 200);
                return $response;

    }

    public function get_current_order_status(Request $request)
    {
        if($request->header('authId')!="")
        {
            $user_id = $request->header('authId');
        }else
        {
            $user_id = $request->authId;
        }
        // $user_id = $request->header('authId');

        $check_request = $this->foodrequest::where('user_id',$user_id)->where('status','!=',10)->where('status','!=',9)->where('is_rated','=',0)->orderBy('id','desc')->get();
//dd(count($check_request));
        if(count($check_request)!=0)
        {
        
            $order_status = array();
            foreach($check_request as $key)
            {

                $restaurant_detail = $this->restaurants::where('id',$key->restaurant_id)->where('status',1)->first();

                if(count($restaurant_detail)!=0)
                {
                        $item_list = DB::table('request_detail')->where('request_id',$key->id)->get();
                       
                        $item_count = 0;

                    if($key->delivery_boy_id!=0)
                    {
                        $delivery_boy_id = $key->delivery_boy_id;
                        // echo $delivery_boy_id; exit;
                        $delivery_boy_detail = DB::table('delivery_partners')->where('id',$delivery_boy_id)->first();

                        $delivery_boy_name = $delivery_boy_detail->name;

                        $delivery_boy_image = $delivery_boy_detail->profile_pic;

                        $delivery_boy_phone = $delivery_boy_detail->phone;
                    }else
                    {
                        $delivery_boy_id = 0;
                        $delivery_boy_name="";
                        $delivery_boy_image="";
                        $delivery_boy_phone="";
                    }

                    $get_item_lists = array();
                    foreach ($item_list as $list) {

                        $item_count = $item_count + $list->quantity;
                        $food_detail = DB::table('food_list')->where('id',$list->food_id)->where('status',1)->first();
                        $get_item_lists[]=array(
                            'item_name'=>$food_detail->name,
                            'item_quantity'=>$list->quantity,
                            'price'=>$food_detail->price * $list->quantity
                        );

                    }

                    $order_status[] = array(
                        'request_id'=>$key->id,
                        'order_id'=>$key->order_id,
                        'is_rated'=>$key->is_rated,
                        'ordered_time'=>$key->ordered_time,
                        'restaurant_name'=>$restaurant_detail->restaurant_name,
                        'restaurant_image'=>$restaurant_detail->image,
                        'item_count'=>$item_count,
                        'bill_amount'=>$key->bill_amount,
                        'status'=>$key->status,
                        'delivery_boy_id'=>$delivery_boy_id,
                        'delivery_boy_image'=>$delivery_boy_image,
                        'delivery_boy_phone'=>$delivery_boy_phone,
                        'item_list'=>$get_item_lists

                    );
                }
            }

            $response_array = array('status' => true,'order_status'=>$order_status);

        }else
        {
            $response_array = array('status' => true,'message'=>'No orders in processing');
        }

        $response = response()->json($response_array, 200);
                return $response;
    }

    public function track_order_detail(Request $request)
    {
        //  if($request->header('authId')!="")
        //     {
        //         $user_id = $request->header('authId');
        //     }else
        //     {
        //         $user_id = $request->authId;
        //     }
            $user_id = $request->header('authId') ?: $request->authId;


                $validator = Validator::make(
                $request->all(),
                array(
                    'request_id' => 'required'
                ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);

        }else
        {
             $request_id = $request->request_id;
             $trackorderstatus = $this->trackorderstatus;


        $check_request = $this->foodrequest::where('user_id',$user_id)->where('id',$request_id)->first();
        $restaurant_detail= DB::table('restaurants')->where('id',$check_request->restaurant_id)->where('status',1)->first();

            $order_status = array();

                   $item_list = DB::table('request_detail')->where('request_id',$request_id)->get();


                    $item_count = 0;

                if($check_request->delivery_boy_id!=0)
                {
                    $delivery_boy_id = $check_request->delivery_boy_id;

                    $delivery_boy_detail = DB::table('delivery_partners')->where('id',$delivery_boy_id)->first();

                    $delivery_boy_name = $delivery_boy_detail->name;

                    $delivery_boy_image = $delivery_boy_detail->profile_pic;

                    $delivery_boy_phone = $delivery_boy_detail->phone;
                }else
                {
                    $delivery_boy_id = 0;
                    $delivery_boy_name="";
                    $delivery_boy_image="";
                    $delivery_boy_phone="";
                }

                foreach ($item_list as $list) {
                    $item_count = $item_count + $list->quantity;
                }

                $order_status[] = array(
                    'request_id'=>$request_id,
                    'order_id'=>$check_request->order_id,
                    'ordered_time'=>$check_request->ordered_time,
                    'restaurant_name'=>$restaurant_detail->restaurant_name,
                    'restaurant_image'=>BASE_URL.RESTAURANT_UPLOADS_PATH.$restaurant_detail->image,
                    'item_count'=>$item_count,
                    'item_total'=>$check_request->item_total,
                    'offer_discount'=>$check_request->offer_discount,
                    'restaurant_packaging_charge'=>$check_request->restaurant_packaging_charge,
                    'tax'=>$check_request->tax,
                    'delivery_charge'=>$check_request->delivery_charge,
                    'bill_amount'=>$check_request->bill_amount,
                    'status'=>$check_request->status,
                    'delivery_boy_id'=>$delivery_boy_id,
                    'delivery_boy_name'=>$delivery_boy_name,
                    'delivery_boy_image'=>$delivery_boy_image,
                    'delivery_boy_phone'=>$delivery_boy_phone,
                    'restaurant_lat'=>$restaurant_detail->lat,
                    'restaurant_lng'=>$restaurant_detail->lng,
                    'user_lat'=>$check_request->d_lat,
                    'user_lng'=>$check_request->d_lng

                );

                $tracking_detail = $trackorderstatus::where('request_id',$request_id)->get();
    

            $response_array = array('status' => true,'order_status'=>$order_status,'tracking_detail'=>$tracking_detail);

        }

        $response = response()->json($response_array, 200);
                return $response;

    }

    public function order_history(Request $request)
    {
        if($request->header('authId')!="")
        {
            $user_id = $request->header('authId');
        }else
        {
            $user_id = $request->authId;
        }
        
        $orders = DB::table('requests')->where('requests.user_id',$user_id)->latest()->limit(10)->get();

        $order_list = array();
        
                foreach($orders as $key)
                {
                    $order_detail = $this->requestdetail->where('request_id',$key->id)->get();
                    
                    $order_list_detail = array();
                    foreach($order_detail as $k)
                    {
                        if(isset($k->FoodQuantity)) $k->FoodQuantity->price = $k->food_quantity_price;
                        $order_list_detail[] = array(
                            'food_id'=>(!empty($k->Foodlist)?$k->Foodlist->id:""),
                            'food_name'=>(!empty($k->Foodlist)?$k->Foodlist->name:""),
                            'food_quantity'=>$k->quantity,
                            'tax' => (!empty($k->Foodlist)?$k->Foodlist->tax:""),
                            'item_price'=>(!empty($k->Foodlist)?$k->Foodlist->price:0) * $k->quantity,
                            'is_veg'=>(!empty($k->Foodlist)?$k->Foodlist->is_veg:""),
                            'food_size'=>$k->FoodQuantity,
                            'add_ons' => $k->Addons
                        );
                    }
                    
                    //$restaurant_detail = DB::table('restaurants')->where('id',$key->restaurant_id)->where('status',1)->first();
                    $restaurant_detail = DB::table('restaurants')->where('id',$key->restaurant_id)->first();
                    $review = $this->review->where(['order_id'=>$key->id])->orderBy('id','desc')->first();
                    if(!empty($review)){
                        $reviews = $review;
                    }else{
                        $reviews = null;
                    }
                    //if (isset($request_detail->id)) {
                        $order_list[] = array(
                            'request_id'=>$key->id,
                            'order_id'=>$key->order_id,
                            'restaurant_id'=>isset($restaurant_detail->id)?$restaurant_detail->id:$key->restaurant_id,
                            'restaurant_name'=>isset($restaurant_detail->restaurant_name)?$restaurant_detail->restaurant_name:"",
                            'restaurant_image'=>isset($restaurant_detail->image)?BASE_URL.RESTAURANT_UPLOADS_PATH.$restaurant_detail->image:"",
                            'ordered_on'=>$key->ordered_time,
                            'bill_amount'=>$key->bill_amount,
                            'item_list'=>$order_list_detail,
                            'item_total'=>$key->item_total,
                            'offer_discount'=>$key->offer_discount,
                            'restaurant_packaging_charge'=>$key->restaurant_packaging_charge,
                            'tax'=>$key->tax,
                            'is_rated'=>isset($key->is_rated)?$key->is_rated:0,
                            'delivery_charge'=>$key->delivery_charge,
                            'delivery_address'=>$key->delivery_address,
                            'restaurant_address'=>isset($restaurant_detail->address)?$restaurant_detail->address:"",
                            'reviews' => $reviews,
                            'status'=>$key->status,
                        );
                    //}
                    

                }

        $upcoming_orders = DB::table('requests')->where('requests.user_id',$user_id)->where('requests.status','!=',10)->where('requests.status','!=',9)->where('requests.status','!=',7)->get();

        $upcoming_order_list = array();
        
                foreach($upcoming_orders as $key)
                {
                    $upcoming_order_detail = $this->requestdetail->where('request_id',$key->id)->get();
                    $upcoming_order_list_detail = array();
                    foreach($upcoming_order_detail as $k)
                    {
                        if(isset($k->FoodQuantity)) $k->FoodQuantity->price = $k->food_quantity_price;
                        $upcoming_order_list_detail[] = array(
                            'food_id'=>(!empty($k->Foodlist)?$k->Foodlist->id:""),
                            'food_name'=>(!empty($k->Foodlist)?$k->Foodlist->name:""),
                            'food_quantity'=>$k->quantity,
                            'tax' => (!empty($k->Foodlist)?$k->Foodlist->tax:""),
                            'item_price'=>(!empty($k->Foodlist)?$k->Foodlist->price:0) * $k->quantity,
                            'is_veg'=>(!empty($k->Foodlist)?$k->Foodlist->is_veg:""),
                            'food_size'=>$k->FoodQuantity,
                            'add_ons' => $k->Addons
                        );
                    }

                    $restaurant_details = DB::table('restaurants')->where('id',$key->restaurant_id)->where('status',1)->first();
                    //dd($restaurant_details->restaurant_name);
                    if (isset($restaurant_details->id)) {
                        $upcoming_order_list[] = array(
                            'request_id'=>$key->id,
                            'order_id'=>$key->order_id,
                            'restaurant_id'=>isset($restaurant_details->id)?$restaurant_details->id:$key->restaurant_id,
                            'restaurant_name'=>isset($restaurant_details->restaurant_name)?$restaurant_details->restaurant_name:"",
                            'restaurant_image'=>isset($restaurant_details->image)?BASE_URL.RESTAURANT_UPLOADS_PATH.$restaurant_details->image:"",
                            'ordered_on'=>$key->ordered_time,
                            'bill_amount'=>$key->bill_amount,
                            'item_list'=>$upcoming_order_list_detail,
                            'item_total'=>$key->item_total,
                            'offer_discount'=>$key->offer_discount,
                            'restaurant_packaging_charge'=>$key->restaurant_packaging_charge,
                            'tax'=>$key->tax,
                            'delivery_charge'=>$key->delivery_charge,
                            'delivery_address'=>$key->delivery_address,
                            'restaurant_address'=>isset($restaurant_details->address)?$restaurant_details->address:""
                        );
                    }
                    

                }

            
        if(count($upcoming_order_list)!=0||count($order_list)!=0)
        {
            $response_array = array('status' => true,'past_orders'=>$order_list,'upcoming_orders'=>$upcoming_order_list);
        }
        else
        {

            $response_array = array('status' => false,'message'=>"No Orders Placed");

        }
 

        $response = response()->json($response_array, 200);
                return $response;
    }


    public function get_order_status(Request $request)
    {

        // $request_id = $request->request_id;

         if($request->header('authId')!="")
            {
                $user_id = $request->header('authId');
            }else
            {
                $user_id = $request->authId;
            }

        // $user_id = $request->header('authId');

        $request_detail = DB::table('requests')->where('user_id',$user_id)
                                               ->where('status','!=',10)
                                               ->where('status','!=',7)
                                               ->first();

         if(count($request_detail)!=0)
        {

            $order_id = $request_detail->order_id;

            $request_id = $request_detail->id;

            $ordered_time = $request_detail->ordered_time;

            $restaurant_detail = $this->restaurants::where('id',$request_detail->restaurant_id)->where('status',1)->first();

       

                    $user_detail = DB::table('users')->where('id',$request_detail->user_id)->first();

                    $address_detail = array();

                    $request_status = $request_detail->status;

                    $address_detail [] = array(
                        'd_address'=>$request_detail->delivery_address,
                        's_address'=>$restaurant_detail->address,
                        'd_lat'=>$request_detail->d_lat,
                        'd_lng'=>$request_detail->d_lng,
                        's_lat'=>$restaurant_detail->lat,
                        's_lng'=>$restaurant_detail->lng
                    );

                    $food_detail = array();
                    $bill_detail = array();

                    $data = DB::table('request_detail')->where('request_detail.request_id',$request_id)
                                                        ->join('food_list','food_list.id','=','request_detail.food_id')
                                                        ->select('request_detail.quantity as quantity','food_list.name as food','food_list.price as price_per_quantity','food_list.is_veg as is_veg')
                                                        ->get();

                            foreach($data as $d)
                            {
                                $food_detail[] = array(
                                    'name'=>$d->food,
                                    'quantity'=>$d->quantity,
                                    'price'=>$d->quantity * $d->price_per_quantity,
                                    'is_veg'=>$d->is_veg
                                );
                            }

                    $bill_detail[] = array(
                        'item_total'=>$request_detail->item_total,
                        'offer_discount'=>$request_detail->offer_discount,
                        'packaging_charge'=>$request_detail->restaurant_packaging_charge,
                        'tax'=>$request_detail->tax,
                        'delivery_charge'=>$request_detail->delivery_charge,
                        'bill_amount'=>$request_detail->bill_amount
                    );

                    $response_array = array('status'=>true,'request_id'=>$request_id,'ordered_time'=>$ordered_time,'order_id'=>$order_id,'restaurant_detail'=>$restaurant_detail,'user_detail'=>$user_detail,'address_detail'=>$address_detail,'bill_detail'=>$bill_detail,'food_detail'=>$food_detail,'request_status'=>$request_status);

        }else
        {
            $response_array = array('status'=>true,'message'=>'No Orders Available');
        }

         $response = response()->json($response_array, 200);
        return $response;
    }



    /**
     * to list all promocode details
     * 
     * @param object $request
     * 
     * @return json $response
     */
    public function get_promo_list(Request $request)
    {
      
        if($request->header('authId')!="")
        {
            $user_id = $request->header('authId');
        }else
        {
            $user_id = $request->authId;
        }
        
        //get promo list
        $get_promo = $this->promocode->where('status',1)
                                /*->whereDate('available_from','<=',Carbon::now())*/
                                ->whereDate('valid_from','>=',Carbon::now()->toDateString())
                                ->get();

        $response_array = array('status'=>true,'promo_list'=>$get_promo);
        $response = response()->json($response_array, 200);
        return $response;
    }


        /**
    * to check the availability of restaurant during checkout
    *
    * @param ogject $request
    * 
    * @return json $response
    */
    public function check_restaurant_availability(Request $request)
    {
        $validator = Validator::make(
                $request->all(),
                array(
                    'restaurant_id' => 'required',
                    'lat' => 'required',
                    'lng' => 'required'
                ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
        }else
        {
             if($request->header('authId')!="")
            {
                $user_id = $request->header('authId');
            }else
            {
                $user_id = $request->authId;
            }
            $restaurant_id = $request->restaurant_id;
            $source_lat = $request->lat;
            $source_lng = $request->lng;

            $data = $this->restaurants->where('status',1)
                    ->where('id', $restaurant_id)
                    ->select('restaurants.*')
                    ->selectRaw("(6371 * acos(cos(radians(" . $source_lat . "))* cos(radians(`lat`)) 
                            * cos(radians(`lng`) - radians(" . $source_lng . ")) + sin(radians(" . $source_lat . ")) 
                            * sin(radians(`lat`)))) as distance")
                    // ->having('distance','<=',DEFAULT_RADIUS)
                    ->orderBy('distance')
                    ->first();

            if($data)
            {
                $response_array = array('status'=>true,'restaurant'=>$data);
            }else
            {
                $response_array = array('status'=>false,'message'=>__('constants.no_data'));
            }
            $response = response()->json($response_array, 200);
            return $response;
        }
    }
}