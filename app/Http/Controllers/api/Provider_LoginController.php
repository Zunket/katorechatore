<?php

namespace App\Http\Controllers\api;
                                    
use Validator;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController;
use App\Model\Deliverypartners;

class Provider_LoginController extends BaseController
{
	public function get_profile(Request $request)
    {
        
        $partner_id = $request->header('authId');
        $data = $this->deliverypartners->where('id',$partner_id)->first();

        //dd($data);
        $vehicle = $this->vehicle->where('delivery_partners_id',$partner_id)->first();
        //dd($vehicle);
        $partner_rating = 4.4;
        $response_array = array(
            'status'=>$data->status,
            'id'=>$data->id,
            'partner_id'=>$data->partner_id,
            'name'=>$data->name,
            'email'=>$data->email,
            'phone'=>$data->phone,
            'address1'=>isset($data->Deliverypartner_detail)?$data->Deliverypartner_detail->address_line_1:"",
            'address2'=>isset($data->Deliverypartner_detail)?$data->Deliverypartner_detail->address_line_2:"",
            'profile_pic'=>$data->profile_pic,
            'driving_license_no'=>$data->driving_license_no,
            'service_zone'=>isset($data->Deliverypartner_detail->city)?$data->Deliverypartner_detail->city:"",
            'is_approved'=>$data->status,
            'joining_date'=>date("d-m-Y",strtotime($data->created_at)),
            'bank_name'=>isset($data->Deliverypartner_detail)?$data->Deliverypartner_detail->bank_name:"",
            "acc_no"=>isset($data->Deliverypartner_detail)?$data->Deliverypartner_detail->account_no:"",
            "ifsc_code"=>isset($data->Deliverypartner_detail)?$data->Deliverypartner_detail->ifsc_code:"",
            'rating'=>$partner_rating,
            'city'=>isset($data->Deliverypartner_detail->city)?$data->Deliverypartner_detail->city:"",
            'vehicle_name'=>(!empty($vehicle))?$vehicle->vehicle_name:'---',
            'insurance_image'=>(!empty($vehicle))?$vehicle->insurance_image:'---',
            'insurance_expiry_date'=>(!empty($vehicle))?$vehicle->insurance_expiry_date:'---',
            'rc_image'=>(!empty($vehicle))?$vehicle->rc_image:'---',
            'rc_no'=>(!empty($vehicle))?$vehicle->rc_no:'---',
            'vehicle_no'=>(!empty($vehicle))?$vehicle->vehicle_no:'---',
            'insurance_no'=>(!empty($vehicle))?$vehicle->insurance_no:'---',
            'earnings'=>1500.00
        );

   
        // $response_array = array('status'=>true,'data'=>$provider);
        $response = response()->json($response_array, 200);
        return $response;
    }

    public function send_otp_login(Request $request)
    {
        $phone = (string) $request->phone;
                 $otp = rand(10000,99999);
                 $message = 'OTP to verify '.APP_NAME.' Application : '.$otp;
                 $sendSms = $this->send_otp($phone,$otp);

        $response_array = array('status'=>true,'message'=>'OTP sent successfully','otp'=>$otp);

        $response = response()->json($response_array, 200);
        return $response;
    }
    
    public function generate_random_string()
    {
        return rand(11111111,99999999);
    }
    
    public function base_image_upload_license($request,$key)    
    {        
        //dd($request);
        $imageName = $request->file($key)->getClientOriginalName();       
        $ext = $request->file($key)->getClientOriginalExtension();
        $imageName = self::generate_random_string().'.'.$ext;        
        $request->file($key)->move('public/uploads/License/',$imageName);       
        return $imageName;
    }
    
    public function register(Request $request)
    {
        /*if(!$request->id){
            $rules['profile_pic'] = 'required|max:2048';
            $rules['license'] = 'required|max:2048';
            $rules['password'] = 'required';
            $rules['insurance_image'] = 'required';
            $rules['rc_image'] = 'required';
        }
            $rules['city'] = 'required';
            $rules['driver_name'] = 'required';
            $rules['vehicle_name'] = 'required';
            $rules['phone_no'] = 'required';
            $rules['address_line_1'] = 'required';
            $rules['state_province'] = 'required';
            $rules['status'] = 'required';
            $rules['zip_code'] = 'required';
            $rules['country'] = 'required';
            $rules['license_expiry'] = 'required';
            $rules['account_name'] = 'required';
            $rules['account_address'] = 'required';
            $rules['account_no'] = 'required';
            $rules['bank_name'] = 'required';
            $rules['branch_name'] = 'required';
            $rules['branch_address'] = 'required';
            $rules['rc_expiry_date'] = 'required';
            $rules['insurance_expiry_date'] = 'required';
            $rules['rc_no'] = 'required';
            $rules['insurance_no'] = 'required';
            $rules['vehicle_no'] = 'required';
            $rules['vehicle_name'] = 'required';
        */
        $validator = Validator::make(
        $request->all(),
                array(
                    'profile_pic' => 'required',
                    'email' => 'required|email|max:255|unique:delivery_partners',
                    'license' => 'required',
                    'password' => 'required',
                    'insurance_image' => 'required',
                    'rc_image' => 'required',
                    'city' => 'required',
                    'driver_name' => 'required',
                    'vehicle_name' => 'required',
                    'phone' => 'required',
                    'address_line_1' => 'required',
                    'state_province' => 'required',
                    'status' => 'required',
                    'zip_code' => 'required',
                    'country' => 'required',
                    'license_expiry' => 'required',
                    'account_name' => 'required',
                    'account_address' => 'required',
                    'account_no' => 'required',
                    'bank_name' => 'required',
                    'branch_name' => 'required',
                    'branch_address' => 'required',
                    'rc_expiry_date' => 'required',
                    'insurance_expiry_date' => 'required',
                    'rc_no' => 'required',
                    'insurance_no' => 'required',
                    'vehicle_no' => 'required',
                    //'vehicle_name' => 'required'
                ));
                
        //$validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
            
        }   else{
        	
        	if($request->id){
                $insert1 = Deliverypartners::find($request->id);
                $msg = "update_success_msg";
        	}else{
                $insert1 = new Deliverypartners();
                $insert1->password=$this->encrypt_password($request->password);
                $msg = "add_success_msg";
                
        	}
            
            // if($request->license){
            //     $license = self::base_image_upload_license($request,'license');
            //     $insert1->license=$license;
            // }
            // if($request->license){
            //     $profile_picture = self::base_image_upload_profile($request,'profile_pic');
            //     $insert1->profile_pic=$profile_picture;
            // }
	        
            $insert1->partner_id=$this->generate_partner_id();
            $insert1->name=$request->driver_name;
            $insert1->email=$request->email;
            $insert1->phone=$request->phone;
            $insert1->expiry_date=date("Y-m-d",strtotime($request->license_expiry));
            $insert1->status=$request->status;
            $insert1->save();

           	$partner_id = $insert1->id;
            
            if($request->id){
                $insert = $this->delivery_partner_details->where('delivery_partners_id',$request->id)->first();
                if(empty($insert)) $insert = $this->delivery_partner_details;
            }else{
	            $insert = $this->delivery_partner_details;
	        }

            $insert->delivery_partners_id=$partner_id;
            $insert->city=$request->city;
            $insert->vehicle_name=$request->vehicle_name;
            $insert->address_line_1=$request->address_line_1;
            $insert->address_line_2=$request->address_line_2;
            //$insert->address_city=$request->address_city;
            $insert->state_province=$request->state_province;
            $insert->country=$request->country;
            $insert->zip_code=$request->zip_code;
            $insert->about=$request->about;
            $insert->account_name=$request->account_name;
            $insert->account_address=$request->account_address;
            $insert->account_no=$request->account_no;
            $insert->bank_name=$request->bank_name;
            $insert->branch_name= $request->branch_name;
            $insert->branch_address=$request->branch_address;
            $insert->swift_code=$request->swift_code;
            $insert->routing_no=$request->routing_no;
            $insert->save();


            if($request->id){
                $vehicle = $this->vehicle->where('delivery_partners_id',$request->id)->first();
                if(empty($vehicle)) $vehicle = $this->vehicle;
            }else{
	            $vehicle = $this->vehicle;
            }
            // if($request->insurance_image){
            //     $insurance_image = self::base_image_upload_profile($request,'insurance_image');
            //     $vehicle->insurance_image=$insurance_image;
            // }
            // if($request->rc_image){
            //     $rc_image = self::base_image_upload_profile($request,'rc_image');
            //     $vehicle->rc_image=$rc_image;
            // }
            $vehicle->delivery_partners_id=$partner_id;
            $vehicle->vehicle_name=$request->vehicle_name;
            $vehicle->vehicle_no=$request->vehicle_no;
            $vehicle->insurance_no=$request->insurance_no;
            $vehicle->rc_no=$request->rc_no;
            $vehicle->insurance_expiry_date=date("Y-m-d",strtotime($request->insurance_expiry_date));
            $vehicle->rc_expiry_date=date("Y-m-d",strtotime($request->rc_expiry_date));
            $vehicle->save();
            $response_array = array('status'=>true,'message'=>'Driver registered successfully');
        }

        $response = response()->json($response_array, 200);
        return $response;
    }
    
    public function base_image_upload_profile($request,$key)    
    {        
        $imageName = $request->file($key)->getClientOriginalName();       
         $ext = $request->file($key)->getClientOriginalExtension();
         $imageName = self::generate_random_string().'.'.$ext;        
         $request->file($key)->move('public/uploads/Profile/',$imageName);       
         return $imageName;
    }

    public function provider_login(Request $request)
    {

        $validator = Validator::make(
        $request->all(),
                array(
                    'device_token' => 'required',
                    'phone' => 'required',
                    'password' => 'required'
                ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
        }
        else
        {
            $deliverypartners = $this->deliverypartners;
            $device_token = $request->device_token;
            $device_type = $request->device_type;
            
                $phone = $request->phone;
                $password = $request->password;


                $data = $deliverypartners::where('phone',$phone)->where('password',$this->encrypt_password($password))->first();
                //dd($data);
                if(count($data)!=0)
                {
                
                    $authId = $data['id'];
                    $profile_image = $data['profile_pic'];
                    
                if($profile_image==null)
                {
                    $profile_image = PROFILE_ICON;
                }
                if($data['name']!=NULL)
                {
                    $name = $data['name'];
                }else
                {
                    $name="";
                } 
                $bank_name = isset($data->Deliverypartner_detail)?$data->Deliverypartner_detail->bank_name:"";
                $acc_no    = isset($data->Deliverypartner_detail)?$data->Deliverypartner_detail->account_no:"";
                $ifsc_code = isset($data->Deliverypartner_detail)?$data->Deliverypartner_detail->ifsc_code:"";
            
                $authToken =$this->generateRandomString();
                
                $partner_id = $data->partner_id;
                
                $deliverypartners::where('id',$data->id)->update(['device_type'=>$device_type, 'device_token'=>$device_token,'authToken'=>$authToken]);
                
                $response_array = array('status' => true,'message' => 'Logged in successfully','authId'=>$authId,'authToken'=>$authToken,'phone'=>$phone,'profile_image'=>$profile_image,'email'=>$data['email'],'user_name'=>$name,'partner_id'=>$partner_id,'bank_name'=>$bank_name,'acc_no'=>$acc_no,'ifsc_code'=>$ifsc_code);
                
                }else
                {
                    $response_array = array('status' => false,'message' => 'Invalid Login');
                }
            
        }

        $response = response()->json($response_array, 200);
        return $response;
    }
    

    public function update_profile(Request $request)
    {
        
        # code... profile_update
        $deliverypartners = $this->deliverypartners;
        
        $custom = $this->custom;
        $update = array();
        $update1 = array();
        $request->id = $request->authId;
        $data = $deliverypartners::where('id',$request->id)->first();
        $deliverypartners_details = $deliverypartners->Deliverypartner_detail;
        //$data1 = DB::table('delivery_partner_details')->where('delivery_partners_id',$request->id)->first();
        //$data1 = $deliverypartners_details::where('delivery_partners_id',$request->id)->first();

        if ($request->name) {
            $update['name'] = $request->name;
        }
        if ($request->email) {
            $update['email'] = $request->email;
        }
        if ($request->phone) {
            $update['phone'] = $request->phone;
        }
        if ($request->profile_pic) {
            if ($data->profile_pic != "") {
                $custom::delete_image($data->profile_pic);
            }
            $profile_pic = self::base_image_upload_profile($request,'profile_pic');
            $update['profile_pic'] = $profile_pic;
            //$profile_pic = $custom::upload_image($request,'profile_image');
            //$update['profile_image'] = $profile_pic;
        }
        
        if ($request->zip_code) {
            $update1['zip_code'] = $request->zip_code;
        }
        
        if ($request->country) {
            $update1['country'] = $request->country;
        }
        if ($request->vehicle_name) {
            $update1['vehicle_name'] = $request->vehicle_name;
        }
        if ($request->address_line_1) {
            $update1['address_line_1'] = $request->address_line_1;
        }
        if ($request->address_line_2) {
            $update1['address_line_2'] = $request->address_line_2;
        }

        
        $vehicle = $this->vehicle->where('delivery_partners_id',$request->id)->first();

        if($request->insurance_image){
                $insurance_image = self::base_image_upload_profile($request,'insurance_image');
                $vehicle->insurance_image=$insurance_image;
            }
        if($request->vehicle_name!="")
        {
            $vehicle->vehicle_name=$request->vehicle_name;
        }
        if($request->vehicle_no!="")
        {
            $vehicle->vehicle_no=$request->vehicle_no;
        }
        if($request->rc_no!="")
        {
            $vehicle->rc_no=$request->rc_no;
        }
        if($request->insurance_no!="")
        {
            $vehicle->insurance_no=$request->insurance_no;
        }
        if($request->insurance_expiry_date!="")
        {
            $vehicle->insurance_expiry_date=date("Y-m-d",strtotime($request->insurance_expiry_date));
        }
        $deliverypartners::where('id',$request->id)->update($update);
        
        //$deliverypartners_details::where('delivery_partners_id',$request->id)->update($update1);
      
        $delivery_partner_details = DB::table('delivery_partner_details')->where('delivery_partners_id',$request->id)->update($update1);
        $vehi = DB::table('vehicle')->where('delivery_partners_id',$request->id)->first();
        $data = $deliverypartners::where('id',$request->id)->first();
        $returndata = array('name' => $data->name,
                            'phone' => $data->phone,
                            'email' => $data->email,
                            'address_line_1' => $data->address_line_1,
                            'address_line_2' => $data->address_line_2,
                            'vehicle_name' => $vehi->vehicle_name,
                            'vehicle_no' => $vehi->vehicle_no,
                            'rc_no' => $vehi->rc_no,
                            'insurance_no' => $vehi->insurance_no,
                            'insurance_expiry_date' => $vehi->insurance_expiry_date,
                            'profile_pic' => $data->profile_pic,
                            'id' => $data->id,
                            'token' => $request->header('authToken'));
        $response_array = array('status'=>true,'message'=>'Profile updated successfully','data'=>$returndata);
        $response = response()->json($response_array, 200);
        return $response;
    }

    public function forgot_password(Request $request)
    {
        $validator = Validator::make(
                $request->all(),
                array(
                    'phone' => 'required'
                ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
        }else
        {
            $phone = $request->phone;

            $deliverypartners = $this->deliverypartners;

            $check_user = $deliverypartners::where('phone',$phone)->first();

            if(count($check_user)!=0)
            {
                $phone = (string) $request->phone;
                $otp = rand(10000,99999);
                $message = 'OTP to verify '.APP_NAME.' Application : '.$otp;
                $sendSms = $this->send_otp($phone,$otp);

            $response_array = array('status'=>true,'message'=>'OTP sent successfully','otp'=>$otp);
            }else
            {
                $response_array = array('status'=>false,'message'=>'Mobile number not registered');
            }
        }

         $response = response()->json($response_array, 200);
        return $response;

    }

    public function reset_password(Request $request)
    {

         $validator = Validator::make(
                $request->all(),
                array(
                    'password' => 'required',
                    'phone' => 'required'
                ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
        }else
        {
            $password = $this->encrypt_password($request->password);
            $phone = $request->phone;
            $deliverypartners = $this->deliverypartners;

            $get_user = $deliverypartners::where('phone',$phone)->first();

            $deliverypartners::where('phone',$phone)->update(['password'=>$password]);

            $response_array = array('status'=>true,'message'=>'Password Reset Successfull');
        }

        $response = response()->json($response_array, 200);
        return $response;

    }


    /**
     * API for earnings based on daily
     * 
     * @param object $request
     * 
     * @return json $response
     */
    public function today_earnings(Request $request)
    {
        $user_id = $request->header('authId');
        $foodrequest = $this->foodrequest;
        $today_date = new \DateTime();
        $date = ((isset($request->filter_date) && $request->filter_date!='')) ? new \DateTime($request->filter_date) : $today_date;
        
        //get daily total earnings        
        $today_earnings = $foodrequest::where('delivery_boy_id',$user_id)
                            ->whereBetween('ordered_time',[$date->format('Y-m-d')." 00:00:00", $date->format('Y-m-d')." 23:59:59"])
                            ->where('is_paid',1)
                            ->sum('bill_amount');
        $total_earnings = $foodrequest::where('delivery_boy_id',$user_id)
                            ->where('is_paid',1)
                            ->sum('bill_amount');
        $total_orders =    $foodrequest::where('delivery_boy_id',$user_id)
                            ->where('is_paid',1)
                            ->count();                    
        $today_incentives = $foodrequest::where('delivery_boy_id',$user_id)
                            ->whereBetween('ordered_time',[$date->format('Y-m-d')." 00:00:00", $date->format('Y-m-d')." 23:59:59"])
                            ->where('is_paid',1)
                            ->sum('delivery_boy_commision'); 

        $response_array = array('status'=>true,'today_earnings'=>$today_earnings,'total_earnings'=>$total_earnings,'today_incentives'=>$today_incentives,'total_orders'=>$total_orders);
        $response = response()->json($response_array, 200);
        return $response;
    }
    
    



    /**
     * API for weekly earnings
     * 
     * @param object $request
     * 
     * @return json $response
     */
    public function weekly_earnings(Request $request)
    {
        $user_id = $request->header('authId');
        $foodrequest = $this->foodrequest;
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        $date = ((isset($request->filter_date) && $request->filter_date!='')) ? $request->filter_date : Carbon::now();

        //get weekly earnings
        $weekly_earnings    = $foodrequest->where('delivery_boy_id',$user_id)->where('is_paid',1)
                                ->whereBetween('ordered_time',[Carbon::parse($date)->startOfWeek(),Carbon::parse($date)->endOfWeek()])
                                ->sum('bill_amount');
        $weekly_incentives  = $foodrequest->where('delivery_boy_id',$user_id)->where('is_paid',1)
                                ->whereBetween('ordered_time',[Carbon::parse($date)->startOfWeek(),Carbon::parse($date)->endOfWeek()])
                                ->sum('delivery_boy_commision');
        $graph_data         = $foodrequest->where('delivery_boy_id',$user_id)->where('is_paid',1)
                                ->whereBetween('ordered_time',[Carbon::parse($date)->startOfWeek(),Carbon::parse($date)->endOfWeek()])
                                ->select(array(DB::Raw('sum(bill_amount) as total_amount'),DB::Raw('count(id) as total_orders'),DB::Raw('DATE(ordered_time) day')))
                                ->groupBy('day')
                                ->get();
        $response_array     = array('status'=>true,'weekly_earnings'=>round($weekly_earnings,2),'weekly_incentives'=>
                                round($weekly_incentives,2),'graph_data'=>$graph_data);

        $response = response()->json($response_array, 200);
        return $response;
    }


    /**
     * API for monthly earnings
     * 
     * @param object $request
     * 
     * @return json $response
     */
    public function monthly_earnings(Request $request)
    {
        $user_id = $request->header('authId');
        $foodrequest = $this->foodrequest;
        $date = ((isset($request->filter_date) && $request->filter_date!='')) ? $request->filter_date : Carbon::now();

        //get weekly earnings
        $monthly_earnings   = $foodrequest->where('delivery_boy_id',$user_id)->where('is_paid',1)
                                    ->whereBetween('ordered_time',[Carbon::parse($date)->startOfMonth(),Carbon::parse($date)->endOfMonth()])
                                    ->sum('bill_amount');
        $monthly_incentives = $foodrequest->where('delivery_boy_id',$user_id)->where('is_paid',1)
                                    ->whereBetween('ordered_time',[Carbon::parse($date)->startOfMonth(),Carbon::parse($date)->endOfMonth()])
                                    ->sum('delivery_boy_commision');
        $graph_data         = $foodrequest->where('delivery_boy_id',$user_id)->where('is_paid',1)
                                    ->whereBetween('ordered_time',[Carbon::parse($date)->startOfMonth(),Carbon::parse($date)->endOfMonth()])
                                    ->select(array(DB::Raw('sum(bill_amount) as total_amount'),DB::Raw('count(id) as total_orders'),DB::Raw('DATE(ordered_time) day')))
                                    ->groupBy('day')
                                    ->get();
        $response_array = array('status'=>true,'monthly_earnings'=>round($monthly_earnings,2),'monthly_incentives'=>
                            round($monthly_incentives,2),'graph_data'=>$graph_data);

        $response = response()->json($response_array, 200);
        return $response;
    }


    /**
     * delivery boy logout
     * 
     * @param object $request
     * 
     * @return json $response
     */
    public function logout(Request $request)
    {
        $user_id = $request->header('authId');
        $deliverypartners = $this->deliverypartners;
        $deliverypartners::where('id','=',$user_id)->update(['authToken'=>0]);
        $deliverypartners::where('id','=',$user_id)->update(['device_token'=>0]);
        $response_array = array('status'=>true,'message'=>'Logged Out Successfully');
        $response = response()->json($response_array, 200);
        return $response;
    }


    /**
     * API for driver payout
     * 
     * @param object $request
     * 
     * @return json $response
     */
    public function payout_details(Request $request)
    {
        $user_id = $request->header('authId');

        if($user_id=='')
        {
            $error_messages = "AuthID should not be empty";
            $response_array = array('status' => false, 'error_code' => 101, 'message' => $error_messages);
        }
        //get payout history
        $payout_history  = $this->driver_payout_history->where('delivery_boy_id', $user_id)
                                        ->orderBy('created_at','desc')->limit(5)->get();
                            
        $pending_amount = $this->foodrequest->where('delivery_boy_id', $user_id)
                                    ->where('driver_settlement_status',0)->where('status',7)
                                    ->sum('delivery_boy_commision');

        $response_array = array('status'=>true,'pending_payout'=> round($pending_amount,2),'payout_history'=>$payout_history);
        $response = response()->json($response_array, 200);
        return $response;
    }

	
}