<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    protected $table = 'users';

    /**
    * set relationship to food request.
    *
    */
    public function Foodrequest()
    {
        return $this->hasMany('App\Model\Foodrequest','user_id','id');
    }

    public function review()
    {
        return $this->hasMany('App\Model\Review','customer_id');
    }
}
