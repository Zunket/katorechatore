<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubscribtionPayment extends Model
{
    protected $table = 'subscribtion_payment';
    //protected $fillable = ['category_name', 'status', 'created_at', 'updated_at' ];
    protected $hidden = ["created_at", "updated_at"];
    /**
     * set relationship to foodlist.
     */
    // public function Foodlist()
    // {
    //     return $this->hasMany('App\Model\Foodlist','id','category_id');
    // }
    public function subscribtion()
    {
        return $this->belongsTo('App\Model\Subscribtion','subscribtion_id');
    }
}
