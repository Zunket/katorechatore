<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    

    /**
    * set relationship to food request.
    *
    */
    public function user()
    {
        return $this->belongsTo('App\Model\Users','customer_id');
    }
    public function restaurant()
    {
        return $this->belongsTo('App\Model\Restaurants','restaurant_id');
    }
}
