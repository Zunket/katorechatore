<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Subscribtion extends Model
{
    protected $table = 'subscribtions';
    //protected $fillable = ['category_name', 'status', 'created_at', 'updated_at' ];
    
    protected $hidden = ["created_at", "updated_at"];

    /**
     * set relationship to foodlist.
     */
    public function payment()
    {
        return $this->hasOne('App\Model\SubscribtionPayment','subscribtion_id');
    }
}
