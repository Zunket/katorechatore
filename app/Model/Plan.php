<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
class Plan extends Model
{
    //
    //protected $fillable = ['category_name', 'status', 'created_at', 'updated_at' ];

    /**
     * set relationship to foodlist.
     */
    public function user()
    {
        return $this->hasMany('App\Model\Users','id','user_id');
    }


    public function getImgAttribute($value){
    		return  URL::to($value);
    }
}
