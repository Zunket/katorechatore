<?php

use Illuminate\Http\Request;

header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
//date_default_timezone_set("Asia/Kolkata");
Route::group(['prefix'=>'admin'],function(){

Route::group(['namespace'=>'admin'],function(){

        Route::get('/', 'LoginController@index');
        Route::get('/register', 'LoginController@registerLogin')->name('register');
        Route::post('/register', 'LoginController@register')->name('register.post');
        Route::post('/login','LoginController@login');

        Route::group(['middleware'=>'checkLogin'],function(){

            Route::get('/dashboard','DashboardController@index');

            Route::get('/logout','LoginController@logout');

                 //Dispatcher
                
            Route::get('/dispatcher','RestaurantController@dispatcher');


                // Restaurant List
            Route::get('/store_list','RestaurantController@restaurant_list');
            Route::get('/setting/edit_store/{id}','RestaurantController@setting_edit_restaurant'); 
            Route::get('/setting/edit_account/{id}','RestaurantController@setting_edit_account'); 
            Route::get('/setting/edit_paypal/{id}','RestaurantController@setting_edit_paypal'); 
            Route::POST('/setting/setting_add_to_restaurants','RestaurantController@setting_add_to_restaurants');
            Route::POST('/setting/setting_add_to_accounts','RestaurantController@setting_add_to_accounts');
            Route::POST('/setting/setting_add_to_paypal','RestaurantController@setting_add_to_paypal');
            Route::get('/add_store','RestaurantController@restaurant');
            Route::POST('/add_to_restaurants','RestaurantController@add_to_restaurants');
            Route::GET('/edit_store/{id}','RestaurantController@edit_restaurant');
            Route::POST('/delete_store/{id}','RestaurantController@delete_restaurant');
            Route::get('/store_view/{id}','RestaurantController@restaurant_view');

            Route::GET('/status_enable/{id}','RestaurantController@status_enable');
            Route::GET('/status_disable/{id}','RestaurantController@status_disable');
             //check restaurant address
            Route::post('/check_restaurant_address','RestaurantController@check_restaurant_address');



                // Delivery Peopple API
            Route::get('/deliverypeople_list','DeliverypeopleController@deliverypeople_list');
            Route::get('/add_deliverypeople','DeliverypeopleController@add_deliverypeople');
            Route::POST('/add_to_deliverypeople','DeliverypeopleController@add_to_deliverypeople');
            Route::GET('/edit_delivery_partner/{id}','DeliverypeopleController@edit_delivery_partner');
            Route::POST('/delete_delivery_partner','DeliverypeopleController@delete_delivery_partner');


            Route::get('/shift_delivery', function () {
                    return view('shift_delivery');
                });

                // Dispute manager
            Route::get('/disp_managerlist','DispController@disp_managerlist');
            Route::get('/add_dispmanager','DispController@add_dispmanager');

             
                // Promocode
            Route::get('/promocodes_list','PromocodeController@promocodes_list');
            Route::get('/add_promocode','PromocodeController@add_promocode');
            Route::post('/add_to_promocode','PromocodeController@add_to_promocode');
            Route::GET('/edit_promocode/{id}','PromocodeController@edit_promocode');
            Route::POST('/delete_promocode/{id}','PromocodeController@delete_promocode');


             // Banners
             Route::get('/banner_list','BannerlistController@banner_list');
             Route::get('/add_banner','BannerlistController@add_banner');
             Route::post('/add_to_banners','BannerlistController@add_to_banners');
             Route::GET('/edit_banner/{id}','BannerlistController@edit_banner');
             Route::GET('/edit_user/{id}','UserController@edit_user');
             Route::POST('/delete_banner','BannerlistController@delete_banner');
             Route::POST('/delete_user/{id}','UserController@delete_user');
             
                // Note board
             Route::GET('/noticeboard_list','NoteboardController@noticeboard_list');
             Route::GET('/add_noticeboard','NoteboardController@add_noticeboard');
             Route::GET('/custumpush','NoteboardController@custumpush');
             Route::GET('/notification_list','NoteboardController@notification_list');
             Route::POST('/send_custumpush','NoteboardController@send_custumpush');
              Route::POST('/delete_notification','NoteboardController@delete_notification');
                Route::get('/user_list','UserController@user_list');
                Route::get('/add_user','UserController@Adduser');


                            Route::get('/restuarant_leads', function () {
                                return view('restuarant_leads');   
                            });

                            Route::get('/delivery_people', function () {
                                return view('delivery_people');   
                            });


                            Route::get('/news_letter', function () {
                                return view('news_letter');   
                            });


                            /*Route::get('/deliveries', function () {
                                return view('deliveries');   
                            });*/


                            // Cuisine List
                            Route::get('/cuisines_list','ItemmasterController@get_cuisines_list');
                            Route::get('/deliveries','OrderController@deliveries');
                            Route::get('/add_cuisines','ItemmasterController@add_cuisines');

                            Route::POST('/add_to_cuisines','ItemmasterController@add_to_cuisines');
                            Route::POST('/delete_cuisine','ItemmasterController@delete_cuisine');
                             Route::POST('/delete_addon','ItemmasterController@delete_addon');
                            //Add-ons route
                            Route::get('/addons_list', 'ItemmasterController@list_addons');
                            Route::get('/add_addons', 'ItemmasterController@add_addons');
                            Route::get('/edit_addons/{id}', 'ItemmasterController@edit_addons');
                            Route::POST('/store_addons','ItemmasterController@store_addons');
                            Route::GET('/addon_status_enable/{id}','ItemmasterController@addon_status_enable');
                            Route::GET('/addon_status_disable/{id}','ItemmasterController@addon_status_disable');

                            //Food quantity route
                            Route::get('/food-quantity-list', 'ItemmasterController@list_food_quantity');
                            Route::get('/add-food-quantity', 'ItemmasterController@add_food_quantity');
                            Route::get('/edit-food-quantity/{id}', 'ItemmasterController@edit_food_quantity');
                            Route::POST('/store-food-quantity','ItemmasterController@store_food_quantity');


                            // Category list
                            Route::get('/category_list','ItemmasterController@get_category_list');
                            Route::get('/add_category', 'ItemmasterController@index');
                            Route::POST('/add_to_category','ItemmasterController@add_to_category');
                            Route::GET('edit_category/{id}','ItemmasterController@edit_category');
                            Route::POST('delete_category/{id}','ItemmasterController@delete_category');
                            Route::POST('delete_food_quantity/{id}','ItemmasterController@delete_food_quantity');


                            // Product List
                            Route::get('/product_list','RestaurantController@product_list');
                            Route::get('/add_product', 'RestaurantController@add_product');
                            Route::POST('/add_to_product', 'RestaurantController@add_to_product');
                            Route::GET('edit_product_list/{id}', 'RestaurantController@edit_product_list');
                            Route::POST('/update_product_list', 'RestaurantController@update_product_list');
                            Route::post('/delete_product/{food_id}','RestaurantController@delete_product_list');
                            Route::get('/getrestaurant_based_detail/{id}', 'RestaurantController@getrestaurant_based_detail');
                            Route::GET('/food_status_enable/{id}','RestaurantController@food_status_enable');
                            Route::GET('/food_status_disable/{id}','RestaurantController@food_status_disable');

                            // Restaurant Cuisines
                            Route::get('/restaurant_cuisines','RestaurantmasterController@restaurant_cuisines');
                            Route::get('/add_restaurant_cuisines','RestaurantmasterController@add_restaurant_cuisines');
                            Route::POST('/add_to_restaurant_cuisines','RestaurantmasterController@add_to_restaurant_cuisines');
                            Route::POST('/delete_restaurant_cuisine','RestaurantmasterController@delete_restaurant_cuisine');
                            Route::GET('/restaurant_status_enable/{id}','RestaurantmasterController@restaurant_status_enable');
                            Route::GET('/restaurant_status_disable/{id}','RestaurantmasterController@restaurant_status_disable');

                            // Restaurant Menu list
                            Route::get('/restaurant_menu','RestaurantController@restaurant_menu');
                            Route::post('/store_restaurant_menu','RestaurantController@store_restaurant_menu');
                            Route::get('/add_restaurant_menu','RestaurantController@add_restaurant_menu');
                            Route::get('/edit_restaurant_menu/{id}','RestaurantController@editrestaurant_menu');
                            Route::post('/update_restaurant_menu/{id}','RestaurantController@edit_restaurant_menu');
                            Route::post('/delete_restaurant_menu/{id}','RestaurantController@del_restaurant_menu');
                            Route::GET('/menu_status_enable/{id}','RestaurantController@menu_status_enable');
                            Route::GET('/menu_status_disable/{id}','RestaurantController@menu_status_disable');
                            
                            //order managemnt
                            Route::get('/orders/{type}','OrderController@order_list');

                             Route::get('/order_dashboard','OrderController@order_dashboard');
                             Route::get('/orderwise_report_pagination','RestaurantController@orderwise_report_pagination');

                            Route::get('/neworder_list/{status?}','OrderController@neworder_list');
                            Route::post('is_read','DashboardController@is_read_post');
                            Route::get('/accept_request/{request_id}','OrderController@accept_request');
                            Route::get('/cancel_request/{request_id}','OrderController@cancel_request');
                            Route::get('/assign_request/{request_id}','OrderController@assign_request');
                            Route::get('/view_order/{request_id}','OrderController@view_order');
                            Route::get('/commission_settings','DashboardController@commission_settings');
                            Route::POST('/update_commission_settings','DashboardController@update_commission_settings');

                            //Restaurant Reports
                            Route::get('/restaurant_report','RestaurantController@restaurant_report');
                            Route::post('/restaurant_report_filter','RestaurantController@restaurant_report_filter');

                            //Admin Restaurant Report 
                            Route::get('/admin_restaurant_report','RestaurantController@admin_restaurant_report');
                            Route::get('/admin_report_view/{id}','RestaurantController@admin_report_view');
                            Route::get('/delivery_boy_reports_view/{id}','RestaurantController@delivery_boy_reports');
                            Route::get('/delivery_boy_reports','RestaurantController@delivery_boy_report');
                            Route::post('/delivery_boy_report_filter','RestaurantController@delivery_boy_report_filter');
                             Route::get('/deliveryboy_report_pagination','RestaurantController@deliveryboy_report_pagination');
                            Route::get('/restaurant_report_pagination','RestaurantController@restaurant_report_pagination');

                          //City Management
                          Route::get('add_city','RestaurantController@city_management');
                          Route::post('/city_add','RestaurantController@add_city');
                          Route::get('edit_city/{id}','RestaurantController@edit_city');
                          Route::post('/update_city','RestaurantController@update_city');
                          Route::get('city_list','RestaurantController@city_list');
                          Route::get('add_areas/{id}','RestaurantController@area_setting');
                          Route::get('edit_area/{id}','RestaurantController@edit_area');
                          Route::post('/area_add','RestaurantController@add_area');
                          Route::get('view_areas/{id}','RestaurantController@area_list');
                          Route::post('/update_area_list','RestaurantController@update_area_list');
                          Route::post('/delete_area_list/{id}','RestaurantController@delete_area_list');
                          Route::post('/delete_city/{id}','RestaurantController@delete_city');
                          Route::post('/delete_country/{id}','RestaurantController@delete_country');
                          Route::post('/delete_state/{id}','RestaurantController@delete_state');
                          //get areas based on city
                          Route::get('/getcity_area/{id}', 'RestaurantController@getcity_area');
                          //get state and area based on country
                          Route::get('/getprovience/{provienceid}/{id}', 'NationController@getprovience');

                         //Document Management
                        Route::get('add_document','RestaurantController@document_management');
                        Route::post('/document_add','RestaurantController@document_add');
                        Route::get('document_list','RestaurantController@document_list');

                        Route::get('plan','RestaurantController@plans');
                        Route::post('/plan','RestaurantController@add_plan');
                        Route::get('/plan/{id}','RestaurantController@edit_plan');
                        Route::post('delete_plan/{id}','RestaurantController@delete_plan');
                        //Vehicle Management
                        Route::get('add_vehicle','RestaurantController@vehicle_management');
                        Route::post('/vehicle_add','RestaurantController@vehicle_add');
                        Route::post('/user_add','UserController@user_add');
                        Route::get('/vehicle_edit/{id}','RestaurantController@editvehicle');
                        Route::get('vehicle_list','RestaurantController@vehicle_list');
                        Route::post('delete_vehicle/{id}','RestaurantController@delete_vehicle');
                        Route::post('delete_document/{id}','RestaurantController@delete_document');
                        Route::post('delete_reason/{id}','RestaurantController@delete_reason');
                        //Cancellation Reasons
                        Route::get('cancellation_reason_list','RestaurantController@cancellation_reason');
                        Route::post('/add_reason','RestaurantController@add_reason');
                        Route::get('reason_list','RestaurantController@reason_list');

                        //Driver Management
                         Route::get('review','RestaurantController@review');
                         Route::post('delete_rating','RestaurantController@delete_rating');
                         Route::get('add_new_driver','RestaurantController@driver');
                         Route::post('add_driver','RestaurantController@add_driver');
                         Route::get('driver_list','RestaurantController@driver_list');
                         Route::get('view_delivery_boy_order_details/{id}','RestaurantController@view_deliveryboy_order_details');
                         Route::GET('/edit_delivery_boy_details/{id}','RestaurantController@edit_delivery_boy_details');

                         //Coupon Management
                         Route::get('add_coupon','PromocodeController@coupon');
                         Route::post('coupon_add','PromocodeController@coupon_add');
                         Route::get('coupon_list','PromocodeController@coupon_list');
                         Route::GET('/edit_coupon/{id}','PromocodeController@edit_coupon');
                         Route::POST('/delete_coupon/{id}','PromocodeController@delete_coupon');

                      //get payout based on type
                      Route::GET('/payout/{type}', 'TransactionController@Getpayout');
                      Route::GET('/add_payout/{type}/{amount}/{id}', 'TransactionController@Getaddpayment');
                      Route::POST('/add_payout', 'TransactionController@addpayment');
                      Route::GET('/payout_history/{type}', 'TransactionController@Getpayout_history');

                      //get site setting
                      Route::GET('/settings/{type}', 'SettingController@Getsettings');
                      Route::POST('/update-setting', 'SettingController@Updatesetting');

                      Route::GET('/add_email', 'SettingController@Getaddemail');
                      Route::GET('/email_template_list', 'SettingController@Getemailtemplate');

                      //get country list
                      Route::GET('/country_list', 'NationController@Getcountrylist');
                      Route::GET('/add_country', 'NationController@Addcountry');
                      Route::GET('/edit_country/{id}', 'NationController@AddEditcountry');
                      Route::POST('/save_country', 'NationController@Savecountry');
                      Route::GET('/default_country/{id}', 'NationController@Defaultcountry');
                      Route::GET('/state_list', 'NationController@Getstatelist');
                      Route::GET('/add_state', 'NationController@Addstate');
                      Route::GET('/edit_state/{id}', 'NationController@AddEditstate');
                      Route::POST('/save_state', 'NationController@Savestate');

                      Route::get('/get_state_ajax/{id}','NationController@get_state_ajax');

        });  

    });  
    });  


