<?php

header('Access-Control-Allow-Origin: *');
// header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
// header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
//date_default_timezone_set("Asia/Kolkata");

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace'=>'api'],function(){

	Route::POST('/register','LoginController@register');
	Route::POST('/login','LoginController@user_login');
	Route::POST('/send_otp','LoginController@send_otp_login');
	Route::POST('/forgot_password','LoginController@forgot_password');
	Route::POST('/reset_password','LoginController@reset_password');
	Route::POST('/update_profile','LoginController@update_profile')->middleware('checkApiGuest');

	// Route::group(['middleware'=>'authCheck'],function(){
		Route::GET('plans','UserController@plans');
		Route::POST('subscribe','UserController@subscribe');
		Route::GET('get_default_address','UserController@get_default_address');
		Route::POST('set_delivery_address','UserController@set_delivery_address');
		Route::POST('track_order_detail','UserController@track_order_detail')->middleware('checkApiGuest');
		Route::GET('/get_current_order_status','UserController@get_current_order_status');
		Route::GET('/get_delivery_address','UserController@get_delivery_address');
		Route::POST('/add_delivery_address','UserController@add_delivery_address');
		Route::GET('/get_profile','LoginController@get_profile');
		Route::GET('/get_filter_list/{filter_type}','UserController@get_filter_list');	// filter_type =1 - Cusines table else relevance table
		Route::POST('/get_relevance_restaurant','UserController@get_relevance_restaurant');
		Route::POST('/get_menu','UserController@get_menu');
		Route::POST('/get_nearby_restaurant','UserController@get_nearby_restaurant');
		Route::GET('/get_nearby_restaurant','UserController@get_nearby_restaurant');
		Route::GET('/get_banners','UserController@get_banners');
		Route::GET('/get_popular_brands','UserController@get_popular_brands');
		Route::POST('/search_restaurants','RestaurantController@search_restaurants');
		//Route::GET('/get_restaurant_list','UserController@get_restaurant_list');
		Route::GET('/get_favourite_list','UserController@get_favourite_list')->middleware('checkApiGuest');
		Route::POST('/update_favourite','UserController@update_favourite')->middleware('checkApiGuest');
		Route::POST('/filter_relevance_restaurant','UserController@filter_relevance_restaurant');
		Route::POST('/single_restaurant','RestaurantController@single_restaurant');
		Route::POST('/add_to_cart','RestaurantController@add_to_cart')->middleware('checkApiGuest');
		Route::POST('/reduce_from_cart','RestaurantController@reduce_from_cart')->middleware('checkApiGuest');
		Route::GET('/check_cart','RestaurantController@check_cart');
		Route::GET('/get_category/{restaurant_id}','RestaurantController@get_category');
		Route::POST('/get_category_wise_food_list','RestaurantController@get_category_wise_food_list');
		Route::POST('/get_food_list','RestaurantController@get_food_list');
		Route::POST('/checkout','RestaurantController@checkout')->middleware('checkApiGuest');
		Route::POST('/paynow','RestaurantController@paynow')->middleware('checkApiGuest');
		Route::GET('/order_history','UserController@order_history');
		Route::GET('/get_order_status','UserController@get_order_status');
		Route::GET('/logout','LoginController@logout');

		//check restuarant during checkout
		Route::POST('/check_restaurant_availability','UserController@check_restaurant_availability');

		//get offers list
		Route::GET('/get_promo_list','UserController@get_promo_list');

		//update ratings for order
		Route::POST('/order_ratings','OrderController@order_ratings')->middleware('checkApiGuest');
		Route::POST('/get_last_order_rating','OrderController@lastOrderRating');

		//validate promocode
		Route::POST('/check_promocode','OrderController@check_promocode');

	// });


	Route::group(['prefix'=>'providerApi'],function(){

		
		Route::POST('/register','Provider_LoginController@register');
		Route::POST('/login','Provider_LoginController@provider_login');
		Route::POST('/send_otp','Provider_LoginController@send_otp_login');
		Route::POST('/forgot_password','Provider_LoginController@forgot_password');
		Route::POST('/reset_password','Provider_LoginController@reset_password');

        Route::post('/get_profile','Provider_LoginController@get_profile');
        Route::GET('/get_profile','Provider_LoginController@get_profile');
		Route::group(['middleware'=>'authCheck'],function(){

			
			

			Route::POST('/update_profile','Provider_LoginController@update_profile')->middleware('checkApiGuest');
			Route::POST('/get_address_detail','OrderController@get_address_detail');
			Route::POST('/update_request','OrderController@update_request');
			Route::POST('/cancel_request','OrderController@cancel_request');
			Route::GET('/get_order_status','OrderController@get_order_status');
			Route::GET('/order_history','OrderController@order_history')->middleware('checkApiGuest');
			
			//earnings api
			Route::POST('/today_earnings','Provider_LoginController@today_earnings');
			Route::POST('/weekly_earnings','Provider_LoginController@weekly_earnings');
			Route::POST('/monthly_earnings','Provider_LoginController@monthly_earnings');

			//payout details api
			Route::GET('/payout_details','Provider_LoginController@payout_details');

		});

	});



});



